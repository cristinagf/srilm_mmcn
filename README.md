# mmcn 2014
Tested on srilm 1.7.0
Here the source for multi-microphone confusion network (MMCN) will be uploaded.
Goal: combine word lattices extracted from multi-microphone captured signals into a Confusion Network
Microphones must be synchronized. A minimum of 3 microphone signal decoding processes are required. 

1) Patch Makefile (to incorporate MMCN source)
2) Add to lattice/src folder the files lattice.cc and mmcn.cc
3) Compile SRiLM


(2016 moving to bitbucket.. https://bitbucket.org/cristinagf/srilm_mmcn.)
