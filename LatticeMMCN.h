/* 
 * File:   LatticeMMCN.h
 * Author: guerrero
 *
 * Created on May 22, 2014, 5:01 PM
 */

#ifndef LATTICEMMCN_H
#define	LATTICEMMCN_H

#include "Lattice.h"

class LatticeMMCN : public Lattice {    
    public:
        //LatticeMMCN(const LatticeMMCN& orig);
        LatticeMMCN (Vocab &vocab, const char *name, SubVocab &ignoreVocab) : Lattice (vocab, name, ignoreVocab) {}
        //virtual ~LatticeMMCN();
        //using Lattice::Lattice;
        LogP2 getTotalPosterior(File &file, double posteriorScale); //CGF
private:

};

#endif	/* LATTICEMMCN_H */

