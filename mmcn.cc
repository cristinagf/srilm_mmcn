/*
 * mmcn --
 *	Combine word lattices directly into a Confusion Network
 */

#ifndef lint
static char Copyright[] = "Copyright (c) 1997-2011 SRI International, 2012 Microsoft Corp.  All Rights Reserved.";
static char RcsId[] = "@(#)$Id: mmcn.cc,v 1.0 2014/05/14 11:19:00 cguerrero Exp $";
#endif


#ifdef PRE_ISO_CXX
#include <iostream.h>
#else
#include <iostream>
using namespace std;
#endif
#include <stdio.h>
#include <math.h>
#include <locale.h>
#include <errno.h>
#ifndef _MSC_VER
#include <sys/time.h>
#include <unistd.h>
#endif
#include <signal.h>
#include <setjmp.h>

#ifndef SIGALRM
#define NO_TIMEOUT
#endif

#include "option.h"
#include "version.h"
#include "Vocab.h"
#include "Lattice.h"
#include "MultiwordVocab.h"
#include "ProductVocab.h"
#include "MultiwordLM.h"
#include "NonzeroLM.h"
#include "Ngram.h"
#include "LMClient.h"
#include "ClassNgram.h"
#include "SimpleClassNgram.h"
#include "ProductNgram.h"
#include "BayesMix.h"
#include "LoglinearMix.h"
#include "RefList.h"
#include "LatticeLM.h"
#include "WordMesh.h"
#include "zio.h"
#include "mkdir.h"

#include "LatticeMMCN.h"
#include <sstream>
#include <fstream>
#include <vector>
#include <algorithm>    // std::min_element, std::max_element

#define DebugPrintFunctionality 1	// same as in Lattice.cc
static int haveError = 0;
static int version = 0;
static int debug = 0;
static int debugx = 0;
static int printsilence = 0;
static int savebounds = 0;
static char *inLatticeList = 0;
static char *outLatticeDir = 0;
static int readHTK = 0;
static int computePosteriors = 0;
static char *writePosteriors = 0;
static char *writePosteriorsDir = 0;
static char *writeMesh = 0;
static char *writeMeshDir = 0;

static double posteriorScale = 8.0;
static int useHTKnulls = 1;
static int order = 3;
static int intlogs = 0;
static char *noiseVocabFile = 0;


static HTKHeader htkparms(HTK_undef_float, HTK_undef_float, HTK_undef_float,
        HTK_undef_float, HTK_undef_float, HTK_undef_float,
        HTK_undef_float, HTK_undef_float, HTK_undef_float,
        HTK_undef_float, HTK_undef_float, HTK_undef_float,
        HTK_undef_float, HTK_undef_float, HTK_undef_float);

/* MMCN Parameters and Consts */
static char *writeAnalysis1 = 0;
static char *writeCandidates = 0;
//static float THRSNULL = 0.46;
static double THRSNULL = 0.46;
static double conversionfactor = 100.0;
static double manualdelta = 0.03;
static double manualdeltaS = 0.03;
static double manualdeltaE = 0.03;
static double perctgDelta = 0.2;
static double minmics = 1.0;
static unsigned additionOriginalVersion = 1;
static double shiftbound=0.0;
static int shiftendbound = 1;
static int coupleboundtoshift = 1;

/*----------------------------*/
static double minPosteriorProb = 0.3;
static char *tmpDataFile = 0;
static char *outCNfilename = 0;
static char *outHypfilename = 0;
static char *outBoundfilename = 0;
static char *inBoundfilename = 0;

static char *outCNfilename2 = 0;
static char *prefixout = 0;

static char *outPPRef = 0;
static char *outPPMMCN = 0;


static unsigned numMics = 1;
static vector<double> vBoundaries;
static vector<double> vBoundariesFwBw;
static vector<double> vBoundariesTests;
vector<vector<double> > vCoupleBoundaries;
vector<string> vRefwords;
static int boundsvalidation = 0;
static int scoremethod = 1;
static double lratio = 66.667;
static vector<string> vHypothesis;

static vector<double> vPostHypothesis;
static vector<float> vTStartHypothesis;
static vector<float> vTEndHypothesis;
static double deltaref = 0.2;
static double alpha = 2.5;
static char *utterance = 0;

static double percdeltab = 66.667;
static char *inRefWords = 0;
static unsigned typeAnalysis = 0;

static Option options[] = {
    { OPT_TRUE, "version", &version, "print version information"},
    { OPT_INT, "debug", &debug, "debug level"},
    { OPT_INT, "debugx", &debugx, "debug local level"},
    { OPT_INT, "printsilence", &printsilence, "print SIL if it's the maxscored in segment"},
    { OPT_INT, "savebounds", &savebounds, "save boundaries 1-0"},
    { OPT_STRING, "in-lattice-list", &inLatticeList, "input lattice list for expansion or bigram weight substitution"},
    { OPT_STRING, "tmpfile", &tmpDataFile, "filename where temporal data will be saved"},
    { OPT_TRUE, "read-htk", &readHTK, "read input lattices in HTK format"},
    { OPT_TRUE, "compute-posteriors", &computePosteriors, "compute the node posteriors of given lattices"},
    { OPT_STRING, "write-posteriors", &writePosteriors, "write posterior lattice format to this file"},
    { OPT_TRUE, "compute-posteriors", &computePosteriors, "compute the node posteriors of given lattices"},
    { OPT_STRING, "write-posteriors", &writePosteriors, "write posterior lattice format to this file"},
    { OPT_STRING, "write-posteriors-dir", &writePosteriorsDir, "write posterior lattices to this directory"},
    { OPT_STRING, "write-mesh", &writeMesh, "write posterior mesh (sausage) to this file"},
    { OPT_STRING, "write-mesh-dir", &writeMeshDir, "write posterior meshes to this directory"},
    { OPT_FLOAT, "min-posterior", &minPosteriorProb, "minimum posterior probability of links to use in the boundary identification"},
    { OPT_FLOAT, "lratio", &lratio, "ratio of link length to keep for deltaBoundaries"},
    { OPT_FLOAT, "deltaref", &deltaref, "default deltaBoundaries to identify very-short segments"},
    { OPT_FLOAT, "alpha", &alpha, "default alpha value to scale Posterior Scores"},
    { OPT_STRING, "out-mesh", &outCNfilename, "filename where output CN will be saved"},
    { OPT_STRING, "cn2", &outCNfilename2, "filename where output CN2 will be saved"},
    { OPT_STRING, "out-hypothesis", &outHypfilename, "filename where output will be saved"},
    { OPT_STRING, "out-bounds", &outBoundfilename, "filename where boundaries output will be saved"},
    { OPT_STRING, "in-bounds", &inBoundfilename, "filename from where boundaries are read (eg reference)"},
    { OPT_STRING, "outPPRef", &outPPRef, "filename where PP output of forced RefBoundaries will be saved"},
    { OPT_STRING, "outPPMMCN", &outPPMMCN, "filename where PP output of MMCN using forced RefBoundaries will be saved"},
    { OPT_STRING, "prefixout", &prefixout, "prefix for output of shifting hypotheses to be levenshtein evaluated"},
    { OPT_STRING, "utterance", &utterance, "id utterance name to print on the result file"},
    { OPT_STRING, "reference", &inRefWords, "input reference words and its boundaries"},
    { OPT_STRING, "outanalysis", &writeAnalysis1, "write output of analysis 1 to this file"},
    { OPT_FLOAT, "deltab", &percdeltab, "deltaBoundaries for analysis"},
    { OPT_INT, "typeanalysis", &typeAnalysis, "typeAnalysis 1 2 or 3"},
    { OPT_FLOAT, "minmics", &minmics, "minNummics for peak selection"},
    { OPT_INT, "boundsvalidation", &boundsvalidation, "Method to apply for boundary validation 0 1 2"},
    { OPT_INT, "scoremethod", &scoremethod, "Method to apply for InterMic score 1AVG 2MAX"},
    { OPT_INT, "coupleboundtoshift", &coupleboundtoshift, "Id of coupleboundtoshift by shiftbound"},
    { OPT_FLOAT, "THRSNULL", &THRSNULL, "Threshold to indicate Null is prevalent in segment"},
    { OPT_FLOAT, "manualdelta", &manualdelta, "Manually fed DeltaT to forcedSegment Validation"},
    { OPT_FLOAT, "manualdeltaS", &manualdeltaS, "Manually fed DeltaT to forcedSegment Start Validation"},
    { OPT_FLOAT, "manualdeltaE", &manualdeltaE, "Manually fed DeltaT to forcedSegment End Validation"},
    { OPT_FLOAT, "shiftbound", &shiftbound, "Manually shift a bound at forcedSegment boundaries "},
    { OPT_INT, "shiftendbound", &shiftendbound, "Flag shiftendbound =1 or 0, default=1"},
    { OPT_STRING, "outcandidates", &writeCandidates, "write boundaries and candidates of confusion sets to this file"}
};

double round(double d) {
    return floor(d + 0.5);
}

/* Sign function as template, take forever to compile */
/*template <typename T> int sgn(T val) {*/
/*    return (T(0) < val) - (val < T(0));*/
/*}*/

/* Sign function */
short signd(double d) {
    if (d == 0) {
        return 0;
    } else {
        if ((fabs(d) - d) > 0) {
            return -1;
        } else {
            return 1;
        }

    }
}

/*
 * From each lattice file, extract data in the proper format for processing
 * #PFSGnode word EndTime PosteriorProb idMicrophone
 * Nodes with NULL word are excluded from the processing
 */
void getDataMatrix(int latticeCount, char *inLat, Vocab &vocab, SubVocab &ignoreWords) {
    Lattice lat(vocab, idFromFilename(inLat), ignoreWords);
    lat.debugme(debug);

    //Read HTK Lattice
    {
        File file(inLat, "r", 0);
        if (file.error()) {
            cerr << "error opening ";
            perror(inLat);
            haveError = 1;
            return;
        }

        Boolean status;
        htkparms.amscale = posteriorScale;
        status = lat.readHTK(file, &htkparms, useHTKnulls);
        if (!status) {
            cerr << "ERROR reading " << inLat << endl;
            haveError = 1;
            return;
        }

        char *writePosteriors = tmpDataFile;
        File fileout(writePosteriors, "a");
        LogP2 totalPosterior = lat.getTotalPosterior(file, posteriorScale);
        for (unsigned i = 0; i < lat.getMaxIndex(); i++) {
            LatticeNode *node = lat.nodes.find(i);
            
            float prevtime;
            TRANSITER_T<NodeIndex, LatticeTransition> inTransIter(node->inTransitions);
            NodeIndex previous;
            if (inTransIter.next(previous)) {
                LatticeNode *prevNode = lat.findNode(previous);
                assert(prevNode != 0);
                //cout << lat.getWord(node->word) << " " << prevNode->htkinfo->time << "->" << node->htkinfo->time << endl;
                prevtime = prevNode->htkinfo->time;
            } else {
                prevtime = 0;
            }
            string sLatWord = (string) (lat.getWord(node->word));
            if (sLatWord.compare("NULL") == 0) {

                if (prevtime < node->htkinfo->time) {
                    fprintf(fileout, "%u %s %lf %lf %lf %u\n", i,
                            lat.getWord(node->word), prevtime, node->htkinfo->time,
                            (double) LogPtoProb(node->posterior - totalPosterior), latticeCount);
                }
            } else {
                fprintf(fileout, "%u %s %lf %lf %lf %u\n", i,
                        lat.getWord(node->word), prevtime, node->htkinfo->time,
                        (double) LogPtoProb(node->posterior - totalPosterior), latticeCount);
            }

        }
    }

}

/*
 * Analysis 1 th1=0
 * For a DeltaB/minPP under analysis
 * check occurrences of links with Wi
 * meeting the boundary restrictions by DeltaB
 * and with the boundaries from a minPosteriorProb given.
 */
void analysis_deltaB() {
    cout << "Looking for Wi in Boundaries..." << endl;
    File fileout(writeAnalysis1, "a");

    /* Get every Word-Boundaries in reference file */
    ifstream inputFile(inRefWords);
    string line;
    vector<float> vTimeS, vTimeE;
    vector<string> vWord;
    while (getline(inputFile, line)) {
        if (!line.length() || line[0] == '#')
            continue;
        char cWordlink[20];
        double startTime = 0., endTime = 0.;
        sscanf(line.c_str(), "%s %lf %lf", &cWordlink, &startTime, &endTime);
        string sWordlink = (string) cWordlink;

        vTimeS.push_back(startTime);
        vTimeE.push_back(endTime);
        vWord.push_back(sWordlink);
    }

    /* For every line in DataFile
     * check if link belongs to matching reference word
     * if so, check boundaries for every DeltaBPerc,
     * if so, record w-mic-DeltaBPerc-LengthRef-output=1
     * else:
     */
    vector<vector<unsigned> > mWordsMics;
    vector<unsigned> vZeros;
    for (unsigned mic = 0; mic < numMics; mic++) {
        vZeros.push_back(0);
    }
    for (unsigned wrow = 0; wrow < vWord.size(); wrow++) {
        mWordsMics.push_back(vZeros);
    }

    ifstream inputFileData(tmpDataFile);
    string lineData;
    while (getline(inputFileData, lineData)) {
        unsigned numLatNode = 0, idMic = 0;
        char cWordlink[20];
        double startTime = 0., endTime = 0., postProb = 0.;
        //double sampleLength = 0.;
        sscanf(lineData.c_str(), "%u %s %lf %lf %lf %u", &numLatNode, &cWordlink, &startTime, &endTime, &postProb, &idMic);
        string sWordlink = (string) cWordlink;
        /* For every reference word .. */
        for (unsigned i_word = 0; i_word < vWord.size(); i_word++) {
            string refWord = vWord[i_word];
            double refTStart = vTimeS[i_word];
            double refTEnd = vTimeE[i_word];
            float refLength = refTEnd - refTStart;
            /* Only if Word matches ref, then do analysis */
            if ((sWordlink.compare(refWord) == 0) && (postProb >= minPosteriorProb)) {
                /* For every DeltaB to analyze */

                double DeltaB = refLength * percdeltab / 100.0;
                double refStartDeltaA = refTStart - DeltaB;
                double refStartDeltaB = refTStart + DeltaB;
                double refEndDeltaA = refTEnd - DeltaB;
                double refEndDeltaB = refTEnd + DeltaB;

                Boolean foundValidLink = ((startTime >= refStartDeltaA && startTime <= refStartDeltaB) & (endTime >= refEndDeltaA && endTime <= refEndDeltaB));
                if (foundValidLink) {
                    mWordsMics[i_word][idMic] = mWordsMics[i_word][idMic] + 1;
                }
            }
        }
    }
    /* Output if Wref was not found in each mic */
    for (unsigned wrow = 0; wrow < vWord.size(); wrow++) {
        string currentWord = vWord[wrow];
        for (unsigned mic = 0; mic < numMics; mic++) {
            unsigned counts = mWordsMics[wrow][mic];
            if (counts > 0) {
                fprintf(fileout, "%s %u %u\n", currentWord.c_str(), mic, counts);
            }
        }
    }

}

void useFileBoundaries() {
    cout << "Reading string of time-boundaries from file: " << inBoundfilename << endl;
    ifstream inputFile(inBoundfilename);
    string line;
    double fsampling = 16000.0;

    while (getline(inputFile, line)) {
        if (!line.length() || line[0] == '#')
            continue;
        double timeBoundary = 0.;
        sscanf(line.c_str(), "%lf", &timeBoundary);
        vBoundaries.push_back(timeBoundary / fsampling);
    }

    cout << "Read boundaries: ";
    for (short i = 0; i < vBoundaries.size(); i++) {
        cout << vBoundaries[i] << " ";
    }
    cout << endl;
}

void useFileBoundarieswrd() {
    cout << "Reading string of time-boundaries from wrdfile: " << inBoundfilename << endl;
    ifstream inputFile(inBoundfilename);
    string line;
    double fsampling = 16000.0;

    while (getline(inputFile, line)) {
        if (!line.length() || line[0] == '#')
            continue;
        double timeBoundary1 = 0., timeBoundary2 = 0.;
        char cRefword[25];
        sscanf(line.c_str(), "%lf %lf %s", &timeBoundary1, &timeBoundary2, &cRefword);

        string sRefword = (string) cRefword;
        vector<double> vBoundaryWord;
        vBoundaryWord.push_back(timeBoundary1 / fsampling);
        vBoundaryWord.push_back(timeBoundary2 / fsampling);
        vCoupleBoundaries.push_back(vBoundaryWord); /* Vector of vector (couple) of bounds */
        vRefwords.push_back(sRefword); /* Vector of words */
        
        vBoundaries.push_back(timeBoundary1 / fsampling); //Vector of bounds
        vBoundaries.push_back(timeBoundary2 / fsampling); //Vector of bounds        
    }
    std::sort(vBoundaries.begin(), vBoundaries.end());
    vBoundaries.erase(std::unique(vBoundaries.begin(), vBoundaries.end()), vBoundaries.end());

    cout << "Read boundaries: ";
    for (short i = 0; i < vCoupleBoundaries.size(); i++) {
        cout << "(" << vCoupleBoundaries[i][0] << " " << vCoupleBoundaries[i][1] << ") ";
    }
    cout << endl;
}

/*
 * Use DataMatrix in the proper format to Identify Time Boundaries
 * to use in posterior Segment Validation
 * pq curve (y) is derived from link starting/ending points
 * and the peaks are extracted from y'.
 */
void identifyBoundariesPQ() {
    cout << "Identifying time boundaries. " << "Num Microphones: " << numMics << endl;
    /* ************ Selecting valid links from DataMatrix ************ */
    unsigned minPosteriorProbX = 0;
    unsigned deltafr = 5, k = 3;
    ifstream inputFile(tmpDataFile);
    string line;
    vector<unsigned> vTimeStart, vTimeEnd, vMicid;
    vector<double> vPcurve, vQcurve, vPpq, vPpqd;
    vector<string> vWords;
    unsigned maxTime = 0;
    vector<double> xtime;

    /* Get data from DataMatrix */
    while (getline(inputFile, line)) {
        if (!line.length() || line[0] == '#')
            continue;
        unsigned numLatNode = 0, idMic = 0;
        char cWordlink[25]; //char wordlink;
        double startTime = 0., endTime = 0., postProb = 0.;
        sscanf(line.c_str(), "%u %s %lf %lf %lf %u", &numLatNode, &cWordlink, &startTime, &endTime, &postProb, &idMic);
        string sWordlink = (string) cWordlink;
        if (postProb >= minPosteriorProbX) {
            vTimeStart.push_back(round((startTime * conversionfactor)));
            vTimeEnd.push_back(round((endTime * conversionfactor)));
            vWords.push_back(sWordlink);
            vMicid.push_back(idMic);
            if (((endTime * conversionfactor)) > maxTime) {
                maxTime = round((endTime * conversionfactor));
            }
        }
    }
    for (double i = 0; i <= (maxTime / conversionfactor); i = i + 0.01) {
        xtime.push_back(i);
    }

    /* Denom derivative */
    double pdenom = 0.000000;
    for (int j = 0; j <= 2 * k; j++) {
        short nj = j - k;
        pdenom += (pow(nj, 2));
    }

    /* P computed using link start times */
    for (unsigned timefr = 0; timefr < maxTime; ++timefr) {
        vector<string> vWordsFrame;
        vWordsFrame.clear();
        /* Define time-window [pretimefr, postimefr] */
        int pretimefr, postimefr;
        pretimefr = timefr - deltafr;
        postimefr = timefr + deltafr;
        if (timefr <= deltafr) {
            pretimefr = 0;
        }
        if (timefr >= (maxTime - deltafr)) {
            postimefr = timefr;
        }
        vector<unsigned> vTfr;
        /* Get list of index and word of links starting from time-window */
        /* Omit NULL links */
        for (unsigned i = 0; i < vTimeStart.size(); i++) {
            if (vTimeStart[i] >= pretimefr & vTimeStart[i] <= postimefr) {
                if (!(vWords[i].find("NULL") != std::string::npos)) {
                    vTfr.push_back(i);
                    vWordsFrame.push_back(vWords[i]);
                }
            }
        }
        /* Get list of unique occurrence of words */
        std::sort(vWordsFrame.begin(), vWordsFrame.end());
        vWordsFrame.erase(std::unique(vWordsFrame.begin(), vWordsFrame.end()), vWordsFrame.end());
        double p = 0.000000;
        if (vWordsFrame.empty()) {
            p = 0.000000;
        } else {
            p = 1.000000;
        }

        /* For each word in time-window */
        for (unsigned wid = 0; wid < vWordsFrame.size(); wid++) {
            unsigned countwid = 0;
            /* check for each mic: */
            for (unsigned mic = 0; mic < numMics; mic++) {
                /* Are there any links starting from time-window in this mic? */
                vector<unsigned> vIndexMic;
                for (unsigned selectedlink = 0; selectedlink < vTfr.size(); selectedlink++) {
                    if (vMicid[vTfr[selectedlink]] == mic) {
                        vIndexMic.push_back(vTfr[selectedlink]);
                    }
                }

                if (!(vIndexMic.empty())) {
                    unsigned counterwmic = 0;
                    /* If so, get list of unique words */
                    vector<string> vWordsMicFr;
                    for (unsigned indexm = 0; indexm < vIndexMic.size(); indexm++) {
                        //vWordsMicFr.push_back(vWords[vIndexMic[indexm]]);
                        if (vWords[vIndexMic[indexm]].compare(vWordsFrame[wid]) == 0) {
                            counterwmic++;
                        }
                    }
                    if (counterwmic > 0) {
                        countwid++;
                    }
                }
            }
            double productp = countwid / double(numMics);
            p = p*productp;
        }
        double scalefr = (1.000000 / vWordsFrame.size());
        vPcurve.push_back(pow(p, scalefr));
    }


    /* Q computed using link end times */
    for (unsigned timefr = 0; timefr < maxTime; ++timefr) {
        vector<string> vWordsFrame;
        vWordsFrame.clear();
        /* Define time-window [pretimefr, postimefr] */
        int pretimefr, postimefr;
        pretimefr = timefr - deltafr;
        postimefr = timefr + deltafr;
        if (timefr <= deltafr) {
            pretimefr = 0;
        }
        if (timefr >= (maxTime - deltafr)) {
            postimefr = timefr;
        }
        vector<unsigned> vTfr;
        /* Get list of index and word of links starting from time-window */
        /* Omit NULL links */
        for (unsigned i = 0; i < vTimeEnd.size(); i++) {
            if (vTimeEnd[i] >= pretimefr & vTimeEnd[i] <= postimefr) {
                if (!(vWords[i].find("NULL") != std::string::npos)) {
                    vTfr.push_back(i);
                    vWordsFrame.push_back(vWords[i]);
                }
            }
        }
        /* Get list of unique occurrence of words */
        std::sort(vWordsFrame.begin(), vWordsFrame.end());
        vWordsFrame.erase(std::unique(vWordsFrame.begin(), vWordsFrame.end()), vWordsFrame.end());
        double q = 0.000000;
        if (vWordsFrame.empty()) {
            q = 0.000000;
        } else {
            q = 1.000000;
        }

        /* For each word in time-window */
        for (unsigned wid = 0; wid < vWordsFrame.size(); wid++) {
            unsigned countwid = 0;
            /* check for each mic: */
            for (unsigned mic = 0; mic < numMics; mic++) {
                /* Are there any links starting from time-window in this mic? */
                vector<unsigned> vIndexMic;
                for (unsigned selectedlink = 0; selectedlink < vTfr.size(); selectedlink++) {
                    if (vMicid[vTfr[selectedlink]] == mic) {
                        vIndexMic.push_back(vTfr[selectedlink]);
                    }
                }

                if (!(vIndexMic.empty())) {
                    unsigned counterwmic = 0;
                    /* If so, get list of unique words */
                    vector<string> vWordsMicFr;
                    for (unsigned indexm = 0; indexm < vIndexMic.size(); indexm++) {
                        if (vWords[vIndexMic[indexm]].compare(vWordsFrame[wid]) == 0) {
                            counterwmic++;
                        }
                    }
                    if (counterwmic > 0) {
                        countwid++;
                    }
                }
            }
            double productp = countwid / double(numMics);
            q = q*productp;
        }

        double scalefr = (1.000000 / vWordsFrame.size());
        vQcurve.push_back(pow(q, scalefr));
    }





    /* p+q */
    for (unsigned i = 0; i < vPcurve.size(); i++) {
        vPpq.push_back(vPcurve[i] + vQcurve[i]);
    }

    /* Deriv p+q */
    for (int idxp = 0; idxp < vPpq.size(); idxp++) {
        double pnum = 0.000000, indivalue = 0.000000;
        for (int j = 0; j <= 2 * k; j++) {
            short nj = j - k;
            if (((idxp + nj) >= 0) && ((idxp + nj) <= vPpq.size())) {
                indivalue = nj * vPpq[idxp + nj];
            } else {
                if ((idxp + nj) < 0) { //if ( (idxp+nj)<=0 ) {
                    indivalue = nj * vPpq[0];
                } else {
                    indivalue = nj * vPpq[vPpq.size()];
                }
            }
            pnum = pnum + indivalue;
        }
        vPpqd.push_back(pnum / pdenom);
    }
    vPcurve.clear();
    vQcurve.clear();

    /* Peak identification */
    unsigned prevval = 0;
    int signprevval = -1;
    for (int idpq = 0; idpq < vPpqd.size(); idpq++) { //CGF! matlab size-1!
        if ((vPpqd[idpq] < 0) && (signprevval > 0)) {
            vBoundaries.push_back(xtime[idpq - 1]);
        }
        signprevval = signd(vPpqd[idpq]);
        prevval = vPpqd[idpq];
    }

    cout << "Boundariesx[" << vBoundaries.size() << "]: ";
    for (unsigned i = 0; i < vBoundaries.size(); ++i) {
        cout << vBoundaries[i] << " ";
    }
    cout << endl;
}

/* Put in vector boundaries, those from Fw and Bw previous validations */
void resetboundaries() {
    vBoundaries.clear();
    /* Leave unique values in vector */
    std::sort(vBoundariesFwBw.begin(), vBoundariesFwBw.end());
    vBoundariesFwBw.erase(std::unique(vBoundariesFwBw.begin(), vBoundariesFwBw.end()), vBoundariesFwBw.end());

    cout << endl << "New boundaries:";
    for (short i = 0; i < vBoundariesFwBw.size(); i++) {
        vBoundaries.push_back(vBoundariesFwBw[i]);
        cout << vBoundariesFwBw[i] << " ";
    }
    cout << endl;
    vHypothesis.clear();
}

/*
 * Use DataMatrix in the proper format to Identify Time Boundaries
 * to use in posterior Segment Validation
 */
void identifyBoundaries() {
    cout << "Identifying time boundaries (minPosterior " << minPosteriorProb
            << ")..." << "Num Microphones: " << numMics << endl;

    /* ************ Selecting valid links from DataMatrix ************ */
    unsigned maxTime = 0;
    ifstream inputFile(tmpDataFile);
    string line;
    vector<unsigned> vTime, vPost;

    while (getline(inputFile, line)) {
        if (!line.length() || line[0] == '#')
            continue;
        unsigned numLatNode = 0, idMic = 0;
        char wordlink;
        double startTime = 0., endTime = 0., postProb = 0.;
        sscanf(line.c_str(), "%u %s %lf %lf %lf %u", &numLatNode, &wordlink, &startTime, &endTime, &postProb, &idMic);

        if (postProb >= minPosteriorProb) {
            vTime.push_back(round((startTime * conversionfactor) + additionOriginalVersion));
            vTime.push_back(round((endTime * conversionfactor) + additionOriginalVersion));
            vPost.push_back(postProb);
            if (((endTime * conversionfactor) + additionOriginalVersion) > maxTime) {
                maxTime = round((endTime * conversionfactor) + additionOriginalVersion);
            }
        }
    }

    /* ************ Accumulator of Time Boundaries ************ */
    vector<unsigned> vAccumulatorBoundaries(maxTime + 1, 0);
    for (unsigned i = 0; i < unsigned(vTime.size()); i++) {
        vAccumulatorBoundaries[vTime[i]]++;
    }
    for (unsigned i = 0; i < maxTime + 1; ++i) {
        if (vAccumulatorBoundaries[i] > 0) {
            cout << "[" << i << ": " << vAccumulatorBoundaries[i] << "] ,";
        }
    }
    cout << endl;

    /* ************ Peak Selection ********** */
    unsigned maxPeaks = 0;
    //double delta = perctgDelta * numMics;
    double delta = minmics;
    int mn = 1000, mx = -1000, mnpos = -1, mxpos = -1;
    Boolean isLookingForMax = true;

    cout << "Delta: " << delta << endl;
    for (unsigned i = 0; i < (maxTime + 1); ++i) {
        int currentAccum = vAccumulatorBoundaries[i];
        if (currentAccum > mx) {
            mx = currentAccum;
            mxpos = i;
        }
        if (currentAccum < mn) {
            mn = currentAccum;
            mnpos = i;
        }

        if (i == maxTime) {
            maxPeaks += 1;
            vBoundaries.push_back(mxpos / conversionfactor); //Original
            mn = currentAccum;
            mnpos = i;
            isLookingForMax = false;
        }

        if (isLookingForMax) {
            if (currentAccum < (mx - delta)) {
                maxPeaks += 1;
                vBoundaries.push_back(mxpos / conversionfactor); //Original
                mn = currentAccum;
                mnpos = i;
                isLookingForMax = false;
            }
        } else {
            if (currentAccum > (mn + delta)) {
                mx = currentAccum;
                mxpos = i;
                isLookingForMax = true;
            }
        }
    }

    cout << "Boundaries: ";
    for (unsigned i = 0; i < maxPeaks; ++i) {
        cout << vBoundaries[i] << " ";
    }
    cout << endl;
}

/*
 * Use parameters: DeltaB and minPosterior to compute
 * per each word in reference -word sequence-
 * if it exists in the confusion set or not
 */
void isHypothesisInCN() {
    /* Get every Word-Boundaries in reference file */
    ifstream refFile(inRefWords);
    string refline;
    vector<float> vTimeS, vTimeE;
    vector<string> vWord;
    /* Read reference words+startBoundary+endBoundary */
    while (getline(refFile, refline)) {
        if (!refline.length() || refline[0] == '#')
            continue;
        char cWordlink[20];
        double startTime = 0., endTime = 0.;
        sscanf(refline.c_str(), "%s %lf %lf", &cWordlink, &startTime, &endTime);
        string sWordlink = (string) cWordlink;

        vTimeS.push_back(startTime);
        vTimeE.push_back(endTime);
        vWord.push_back(sWordlink);
    }

    /* For each word in reference,
     * get confusion set.
     * If ref word is in set C1=1, else C1=0
     * if it has highest posterior C2=1 else C2=0 */
    unsigned currentalign = 1; //boundStart = 0, boundEnd = 0,
    float NullPost = 1;
    File cnout1("/home/guerrero/tmpfilex", "a");

    unsigned numalign = 1;

    for (unsigned i = 0; i < vWord.size(); i++) {
        double thismaxscore = .0;
        vector<double> vThisWScores;
        vThisWScores.clear();

        string currentRefWord = vWord[i];
        unsigned isWordInCandidates = 0;
        unsigned isWordTopCandidate = 0;

        NullPost = 1;
        vector<unsigned> vMicid;
        vector<double> vPosts;
        vector<string> vWords, vSegmentWords;
        vector<double> vFinalScores;

        vFinalScores.clear();
        vSegmentWords.clear();
        vMicid.clear();
        vPosts.clear();
        vWords.clear();

        float tS = vTimeS[i], tE = vTimeE[i], DeltaT = 0.01;
        DeltaT = ((tE - tS)*(percdeltab / 100));
        float WLen = (tE + DeltaT) - max((float) 0.0, (tS - DeltaT));
        if (debug)
            cout << endl << "Checking boundaries " << tS << "," << tE << "(" << DeltaT << ") ";
        /* Select links accomplishing the boundary constraints */
        ifstream inputFile(tmpDataFile);
        string line;
        vector<float> vStartTime, vEndTime;
        while (getline(inputFile, line)) {
            if (!line.length() || line[0] == '#')
                continue;
            unsigned numLatNode = 0, idMic = 0;
            char cWordlink[20];
            string sWordlink;
            float startTime = 0., endTime = 0.;
            double postProb = 0.;
            sscanf(line.c_str(), "%u %19s %f %f %lf %u", &numLatNode, cWordlink, &startTime, &endTime, &postProb, &idMic);
            sWordlink = (string) cWordlink;
            /* Ommit <s> </s> in octave, here is equivalent to NULLs*/
            if ((sWordlink.find("<s>") != std::string::npos) | (sWordlink.find("NULL") != std::string::npos)) {
            } else {
                Boolean startIsValid = false, endIsValid = false;
                startIsValid = ((startTime >= (tS - DeltaT)) && (startTime <= (tS + DeltaT)));
                endIsValid = ((endTime >= (tE - DeltaT)) && (endTime <= (tE + DeltaT)));
                if (startIsValid && endIsValid) {
                    vPosts.push_back(postProb);
                    vWords.push_back(sWordlink);
                    vSegmentWords.push_back(sWordlink);
                    vMicid.push_back(idMic);
                }
            }
        }
        /* If links were found ... */
        if (vPosts.size() > 0) {
            if (debug)
                cout << "  Found " << vPosts.size() << " links.." << endl;
            /* Get list of unique occurrence of words */
            std::sort(vSegmentWords.begin(), vSegmentWords.end());
            vSegmentWords.erase(std::unique(vSegmentWords.begin(), vSegmentWords.end()), vSegmentWords.end());
            /* ****************** DISPLAY ***************** */
            if (debug) {
                cout << "   Unique words: ";
                for (unsigned i = 0; i < vSegmentWords.size(); i++) {
                    cout << vSegmentWords[i] << " ";
                }
                cout << endl << "   ";
            }
            unsigned numWords = vSegmentWords.size();
            vector<vector<double> > mWordsMics;
            /* Per each word */
            for (unsigned w = 0; w < numWords; w++) {
                vector<unsigned> vIndex;
                string currentWord = vSegmentWords[w];
                for (unsigned i = 0; i < vWords.size(); i++) {
                    if (vWords[i] == currentWord) {
                        vIndex.push_back(i);
                    }
                }
                /* Per each microphone */
                vector<double> vMics;
                for (unsigned mic = 0; mic < numMics; mic++) {
                    /* Get indexes of links of current Mic */
                    //cout << "<Mic" << mic << " ";

                    vector<unsigned> vIndexMic;
                    for (unsigned selectedlink = 0; selectedlink < vIndex.size(); selectedlink++) {
                        if (vMicid[vIndex[selectedlink]] == mic) {
                            vIndexMic.push_back(vIndex[selectedlink]);
                        }
                    }
                    /* IntraMic Score */
                    double Postscore = .0;
                    if (vIndexMic.size() > 0) {
                        for (unsigned i = 0; i < vIndexMic.size(); i++) {
                            Postscore = Postscore + vPosts[vIndexMic[i]];
                        }
                    }
                    vMics.push_back(Postscore);
                }
                mWordsMics.push_back(vMics);
                 /*   To catch per mic score */
                if (currentWord.compare(currentRefWord) == 0) {
                    for (unsigned IXmic = 0; IXmic < numMics; IXmic++) {
                        vThisWScores.push_back(vMics[IXmic]);
                    }
                }
            }
            /* Fixed ALPHA*/
            alpha = 1;
            /* InterMic Score */
            vector<double> vNewPost;
            double sumvNewPost = .0, sumvNewPost2 = .0;
            for (unsigned wrow = 0; wrow < mWordsMics.size(); wrow++) {
                double newPost = .0;
                for (unsigned mic = 0; mic < numMics; mic++) {
                    newPost = newPost + mWordsMics[wrow][mic];
                }
                double interMicWordScore = (newPost / numMics);
                vNewPost.push_back(interMicWordScore);
                sumvNewPost = sumvNewPost + (interMicWordScore);
            }
            if (debug) {
                cout << "   Final Scores ";
                cout << "[vNewPost size " << vNewPost.size() << "] ";
                cout << " [alpha*sumvNewPost " << alpha * sumvNewPost << "] ";
            }
            if ((alpha * sumvNewPost) > 1) {
                for (unsigned i = 0; i < vNewPost.size(); i++) {
                    vFinalScores.push_back(vNewPost[i] / (sumvNewPost));
                    sumvNewPost2 += (vNewPost[i] / (sumvNewPost));
                    if (debug)
                        cout << (vNewPost[i] / (sumvNewPost)) << " ";
                }
            } else {
                for (unsigned i = 0; i < vNewPost.size(); i++) {
                    vFinalScores.push_back(vNewPost[i]);
                    sumvNewPost2 += vNewPost[i];
                    if (debug)
                        cout << (vNewPost[i]) << " ";
                }
            }
            /* Criterion to continue/stop search of segment */
            NullPost = 1 - sumvNewPost2;
        } else {
            NullPost = 1;
            if (debug)
                cout << "  Found no valid links" << endl;
        }

        /* If a segment is valid */
        string topCandidateX;
        unsigned rankRefWord;
        if ((NullPost <= THRSNULL) & (vPosts.size() > 0)) {
            /* Add NULL word and score to stack of candidates */
            vFinalScores.push_back(NullPost);
            vSegmentWords.push_back("NULL");
            if (debug)
                cout << "[FScore size " << vFinalScores.size() << "] ";

            /* Find maximum candidate score */
            int indexMax = vSegmentWords.size() - 1;
            double maxscore = .0;

            /****************/
            for (unsigned CDi = 0; CDi < vSegmentWords.size(); CDi++) {
                //fprintf(candidatesout, " %s", vSegmentWords[i].c_str());

                if (vFinalScores[CDi] > maxscore) {
                    indexMax = CDi;
                    maxscore = vFinalScores[CDi];
                }
                if (vSegmentWords[CDi].compare(currentRefWord) == 0) {
                    isWordInCandidates = 1;
                }
            }
            thismaxscore = maxscore;
            /****************/


            if (debug)
                cout << "<< MaxScore " << maxscore << " Index " << indexMax << " :: " << vSegmentWords[indexMax] << ">>";
            string topCandidate = vSegmentWords[indexMax];
            topCandidateX = vSegmentWords[indexMax];
            if (topCandidate.compare(currentRefWord) == 0)
                isWordTopCandidate = 1;

            currentalign++;
            NullPost = 0;

            /* **** Get ranking ref word ***** */
            vector<double> vSortFinalScores(vFinalScores);
            std::sort(vSortFinalScores.begin(), vSortFinalScores.end(), std::greater<double>());
            for (unsigned iCandidate = 0; iCandidate < vSegmentWords.size(); iCandidate++) {
                /* When refWord is found*/
                if (vSegmentWords[iCandidate].compare(currentRefWord) == 0) {
                    //cout << "Found ref " << currentRefWord << " " << iCandidate << endl;
                    /* check what's its rank in sorted vector */
                    for (unsigned jSorted = 0; jSorted < vSortFinalScores.size(); jSorted++) {
                        if (fabs(vFinalScores[iCandidate] == vSortFinalScores[jSorted])) {
                            rankRefWord = jSorted;
                        }
                    }
                }
            }

            unsigned maxRankSearch = 13;
            File candidatesout(writeCandidates, "a");
            fprintf(candidatesout, "%lf %lf", tS, tE);
            fprintf(cnout1, "align %u", (numalign));
            numalign = numalign + 1;
            for (unsigned iCandidate = 0; iCandidate < vSegmentWords.size(); iCandidate++) {
                /* check if it's in first ranks in sorted vector */
                Boolean candidateNotFound = true;
                unsigned jSorted = 0;
                while (candidateNotFound) {
                    if (vFinalScores[iCandidate] == vSortFinalScores[jSorted]) {
                        if (vFinalScores[iCandidate] > 0) {
                            fprintf(candidatesout, " %s", vSegmentWords[iCandidate].c_str());
                            fprintf(cnout1, " %s %f", vSegmentWords[iCandidate].c_str(), vFinalScores[iCandidate]);
                        }
                        candidateNotFound = false;
                    }
                    ++jSorted;
                    if (jSorted >= maxRankSearch) {
                        candidateNotFound = false;
                    }
                }
            }
            fprintf(candidatesout, "\n");
            fprintf(cnout1, "\n");

        }

        File analysisout(writeAnalysis1, "a");
        unsigned numCandidates = vSegmentWords.size();
        fprintf(analysisout, "%s %s %u %u %u %u\n", currentRefWord.c_str(), topCandidateX.c_str(), isWordInCandidates, isWordTopCandidate, numCandidates, rankRefWord);
    }

    /* Compose CN file */
    File cnout(outCNfilename, "a");
    fprintf(cnout, "name cut.mfc\n");
    fprintf(cnout, "numaligns %u\n", (numalign + 1));
    fprintf(cnout, "posterior 1\n");
    fprintf(cnout, "align 0 <s> 1\n");

    cnout1.close();
    ifstream cnFile("/home/guerrero/tmpfilex");
    string cnline;
    while (getline(cnFile, cnline)) {
        if (!cnline.length() || cnline[0] == '#')
            continue;
        fprintf(cnout, "%s\n", cnline.c_str());
    }

    fprintf(cnout, "align %u </s> 1", (numalign));
    cnout.close();
}


/* Segment Validation INC: Use previously identified boundaries (shifted or added)
 * located at vBoundariesTests
 * to delimitate temporal segments, and validate them. */
void validateSegmentsInc(short numinc) {
    ostringstream fileNameStream("");        
    fileNameStream << prefixout << "inc" << numinc << ".txt";
    string outfilename = fileNameStream.str();        
    File hypshiftfile(outfilename.c_str(), "a");
        
    unsigned numalign = 1;
    Boolean isSearchContinuing = true;
    unsigned boundStart = 0, boundEnd = 0, currentalign = 1;
    float NullPost = 1;
    if (debug)
        cout << "Validating temporal segments [Boundaries: " << vBoundariesTests.size() << "]" << endl;
    if (debugx) {
        cout << "[";
        for (short jbound=0; jbound<vBoundariesTests.size(); jbound++){
            cout << vBoundariesTests[jbound] << " ";
        }
        cout << "]" << endl;
    }

    /* =============================================== */
    while (isSearchContinuing) {
        boundEnd = boundStart;
        NullPost = 1;
        vector<unsigned> vMicid;
        vector<double> vPosts;
        vector<string> vWords, vSegmentWords;
        vector<double> vFinalScores;

        while ((NullPost > THRSNULL) && (boundEnd < (vBoundariesTests.size() - 1))) {
            vFinalScores.clear();
            vSegmentWords.clear();
            vMicid.clear();
            vPosts.clear();
            vWords.clear();

            boundEnd = boundEnd + 1;
            float tS = vBoundariesTests[boundStart], tE = vBoundariesTests[boundEnd], DeltaT = 0.01;
            //For TypeDelta=3 (previous version)
            if ((tE - tS) < (deltaref)) {
                DeltaT = ((tE - tS) / 2);
            } else {
                DeltaT = ((tE - tS) * lratio);
            }

            float WLen = (tE + DeltaT) - max((float) 0.0, (tS - DeltaT));
            if (debug)
                cout << endl << "Checking boundaries " << tS << "," << tE << "(" << DeltaT << ") ";

            /* Start link selection */
            ifstream inputFile(tmpDataFile);
            string line;
            vector<float> vStartTime, vEndTime;

            /* Select links accomplishing the boundary constraints */
            while (getline(inputFile, line)) {
                if (!line.length() || line[0] == '#')
                    continue;
                unsigned numLatNode = 0, idMic = 0;
                char cWordlink[20];
                string sWordlink;
                float startTime = 0., endTime = 0.;
                double postProb = 0.;
                
                sscanf(line.c_str(), "%u %19s %f %f %lf %u", &numLatNode, cWordlink, &startTime, &endTime, &postProb, &idMic);
                sWordlink = (string) cWordlink;

                /* Ommit <s> </s> in octave, here is equivalent to NULLs*/
                if ((sWordlink.find("<s>") != std::string::npos) | (sWordlink.find("NULL") != std::string::npos)) {
                   // ..
                } else {
                    Boolean startIsValid = false, endIsValid = false;
                    startIsValid = ((startTime >= (tS - DeltaT)) && (startTime <= (tS + DeltaT)));
                    endIsValid = ((endTime <= (tE + DeltaT)) && (endTime >= (tE - DeltaT)));
                    
                    if (startIsValid && endIsValid) {
                        vPosts.push_back(postProb);
                        vWords.push_back(sWordlink);
                        vSegmentWords.push_back(sWordlink);
                        vMicid.push_back(idMic);
                    }
                }
            }
            /* If links were found ... */
            if (vPosts.size() > 0) {
                if (debug)
                    cout << "  Found " << vPosts.size() << " links.." << endl;
                /* Get list of unique occurrence of words */
                std::sort(vSegmentWords.begin(), vSegmentWords.end());
                vSegmentWords.erase(std::unique(vSegmentWords.begin(), vSegmentWords.end()), vSegmentWords.end());

                /*  DISPLAY  */
                if (debug) {
                    cout << "   Unique words: ";
                    for (unsigned i = 0; i < vSegmentWords.size(); i++) {
                        cout << vSegmentWords[i] << " ";
                    }
                    cout << endl << "   ";
                }

                unsigned numWords = vSegmentWords.size();

                vector<vector<double> > mWordsMics;

                /* Per each word */
                for (unsigned w = 0; w < numWords; w++) {
                    vector<unsigned> vIndex;
                    string currentWord = vSegmentWords[w];
                    for (unsigned i = 0; i < vWords.size(); i++) {
                        if (vWords[i] == currentWord) {
                            vIndex.push_back(i);
                        }
                    }

                    /* Per each microphone */
                    vector<double> vMics;
                    for (unsigned mic = 0; mic < numMics; mic++) {

                        vector<unsigned> vIndexMic;
                        for (unsigned selectedlink = 0; selectedlink < vIndex.size(); selectedlink++) {
                            if (vMicid[vIndex[selectedlink]] == mic) {
                                vIndexMic.push_back(vIndex[selectedlink]);
                            }
                        }

                        /* IntraMic Score */
                        double Postscore = .0;
                        if (vIndexMic.size() > 0) {
                            for (unsigned i = 0; i < vIndexMic.size(); i++) {
                                Postscore = Postscore + vPosts[vIndexMic[i]];
                                //cout << vPosts[vIndexMic[i]] << " ";
                            }
                        } else {
                            //Postscore = Postscore + 0;
                            //cout << "P0 ";
                        }
                        vMics.push_back(Postscore);
                    }
                    mWordsMics.push_back(vMics);
                }

                /* InterMic Score */
                vector<double> vNewPost;
                double sumvNewPost = .0, sumvNewPost2 = .0;
                for (unsigned wrow = 0; wrow < mWordsMics.size(); wrow++) {
                    double newPost = .0;
                    double maxscoreInterM = 0.0;
                    for (unsigned mic = 0; mic < numMics; mic++) {
                        newPost = newPost + mWordsMics[wrow][mic];
                        if (mWordsMics[wrow][mic] > maxscoreInterM) {
                            maxscoreInterM = mWordsMics[wrow][mic];
                        }
                    }
                    
                    double interMicWordScore = 0.0;
                    switch (scoremethod) {
                        case 1:
                            /* AVG InterMic */
                            interMicWordScore = (newPost / numMics);
                            break;
                        case 2:
                            /* MAX among all mics */
                            interMicWordScore = maxscoreInterM; //Use maxscore instead of AvgInterMic
                            break;
                    }
                
                    vNewPost.push_back(interMicWordScore);
                    sumvNewPost = sumvNewPost + (interMicWordScore);
                    //cout << interMicWordScore << " " ;
                }
                //cout << endl;
                if (debug) {
                    cout << "   Final Scores ";
                    cout << "[vNewPost size " << vNewPost.size() << "] ";
                    cout << " [alpha*sumvNewPost " << alpha * sumvNewPost << "] ";
                }
                if ((alpha * sumvNewPost) > 1) {
                    for (unsigned i = 0; i < vNewPost.size(); i++) {
                        vFinalScores.push_back(vNewPost[i] / (sumvNewPost));
                        sumvNewPost2 += (vNewPost[i] / (sumvNewPost));
                        if (debug)
                            cout << (vNewPost[i] / (sumvNewPost)) << " ";
                    }
                } else {
                    for (unsigned i = 0; i < vNewPost.size(); i++) {
                        vFinalScores.push_back(vNewPost[i]);
                        sumvNewPost2 += vNewPost[i];
                        //cout << vFinalScores[i] << " " ;
                        if (debug)
                            cout << "PPw " << (vNewPost[i]) << " ";
                    }
                }

                /* Criterion to continue/stop search of segment */
                NullPost = 1 - sumvNewPost2;

            } else {
                NullPost = 1;
                if (debug)
                    cout << "  Found no valid links" << endl;
            }
            
            if (debug) cout << "((NullP " << NullPost << "))";

        }
        
        if ((NullPost <= THRSNULL) & (vPosts.size() > 0)) {
            /* Add NULL word and score to stack of candidates */
            vFinalScores.push_back(NullPost);
            vSegmentWords.push_back("NULL");
            if (debug) {
                cout << endl << "[Segm: " << vBoundariesTests[boundStart] << " " << vBoundariesTests[boundEnd] << "]" << endl;
                cout << "[FScore size " << vFinalScores.size() << "] ";
            }            

            /* Find maximum candidate score */
            int indexMax = vSegmentWords.size() - 1;
            double maxscore = .0;
            /* =============================================== */
            for (unsigned i = 0; i < vSegmentWords.size(); i++) {
                if (vFinalScores[i] > maxscore) {
                    indexMax = i;
                    maxscore = vFinalScores[i];
                }
            }
            if (debug)
                cout << "<< MaxScore " << maxscore << " Index " << indexMax << " :: " << vSegmentWords[indexMax] << ">>";
            vHypothesis.push_back(vSegmentWords[indexMax]);
            currentalign++;
            NullPost = 0;
            boundStart = boundEnd; //Update next segment end-boundary
            numalign++;
        }

        if (boundEnd >= (vBoundariesTests.size() - 1)) {//Update next valid segment start-boundary
            boundStart++;
        }

        if (boundStart > (vBoundariesTests.size() - 2)) {//End search due to Boundary exceeded
            isSearchContinuing = false;
        }
    }

    
    if (debug) cout << endl << "(H:";
    ostringstream wholehypothesis("");
    for (unsigned i = 0; i < vHypothesis.size(); i++) {
        string hypword = vHypothesis[i];
        if (!(hypword.compare("NULL") == 0)) {
            wholehypothesis << hypword << " ";
            //if (debug) cout << hypword << " ";
        } else {
            //wholehypothesis << "sil ";
        }
        if (debug) cout << hypword << " ";
    }
    string swholehypothesis = wholehypothesis.str();
    fprintf(hypshiftfile, "%s\n", swholehypothesis.c_str());
    if (debugx) cout << swholehypothesis.c_str() << endl;
    if (debug) cout << ")" << endl;
    vHypothesis.clear();
    hypshiftfile.close();

}

/* Remove boundaries, and update boundaries for segmentValidation */
void forceSegmentsWrdSilRem0Bounds() {
    cout << "****** Removals to apply: 0 ********" << endl;
    /* Update vBounds */
    vBoundariesTests.clear();
    for (short iVUpdate = 0; iVUpdate < vBoundaries.size(); iVUpdate++) {
        vBoundariesTests.push_back(vBoundaries[iVUpdate]);
    }

    /* Run SegmentValidation with vBoundariesTests */
    validateSegmentsInc(0);
}
void forceSegmentsWrdSilRem1Bounds() {
    cout << "****** Removals to apply: 1 ********" << endl;

    for (short iSkips = 0; iSkips < (vBoundaries.size()); iSkips++) {
        if (debug) cout << "Removal: " << iSkips << " " << vBoundaries[iSkips];        
        
        /* Duplicate vector of boundaries skipping iSkip */
        vector<float> vBoundShifted;
        if (debug) cout << "[ " ;
        for (short iBoundcopy = 0; iBoundcopy < vBoundaries.size(); iBoundcopy++) {
            if (!(iBoundcopy == iSkips)){
                vBoundShifted.push_back(vBoundaries[iBoundcopy]);
                if (debug) cout << vBoundaries[iBoundcopy] << " ";
            }
        }
        if (debug) cout << "]" << endl;
        /* Sort added boundaries */
        std::sort(vBoundShifted.begin(), vBoundShifted.end());
        
        /* Update vBounds */
        vBoundariesTests.clear();
        for (short iVUpdate=0; iVUpdate<vBoundShifted.size(); iVUpdate++){
            vBoundariesTests.push_back(vBoundShifted[iVUpdate]);
        }
        
        /* Run SegmentValidation with vBoundariesTests */
        validateSegmentsInc(1);
     
    } /* Continue to next adding step ...  */
}
void forceSegmentsWrdSilRem2Bounds() {   
    short numremoval=2;
    cout << "****** Removals to apply: " << numremoval << " ********" << endl;

    for (short iSkips = 0; iSkips < (vBoundaries.size()-numremoval+1); iSkips++) {
        if (debug) cout << "Removal: " << iSkips << " " << vBoundaries[iSkips];        
        
        /* Duplicate vector of boundaries skipping iSkip */
        vector<float> vBoundShifted;
        vector<short> vIndexOriginal;
        if (debug) cout << "[ " ;
        for (short iBoundcopy = 0; iBoundcopy < vBoundaries.size(); iBoundcopy++) {
            if (! ((iBoundcopy == iSkips) | (iBoundcopy == iSkips+1)) ){
                vBoundShifted.push_back(vBoundaries[iBoundcopy]);
                vIndexOriginal.push_back(iBoundcopy);
                if (debug) cout << vBoundaries[iBoundcopy] << " ";
            }
        }
        if (debug) cout << "]" << endl;
        /* Sort added boundaries */
        std::sort(vBoundShifted.begin(), vBoundShifted.end());
        
        /* Update vBounds */
        if (debugx) cout << "[NewB:";
        vBoundariesTests.clear();
        for (short iVUpdate=0; iVUpdate<vBoundShifted.size(); iVUpdate++){
            vBoundariesTests.push_back(vBoundShifted[iVUpdate]);
            if (debugx) cout << vBoundShifted[iVUpdate] << " ";
        }
        if (debugx) cout << "]"<< endl;
        
        /* Run SegmentValidation with vBoundariesTests */
        validateSegmentsInc(numremoval);
        
    } /* Continue to next shifting step ...  */
}
void forceSegmentsWrdSilRem3Bounds() {   
    short numremoval=3;
    cout << "****** Removals to apply: " << numremoval << " ********" << endl;

    for (short iSkips = 0; iSkips < (vBoundaries.size()-numremoval+1); iSkips++) {
        if (debug) cout << "Removal: " << iSkips << " " << vBoundaries[iSkips];        
        
        /* Duplicate vector of boundaries skipping iSkip */
        vector<float> vBoundShifted;
        vector<short> vIndexOriginal;
        if (debug) cout << "[ " ;
        for (short iBoundcopy = 0; iBoundcopy < vBoundaries.size(); iBoundcopy++) {
            if (! ((iBoundcopy == iSkips) | (iBoundcopy == iSkips+1) | (iBoundcopy == iSkips+2)) ){            
                vBoundShifted.push_back(vBoundaries[iBoundcopy]);
                if (debug) cout << vBoundaries[iBoundcopy] << " ";
            }
        }
        if (debug) cout << "]" << endl;
        /* Sort added boundaries */
        std::sort(vBoundShifted.begin(), vBoundShifted.end());
        
        /* Update vBounds */
        if (debugx) cout << "[NewB:";
        vBoundariesTests.clear();
        for (short iVUpdate=0; iVUpdate<vBoundShifted.size(); iVUpdate++){
            vBoundariesTests.push_back(vBoundShifted[iVUpdate]);
            if (debugx) cout << vBoundShifted[iVUpdate] << " ";
        }
        if (debugx) cout << "]"<< endl;
        
        /* Run SegmentValidation with vBoundariesTests */
        validateSegmentsInc(numremoval);
        
    } /* Continue to next shifting step ...  */
}
void forceSegmentsWrdSilRem4Bounds() {   
    short numremoval=4;
    cout << "****** Removals to apply: " << numremoval << " ********" << endl;

    for (short iSkips = 0; iSkips < (vBoundaries.size()-numremoval+1); iSkips++) {
        if (debug) cout << "Removal: " << iSkips << " " << vBoundaries[iSkips];        
        
        /* Duplicate vector of boundaries skipping iSkip */
        vector<float> vBoundShifted;
        vector<short> vIndexOriginal;
        if (debug) cout << "[ " ;
        for (short iBoundcopy = 0; iBoundcopy < vBoundaries.size(); iBoundcopy++) {
            if (! ((iBoundcopy == iSkips) | (iBoundcopy == iSkips+1) | (iBoundcopy == iSkips+2) | (iBoundcopy == iSkips+3) ) ){            
                vBoundShifted.push_back(vBoundaries[iBoundcopy]);
                if (debug) cout << vBoundaries[iBoundcopy] << " ";
            }
        }
        if (debug) cout << "]" << endl;
        /* Sort added boundaries */
        std::sort(vBoundShifted.begin(), vBoundShifted.end());
        
        /* Update vBounds */
        if (debugx) cout << "[NewB:";
        vBoundariesTests.clear();
        for (short iVUpdate=0; iVUpdate<vBoundShifted.size(); iVUpdate++){
            vBoundariesTests.push_back(vBoundShifted[iVUpdate]);
            if (debugx) cout << vBoundShifted[iVUpdate] << " ";
        }
        if (debugx) cout << "]"<< endl;
        
        /* Run SegmentValidation with vBoundariesTests */
        validateSegmentsInc(numremoval);
        
    } /* Continue to next shifting step ...  */
}
/* Increase boundaries, and update boundaries for segmentValidation */
void forceSegmentsWrdSilInc0Bounds() {
    cout << "****** Increments to apply: 0 ********" << endl;
    /* Update vBounds */
    vBoundariesTests.clear();
    for (short iVUpdate = 0; iVUpdate < vBoundaries.size(); iVUpdate++) {
        vBoundariesTests.push_back(vBoundaries[iVUpdate]);
    }

    /* Run SegmentValidation with vBoundariesTests */
    validateSegmentsInc(0);
}
void forceSegmentsWrdSilInc1Bounds() {
    cout << "****** Increments to apply: 1 ********" << endl;

    for (short iSkips = 0; iSkips < (vBoundaries.size()-1); iSkips++) {
        if (debug) cout << "Increment: " << iSkips << " " << vBoundaries[iSkips];        
        
        /* Duplicate vector of boundaries skipping iSkip */
        vector<float> vBoundShifted;
        if (debug) cout << "[ " ;
        for (short iBoundcopy = 0; iBoundcopy < vBoundaries.size(); iBoundcopy++) {
            if ((iBoundcopy == iSkips)){
                vBoundShifted.push_back(vBoundaries[iBoundcopy]);
                if (debug) cout << vBoundaries[iBoundcopy] << " ";
                double nextBoundary=vBoundaries[iBoundcopy+1];
                double addBoundary= vBoundaries[iBoundcopy] + (( nextBoundary-vBoundaries[iBoundcopy] )/2);
                vBoundShifted.push_back(addBoundary);
                if (debug) cout << addBoundary << " ";
            } else {
                vBoundShifted.push_back(vBoundaries[iBoundcopy]);
                if (debug) cout << vBoundaries[iBoundcopy] << " ";
            }
        }
        if (debug) cout << "]" << endl;
        /* Sort added boundaries */
        std::sort(vBoundShifted.begin(), vBoundShifted.end());
        
        /* Update vBounds */
        vBoundariesTests.clear();
        for (short iVUpdate=0; iVUpdate<vBoundShifted.size(); iVUpdate++){
            vBoundariesTests.push_back(vBoundShifted[iVUpdate]);
        }
        
        /* Run SegmentValidation with vBoundariesTests */
        validateSegmentsInc(1);
     
    } /* Continue to next adding step ...  */
}
void forceSegmentsWrdSilIncNBounds(short numinc) {
    cout << "****** Increments to apply: " << numinc << " ********" << endl;

    for (short iSkips = 0; iSkips < (vBoundaries.size()-numinc); iSkips++) {
        if (debug) cout << "Increment: " << iSkips << " " << vBoundaries[iSkips];        
        
        /* Duplicate vector of boundaries skipping iSkip */
        vector<float> vBoundShifted;
        vector<short> vIndexOriginal;
        if (debug) cout << "[ " ;
        for (short iBoundcopy = 0; iBoundcopy < vBoundaries.size(); iBoundcopy++) {
             vBoundShifted.push_back(vBoundaries[iBoundcopy]);
             vIndexOriginal.push_back(iBoundcopy);
             if (debug) cout << vBoundaries[iBoundcopy] << " ";
             if ((iBoundcopy == iSkips)){
                for (short itoadd=0; itoadd<numinc; itoadd++){
                    double curBoundary=vBoundaries[iBoundcopy+itoadd];
                    double nextBoundary=vBoundaries[iBoundcopy+1+itoadd];
                    double addBoundary= curBoundary + (( nextBoundary-curBoundary )/2);
                    vBoundShifted.push_back(addBoundary); if (debug) cout << addBoundary << " ";
                }
             }
        }
        if (debug) cout << "]" << endl;
        /* Sort added boundaries */
        std::sort(vBoundShifted.begin(), vBoundShifted.end());
        
        /* Update vBounds */
        if (debug==4) cout << "[NewB:";
        vBoundariesTests.clear();
        for (short iVUpdate=0; iVUpdate<vBoundShifted.size(); iVUpdate++){
            vBoundariesTests.push_back(vBoundShifted[iVUpdate]);
            if (debug==4) cout << vBoundShifted[iVUpdate] << " ";
        }
        if (debug==4) cout << "]"<< endl;
        
        /* Run SegmentValidation with vBoundariesTests */
        validateSegmentsInc(numinc);
        
    } /* Continue to next shifting step ...  */
}

/* Explicitly use wrd time boundary couples to extract ConfSets */
/* Increasing 0-3 boundaries at a time */
void forceSegmentsWrdSilInc0() {
    unsigned numalign = 1;
    float NullPost = 1;    

    vector<double> vMaxScoresCN; //vector to keep PP of maxScore per CSet, just for display purposes

    cout << "****** Increase: NONE ******" << endl;
    ostringstream fileNameStream("");
    fileNameStream << prefixout << "inc0.txt";
    string outfilename = fileNameStream.str();
    File hypshiftfile(outfilename.c_str(), "a");

    /* Duplicate vector of boundaries no increment */
    vector<float> vBoundShifted;
    vector<short> vIndexOriginal;
    if (debug) cout << "[ ";
    for (short iBoundcopy = 0; iBoundcopy < vBoundaries.size(); iBoundcopy++) {
        vBoundShifted.push_back(vBoundaries[iBoundcopy]);
        vIndexOriginal.push_back(iBoundcopy);
        if (debug) cout << vBoundaries[iBoundcopy] << " ";
    }
    if (debug) cout << "]" << endl;
        
    /* Go through set of boundaries, getting hypothesis */
    for (short iCoupleBound = 0; iCoupleBound < (vBoundShifted.size() - 1); iCoupleBound++) {
        string sCurrentRefword = vRefwords[vIndexOriginal[iCoupleBound]];
        NullPost = 1;
        vector<unsigned> vMicid;
        vector<double> vPosts;
        vector<string> vWords, vSegmentWords;
        vector<double> vFinalScores;

        float tS = vBoundShifted[iCoupleBound], tE = vBoundShifted[iCoupleBound + 1], DeltaT = 0.01;
        if (debug)
            cout << "(t:" << tS << " " << tE << "):";

        /* Start link selection */
        ifstream inputFile(tmpDataFile);
        string line;

        /* Select links accomplishing the boundary constraints */
        while (getline(inputFile, line)) {
            if (!line.length() || line[0] == '#')
                continue;
            unsigned numLatNode = 0, idMic = 0;
            char cWordlink[20];
            string sWordlink;
            float startTime = 0., endTime = 0.;
            double postProb = 0.;

            sscanf(line.c_str(), "%u %19s %f %f %lf %u", &numLatNode, cWordlink, &startTime, &endTime, &postProb, &idMic);
            sWordlink = (string) cWordlink;

            /* Ommit <s> </s> in octave, here is equivalent to NULLs*/
            if (!((sWordlink.find("<s>") != std::string::npos) | (sWordlink.find("NULL") != std::string::npos) | (sWordlink.find("</s>") != std::string::npos))) {
                Boolean startIsValid = false, endIsValid = false;

                /* Validate Start/End +-Delta boundaries */
                float midpoint = startTime + ((endTime - startTime) / 2.0);
                switch (boundsvalidation) {
                    case 1:
                        // Validate Start/End with separate deltas */
                        startIsValid = ((startTime >= (tS - manualdeltaS)) && (startTime <= (tS + manualdeltaS)));
                        endIsValid = ((endTime <= (tE + manualdeltaE)) && (endTime >= (tE - manualdeltaE)));
                        break;
                    case 2:
                        // Use MIDPOINT at a deltatolerance from Start/End */                        
                        if ((midpoint >= tS & midpoint <= tE) | (fabs(midpoint - tS) <= manualdelta) | (fabs(midpoint - tE) <= manualdelta)) {
                            startIsValid = true;
                            endIsValid = true;
                        }
                        break;
                    case 0:
                        // Validate if Start/End are at a DeltaT from boundaries */
                        if ((tE - tS) < (deltaref)) {
                            DeltaT = ((tE - tS) / 2);
                        } else {
                            DeltaT = ((tE - tS) * lratio);
                        }
                        startIsValid = ((startTime >= (tS - DeltaT)) && (startTime <= (tS + DeltaT)));
                        endIsValid = ((endTime <= (tE + DeltaT)) && (endTime >= (tE - DeltaT)));
                        break;
                }

                if (startIsValid && endIsValid) {
                    vPosts.push_back(postProb);
                    vWords.push_back(sWordlink);
                    vSegmentWords.push_back(sWordlink);
                    vMicid.push_back(idMic);
                    if (debug > 1) cout << "l[" << sWordlink << " " << startTime << "," << endTime << " " << postProb << " " << idMic << "] ";
                }
            }
        }
        /* If links were found ... compute avgInterMic(sumIntraMic(PP)) */
        if (vWords.size() > 0) {
            if (debug)
                cout << "  Found " << vPosts.size() << " links.." << endl;
            /* Get list of unique occurrence of words */
            std::sort(vSegmentWords.begin(), vSegmentWords.end());
            vSegmentWords.erase(std::unique(vSegmentWords.begin(), vSegmentWords.end()), vSegmentWords.end());

            /* ****************** DISPLAY ***************** */
            if (debug) {
                cout << "   Unique words: ";
                for (unsigned i = 0; i < vSegmentWords.size(); i++) {
                    cout << vSegmentWords[i] << " ";
                }
                cout << endl << "   ";
            }

            /* ======================================== */
            unsigned numWords = vSegmentWords.size();

            vector<vector<double> > mWordsMics;
            short sIndexRefword = -1;

            /* Per each word */
            for (unsigned w = 0; w < numWords; w++) {
                vector<unsigned> vIndex;
                string currentWord = vSegmentWords[w];
                if (currentWord.compare(sCurrentRefword) == 0) {
                    /* Keep index of refWord in list of words   */
                    /* to later extract PP from InterMic matrix */
                    sIndexRefword = w;
                }
                for (unsigned i = 0; i < vWords.size(); i++) {
                    if (vWords[i] == currentWord) {
                        vIndex.push_back(i);
                    }
                }

                /* Per each microphone */
                vector<double> vMics;
                for (unsigned mic = 0; mic < numMics; mic++) {
                    /* Get indexes of links of current Mic */ //cout << "<Mic" << mic << " ";
                    vector<unsigned> vIndexMic;
                    for (unsigned selectedlink = 0; selectedlink < vIndex.size(); selectedlink++) {
                        if (vMicid[vIndex[selectedlink]] == mic) {
                            vIndexMic.push_back(vIndex[selectedlink]);
                        }
                    }

                    /* IntraMic Score */
                    double Postscore = .0;
                    if (vIndexMic.size() > 0) {
                        for (unsigned i = 0; i < vIndexMic.size(); i++) {
                            Postscore = Postscore + vPosts[vIndexMic[i]];
                        }
                    }
                    vMics.push_back(Postscore);
                }
                mWordsMics.push_back(vMics);
            }

            /* InterMic Score */
            vector<double> vNewPost;
            double sumvNewPost = .0, sumvNewPost2 = .0;
            for (unsigned wrow = 0; wrow < mWordsMics.size(); wrow++) {
                double newPost = .0;

                double maxscoreInterM = 0;
                for (unsigned mic = 0; mic < numMics; mic++) {
                    newPost = newPost + mWordsMics[wrow][mic];
                    if (mWordsMics[wrow][mic] > maxscoreInterM) {
                        maxscoreInterM = mWordsMics[wrow][mic];
                    }
                }
                double interMicWordScore = 0;
                switch (scoremethod) {
                    case 1:
                        /* AVG InterMic */
                        interMicWordScore = (newPost / numMics);
                        break;
                    case 2:
                        /* MAX among all mics */
                        interMicWordScore = maxscoreInterM; //Use maxscore instead of AvgInterMic
                        break;
                }
                vNewPost.push_back(interMicWordScore);
                sumvNewPost = sumvNewPost + (interMicWordScore);
            }

            /* Normalization if sumPP of all candidates is >1 */
            if (debug) {
                cout << " Final Scores ";
                cout << "[vNewPost size " << vNewPost.size() << "] ";
                cout << " [alpha*sumvNewPost " << alpha * sumvNewPost << "] ";
            }
            if ((alpha * sumvNewPost) > 1) {
                for (unsigned i = 0; i < vNewPost.size(); i++) {
                    vFinalScores.push_back(vNewPost[i] / (sumvNewPost));
                    sumvNewPost2 += (vNewPost[i] / (sumvNewPost));
                    if (debug)
                        cout << (vNewPost[i] / (sumvNewPost)) << " ";
                }
            } else {
                for (unsigned i = 0; i < vNewPost.size(); i++) {
                    vFinalScores.push_back(vNewPost[i]);
                    sumvNewPost2 += vNewPost[i];
                    //cout << vFinalScores[i] << " " ;
                    if (debug)
                        cout << "PPw " << (vNewPost[i]) << " ";
                }
            }

            /* Criterion to continue/stop search of segment */
            NullPost = 1 - sumvNewPost2;
        } else {
            NullPost = 1;
            if (debug)
                cout << "  Found no valid links in this segment." << endl;
            //                    /* Keep track of reference hypothesis scores */
        }

        if (debug)
            cout << " (NullPost " << NullPost << ") " << endl;

        /* Add NULL word and score to stack of candidates */
        vFinalScores.push_back(NullPost);
        vSegmentWords.push_back("NULL");

        /* Find maximum candidate score */
        int indexMax = vSegmentWords.size() - 1;
        double maxscore = .0;
        /* Compose CN file */
        // fprintf(cnTmpBody, "align %u", (numalign));
        for (unsigned i = 0; i < vSegmentWords.size(); i++) {
            if (vFinalScores[i] > maxscore) {
                indexMax = i;
                maxscore = vFinalScores[i];
            }
        }
        if (debug) {
            cout << "<< MaxScore " << maxscore << " Index " << indexMax << " :: " << vSegmentWords[indexMax] << ">>";
        }

        vHypothesis.push_back(vSegmentWords[indexMax]);
        vMaxScoresCN.push_back(vFinalScores[indexMax]);
        NullPost = 0;
        numalign++;

    }
    if (debug) cout << "(H: ";
    ostringstream wholehypothesis("");
    for (unsigned i = 0; i < vHypothesis.size(); i++) {
        string hypword = vHypothesis[i];
        if (!(hypword.compare("NULL") == 0)) {
            wholehypothesis << hypword << " ";
        } else {
            //wholehypothesis << "sil ";
        }
        if (debug) cout << hypword << " ";
    }
    string swholehypothesis = wholehypothesis.str();

    fprintf(hypshiftfile, "%s\n", swholehypothesis.c_str());
    if (debug) cout << ")" << endl;
    vHypothesis.clear();


    if (debug)
        cout << endl;
    
    hypshiftfile.close();    
}
void forceSegmentsWrdSilInc1() {
    unsigned numalign = 1;
    float NullPost = 1;    
    vector<double> vMaxScoresCN; //vector to keep PP of maxScore per CSet, just for display purposes
    cout << "****** Increments to apply: 1 ********" << endl;

    for (short iSkips = 0; iSkips < (vBoundaries.size()-1); iSkips++) {
        if (debug) cout << "Increment: " << iSkips << " " << vBoundaries[iSkips];
        ostringstream fileNameStream("");        
        fileNameStream << prefixout << "inc1.txt";
        string outfilename = fileNameStream.str();        
        File hypshiftfile(outfilename.c_str(), "a");
        
        /* Duplicate vector of boundaries skipping iSkip */
        vector<float> vBoundShifted;
        vector<short> vIndexOriginal;
        if (debug) cout << "[ " ;
        for (short iBoundcopy = 0; iBoundcopy < vBoundaries.size(); iBoundcopy++) {
            if ((iBoundcopy == iSkips)){
                vBoundShifted.push_back(vBoundaries[iBoundcopy]);
                vIndexOriginal.push_back(iBoundcopy);
                if (debug) cout << vBoundaries[iBoundcopy] << " ";
                double nextBoundary=vBoundaries[iBoundcopy+1];
                double addBoundary= vBoundaries[iBoundcopy] + (( nextBoundary-vBoundaries[iBoundcopy] )/2);
                vBoundShifted.push_back(addBoundary);
                if (debug) cout << addBoundary << " ";
            } else {
                vBoundShifted.push_back(vBoundaries[iBoundcopy]);
                vIndexOriginal.push_back(iBoundcopy);
                if (debug) cout << vBoundaries[iBoundcopy] << " ";
            }
        }
        if (debug) cout << "]" << endl;
        /* Sort added boundaries */
        std::sort(vBoundShifted.begin(), vBoundShifted.end());


        Boolean runthis=true;
        if (runthis) {
            /* Go through skipped set of boundaries, getting hypothesis */
        for (short iCoupleBound = 0; iCoupleBound < (vBoundShifted.size() - 1); iCoupleBound++) {
            
            //string sCurrentRefword = vRefwords[vIndexOriginal[iCoupleBound]];
            NullPost = 1;
            vector<unsigned> vMicid;
            vector<double> vPosts;
            vector<string> vWords, vSegmentWords;
            vector<double> vFinalScores;

            float tS = vBoundShifted[iCoupleBound], tE = vBoundShifted[iCoupleBound + 1], DeltaT = 0.01;
            if (debug)
                cout << "(t:" << tS << " " << tE << "):";

            /* Start link selection */
            ifstream inputFile(tmpDataFile);
            string line;

            /* Select links accomplishing the boundary constraints */
            while (getline(inputFile, line)) {
                if (!line.length() || line[0] == '#')
                    continue;
                unsigned numLatNode = 0, idMic = 0;
                char cWordlink[20];
                string sWordlink;
                float startTime = 0., endTime = 0.;
                double postProb = 0.;

                sscanf(line.c_str(), "%u %19s %f %f %lf %u", &numLatNode, cWordlink, &startTime, &endTime, &postProb, &idMic);
                sWordlink = (string) cWordlink;

                /* Ommit <s> </s> in octave, here is equivalent to NULLs*/
                /* || (sWordlink.compare(str2) == 0 */
                if (!((sWordlink.find("<s>") != std::string::npos) | (sWordlink.find("NULL") != std::string::npos) | (sWordlink.find("</s>") != std::string::npos))) {
                    Boolean startIsValid = false, endIsValid = false;

                    /* Validate Start/End +-Delta boundaries */
                    float midpoint = startTime + ((endTime - startTime) / 2.0);
                    switch (boundsvalidation) {
                        case 1:
                            // Validate Start/End with separate deltas */
                            startIsValid = ((startTime >= (tS - manualdeltaS)) && (startTime <= (tS + manualdeltaS)));
                            endIsValid = ((endTime <= (tE + manualdeltaE)) && (endTime >= (tE - manualdeltaE)));
                            break;
                        case 2:
                            // Use MIDPOINT at a deltatolerance from Start/End */                        
                            if ((midpoint >= tS & midpoint <= tE) | (fabs(midpoint - tS) <= manualdelta) | (fabs(midpoint - tE) <= manualdelta)) {
                                startIsValid = true;
                                endIsValid = true;
                            }
                            break;
                        case 0:
                            // Validate if Start/End are at a DeltaT from boundaries */
                            if ((tE - tS) < (deltaref)) {
                                DeltaT = ((tE - tS) / 2);
                            } else {
                                DeltaT = ((tE - tS) * lratio);
                            }
                            startIsValid = ((startTime >= (tS - DeltaT)) && (startTime <= (tS + DeltaT)));
                            endIsValid = ((endTime <= (tE + DeltaT)) && (endTime >= (tE - DeltaT)));
                            break;
                    }

                    if (startIsValid && endIsValid) {
                        vPosts.push_back(postProb);
                        vWords.push_back(sWordlink);
                        vSegmentWords.push_back(sWordlink);
                        vMicid.push_back(idMic);
                        if (debug > 1) cout << "l[" << sWordlink << " " << startTime << "," << endTime << " " << postProb << " " << idMic << "] ";
                    }
                }
            }
            /* If links were found ... compute avgInterMic(sumIntraMic(PP)) */
            if (vWords.size() > 0) {
                if (debug)
                    cout << "  Found " << vPosts.size() << " links.." << endl;
                /* Get list of unique occurrence of words */
                std::sort(vSegmentWords.begin(), vSegmentWords.end());
                vSegmentWords.erase(std::unique(vSegmentWords.begin(), vSegmentWords.end()), vSegmentWords.end());

                /* DISPLAY  */
                if (debug) {
                    cout << "   Unique words: ";
                    for (unsigned i = 0; i < vSegmentWords.size(); i++) {
                        cout << vSegmentWords[i] << " ";
                    }
                    cout << endl << "   ";
                }

                unsigned numWords = vSegmentWords.size();

                vector<vector<double> > mWordsMics;
                short sIndexRefword = -1;

                /* Per each word */
                for (unsigned w = 0; w < numWords; w++) {
                    vector<unsigned> vIndex;
                    string currentWord = vSegmentWords[w];
                    for (unsigned i = 0; i < vWords.size(); i++) {
                        if (vWords[i] == currentWord) {
                            vIndex.push_back(i);
                        }
                    }

                    /* Per each microphone */
                    vector<double> vMics;
                    for (unsigned mic = 0; mic < numMics; mic++) {
                        /* Get indexes of links of current Mic */ //cout << "<Mic" << mic << " ";
                        vector<unsigned> vIndexMic;
                        for (unsigned selectedlink = 0; selectedlink < vIndex.size(); selectedlink++) {
                            if (vMicid[vIndex[selectedlink]] == mic) {
                                vIndexMic.push_back(vIndex[selectedlink]);
                            }
                        }

                        /* IntraMic Score */
                        double Postscore = .0;
                        if (vIndexMic.size() > 0) {
                            for (unsigned i = 0; i < vIndexMic.size(); i++) {
                                Postscore = Postscore + vPosts[vIndexMic[i]];
                            }
                        }
                        vMics.push_back(Postscore);
                    }
                    mWordsMics.push_back(vMics);
                }

                /* InterMic Score */
                vector<double> vNewPost;
                double sumvNewPost = .0, sumvNewPost2 = .0;
                for (unsigned wrow = 0; wrow < mWordsMics.size(); wrow++) {
                    double newPost = .0;

                    double maxscoreInterM = 0;
                    for (unsigned mic = 0; mic < numMics; mic++) {
                        newPost = newPost + mWordsMics[wrow][mic];
                        if (mWordsMics[wrow][mic] > maxscoreInterM) {
                            maxscoreInterM = mWordsMics[wrow][mic];
                        }
                    }
                    double interMicWordScore = 0;
                    switch (scoremethod) {
                        case 1:
                            /* AVG InterMic */
                            interMicWordScore = (newPost / numMics);
                            break;
                        case 2:
                            /* MAX among all mics */
                            interMicWordScore = maxscoreInterM; //Use maxscore instead of AvgInterMic
                            break;
                    }
                    vNewPost.push_back(interMicWordScore);
                    sumvNewPost = sumvNewPost + (interMicWordScore);
                }

                /* Normalization if sumPP of all candidates is >1 */
                if (debug) {
                    cout << " Final Scores ";
                    cout << "[vNewPost size " << vNewPost.size() << "] ";
                    cout << " [alpha*sumvNewPost " << alpha * sumvNewPost << "] ";
                }
                if ((alpha * sumvNewPost) > 1) {
                    for (unsigned i = 0; i < vNewPost.size(); i++) {
                        vFinalScores.push_back(vNewPost[i] / (sumvNewPost));
                        sumvNewPost2 += (vNewPost[i] / (sumvNewPost));
                        if (debug)
                            cout << (vNewPost[i] / (sumvNewPost)) << " ";
                    }
                } else {
                    for (unsigned i = 0; i < vNewPost.size(); i++) {
                        vFinalScores.push_back(vNewPost[i]);
                        sumvNewPost2 += vNewPost[i];
                        //cout << vFinalScores[i] << " " ;
                        if (debug)
                            cout << "PPw " << (vNewPost[i]) << " ";
                    }
                }

                /* Criterion to continue/stop search of segment */
                NullPost = 1 - sumvNewPost2;

            } else {
                NullPost = 1;
                if (debug)
                    cout << "  Found no valid links in this segment." << endl;
            }

            if (debug)
                cout << " (NullPost " << NullPost << ") " << endl;

            /* Finally if Set is valid, add NULL to set */

            /* Add NULL word and score to stack of candidates */
            vFinalScores.push_back(NullPost);
            vSegmentWords.push_back("NULL");
            
            /* Find maximum candidate score */
            int indexMax = vSegmentWords.size() - 1;            
            double maxscore = .0;            
            /* Compose CN file */
            // fprintf(cnTmpBody, "align %u", (numalign));
            for (unsigned i = 0; i < vSegmentWords.size(); i++) {
                if (vFinalScores[i] > maxscore) {
                    indexMax = i;
                    maxscore = vFinalScores[i];
                }
            }

            if (debug) {
                cout << "<< MaxScore " << maxscore << " Index " << indexMax << " :: " << vSegmentWords[indexMax] << ">>";
            }

            vHypothesis.push_back(vSegmentWords[indexMax]);
            vMaxScoresCN.push_back(vFinalScores[indexMax]);
            NullPost = 0;
            numalign++;

        }
        if (debug)     cout << "(H" << iSkips << ": ";
        ostringstream wholehypothesis("");
        for (unsigned i = 0; i < vHypothesis.size(); i++) {
            string hypword = vHypothesis[i];
            if (!(hypword.compare("NULL") == 0)) {
                wholehypothesis << hypword << " ";
            } else {
                //wholehypothesis << "sil ";
            }
            if (debug) cout << hypword << " ";
        }
        string swholehypothesis = wholehypothesis.str();

        fprintf(hypshiftfile, "%s\n", swholehypothesis.c_str());
        if (debug)    cout << ")" << endl;
        vHypothesis.clear();

        if (debug)
            cout << endl;

        hypshiftfile.close();
        }
        
    } /* Continue to next shifting step ...  */
}
void forceSegmentsWrdSilInc2() {
    unsigned numalign = 1;
    float NullPost = 1;    
    vector<double> vMaxScoresCN; //vector to keep PP of maxScore per CSet, just for display purposes
    cout << "****** Increments to apply: 2 ********" << endl;

    for (short iSkips = 0; iSkips < (vBoundaries.size()-2); iSkips++) {
        if (debug) cout << "Increment: " << iSkips << " " << vBoundaries[iSkips];
        ostringstream fileNameStream("");        
        fileNameStream << prefixout << "inc2.txt";
        string outfilename = fileNameStream.str();        
        File hypshiftfile(outfilename.c_str(), "a");
        
        /* Duplicate vector of boundaries skipping iSkip */
        vector<float> vBoundShifted;
        vector<short> vIndexOriginal;
        if (debug) cout << "[ " ;
        for (short iBoundcopy = 0; iBoundcopy < vBoundaries.size(); iBoundcopy++) {
            if ((iBoundcopy == iSkips)){
                vBoundShifted.push_back(vBoundaries[iBoundcopy]);
                vIndexOriginal.push_back(iBoundcopy);
                if (debug) cout << vBoundaries[iBoundcopy] << " ";
                double nextBoundary=vBoundaries[iBoundcopy+1];
                double addBoundary= vBoundaries[iBoundcopy] + (( nextBoundary-vBoundaries[iBoundcopy] )/2);
                vBoundShifted.push_back(addBoundary); if (debug) cout << addBoundary << " ";
                
                nextBoundary=vBoundaries[iBoundcopy+2];
                addBoundary= vBoundaries[iBoundcopy+1] + (( nextBoundary-vBoundaries[iBoundcopy+1] )/2);
                vBoundShifted.push_back(addBoundary);  if (debug) cout << addBoundary << " "; 
            } else {
                vBoundShifted.push_back(vBoundaries[iBoundcopy]);
                vIndexOriginal.push_back(iBoundcopy);
                if (debug) cout << vBoundaries[iBoundcopy] << " ";
            }
        }
        if (debug) cout << "]" << endl;

        Boolean runthis=true;
        if (runthis) {
            /* Go through skipped set of boundaries, getting hypothesis */
        for (short iCoupleBound = 0; iCoupleBound < (vBoundShifted.size() - 1); iCoupleBound++) {
            
            //string sCurrentRefword = vRefwords[vIndexOriginal[iCoupleBound]];
            NullPost = 1;
            vector<unsigned> vMicid;
            vector<double> vPosts;
            vector<string> vWords, vSegmentWords;
            vector<double> vFinalScores;

            float tS = vBoundShifted[iCoupleBound], tE = vBoundShifted[iCoupleBound + 1], DeltaT = 0.01;
            if (debug)
                cout << "(t:" << tS << " " << tE << "):";

            /* Start link selection */
            ifstream inputFile(tmpDataFile);
            string line;

            /* Select links accomplishing the boundary constraints */
            while (getline(inputFile, line)) {
                if (!line.length() || line[0] == '#')
                    continue;
                unsigned numLatNode = 0, idMic = 0;
                char cWordlink[20];
                string sWordlink;
                float startTime = 0., endTime = 0.;
                double postProb = 0.;

                sscanf(line.c_str(), "%u %19s %f %f %lf %u", &numLatNode, cWordlink, &startTime, &endTime, &postProb, &idMic);
                sWordlink = (string) cWordlink;

                /* Ommit <s> </s> in octave, here is equivalent to NULLs*/
                /* || (sWordlink.compare(str2) == 0 */
                if (!((sWordlink.find("<s>") != std::string::npos) | (sWordlink.find("NULL") != std::string::npos) | (sWordlink.find("</s>") != std::string::npos))) {
                    Boolean startIsValid = false, endIsValid = false;

                    /* Validate Start/End +-Delta boundaries */
                    float midpoint = startTime + ((endTime - startTime) / 2.0);
                    switch (boundsvalidation) {
                        case 1:
                            // Validate Start/End with separate deltas */
                            startIsValid = ((startTime >= (tS - manualdeltaS)) && (startTime <= (tS + manualdeltaS)));
                            endIsValid = ((endTime <= (tE + manualdeltaE)) && (endTime >= (tE - manualdeltaE)));
                            break;
                        case 2:
                            // Use MIDPOINT at a deltatolerance from Start/End */                        
                            if ((midpoint >= tS & midpoint <= tE) | (fabs(midpoint - tS) <= manualdelta) | (fabs(midpoint - tE) <= manualdelta)) {
                                startIsValid = true;
                                endIsValid = true;
                            }
                            break;
                        case 0:
                            // Validate if Start/End are at a DeltaT from boundaries */
                            if ((tE - tS) < (deltaref)) {
                                DeltaT = ((tE - tS) / 2);
                            } else {
                                DeltaT = ((tE - tS) * lratio);
                            }
                            startIsValid = ((startTime >= (tS - DeltaT)) && (startTime <= (tS + DeltaT)));
                            endIsValid = ((endTime <= (tE + DeltaT)) && (endTime >= (tE - DeltaT)));
                            break;
                    }

                    if (startIsValid && endIsValid) {
                        vPosts.push_back(postProb);
                        vWords.push_back(sWordlink);
                        vSegmentWords.push_back(sWordlink);
                        vMicid.push_back(idMic);
                        if (debug > 1) cout << "l[" << sWordlink << " " << startTime << "," << endTime << " " << postProb << " " << idMic << "] ";
                    }
                }
            }
            /* If links were found ... compute avgInterMic(sumIntraMic(PP)) */
            if (vWords.size() > 0) {
                if (debug)
                    cout << "  Found " << vPosts.size() << " links.." << endl;
                /* Get list of unique occurrence of words */
                std::sort(vSegmentWords.begin(), vSegmentWords.end());
                vSegmentWords.erase(std::unique(vSegmentWords.begin(), vSegmentWords.end()), vSegmentWords.end());

                /* ****************** DISPLAY ***************** */
                if (debug) {
                    cout << "   Unique words: ";
                    for (unsigned i = 0; i < vSegmentWords.size(); i++) {
                        cout << vSegmentWords[i] << " ";
                    }
                    cout << endl << "   ";
                }

                unsigned numWords = vSegmentWords.size();

                vector<vector<double> > mWordsMics;
                short sIndexRefword = -1;

                /* Per each word */
                for (unsigned w = 0; w < numWords; w++) {
                    vector<unsigned> vIndex;
                    string currentWord = vSegmentWords[w];
                    for (unsigned i = 0; i < vWords.size(); i++) {
                        if (vWords[i] == currentWord) {
                            vIndex.push_back(i);
                        }
                    }

                    /* Per each microphone */
                    vector<double> vMics;
                    for (unsigned mic = 0; mic < numMics; mic++) {
                        /* Get indexes of links of current Mic */ //cout << "<Mic" << mic << " ";
                        vector<unsigned> vIndexMic;
                        for (unsigned selectedlink = 0; selectedlink < vIndex.size(); selectedlink++) {
                            if (vMicid[vIndex[selectedlink]] == mic) {
                                vIndexMic.push_back(vIndex[selectedlink]);
                            }
                        }

                        /* IntraMic Score */
                        double Postscore = .0;
                        if (vIndexMic.size() > 0) {
                            for (unsigned i = 0; i < vIndexMic.size(); i++) {
                                Postscore = Postscore + vPosts[vIndexMic[i]];
                            }
                        }
                        vMics.push_back(Postscore);
                    }
                    mWordsMics.push_back(vMics);
                }

                /* InterMic Score */
                vector<double> vNewPost;
                double sumvNewPost = .0, sumvNewPost2 = .0;
                for (unsigned wrow = 0; wrow < mWordsMics.size(); wrow++) {
                    double newPost = .0;

                    double maxscoreInterM = 0;
                    for (unsigned mic = 0; mic < numMics; mic++) {
                        newPost = newPost + mWordsMics[wrow][mic];
                        if (mWordsMics[wrow][mic] > maxscoreInterM) {
                            maxscoreInterM = mWordsMics[wrow][mic];
                        }
                    }
                    double interMicWordScore = 0;
                    switch (scoremethod) {
                        case 1:
                            /* AVG InterMic */
                            interMicWordScore = (newPost / numMics);
                            break;
                        case 2:
                            /* MAX among all mics */
                            interMicWordScore = maxscoreInterM; //Use maxscore instead of AvgInterMic
                            break;
                    }
                    vNewPost.push_back(interMicWordScore);
                    sumvNewPost = sumvNewPost + (interMicWordScore);
                }

                /* Normalization if sumPP of all candidates is >1 */
                if (debug) {
                    cout << " Final Scores ";
                    cout << "[vNewPost size " << vNewPost.size() << "] ";
                    cout << " [alpha*sumvNewPost " << alpha * sumvNewPost << "] ";
                }
                if ((alpha * sumvNewPost) > 1) {
                    for (unsigned i = 0; i < vNewPost.size(); i++) {
                        vFinalScores.push_back(vNewPost[i] / (sumvNewPost));
                        sumvNewPost2 += (vNewPost[i] / (sumvNewPost));
                        if (debug)
                            cout << (vNewPost[i] / (sumvNewPost)) << " ";
                    }
                } else {
                    for (unsigned i = 0; i < vNewPost.size(); i++) {
                        vFinalScores.push_back(vNewPost[i]);
                        sumvNewPost2 += vNewPost[i];
                        //cout << vFinalScores[i] << " " ;
                        if (debug)
                            cout << "PPw " << (vNewPost[i]) << " ";
                    }
                }

                /* Criterion to continue/stop search of segment */
                NullPost = 1 - sumvNewPost2;


            } else {
                NullPost = 1;
                if (debug)
                    cout << "  Found no valid links in this segment." << endl;
                //                    /* Keep track of reference hypothesis scores */
            }

            if (debug)
                cout << " (NullPost " << NullPost << ") " << endl;

            /* Finally if Set is valid, add NULL to set */

            /* Add NULL word and score to stack of candidates */
            vFinalScores.push_back(NullPost);
            vSegmentWords.push_back("NULL");
            
            /* Find maximum candidate score */
            int indexMax = vSegmentWords.size() - 1;            
            double maxscore = .0;            
            /* Compose CN file */
            // fprintf(cnTmpBody, "align %u", (numalign));
            for (unsigned i = 0; i < vSegmentWords.size(); i++) {
                if (vFinalScores[i] > maxscore) {
                    indexMax = i;
                    maxscore = vFinalScores[i];
                }
            }

            if (debug) {
                cout << "<< MaxScore " << maxscore << " Index " << indexMax << " :: " << vSegmentWords[indexMax] << ">>";
            }

            vHypothesis.push_back(vSegmentWords[indexMax]);
            vMaxScoresCN.push_back(vFinalScores[indexMax]);
            //            currentalign++;
            NullPost = 0;
            numalign++;
            //        }

        }
        if (debug)     cout << "(H" << iSkips << ": ";
        ostringstream wholehypothesis("");
        for (unsigned i = 0; i < vHypothesis.size(); i++) {
            string hypword = vHypothesis[i];
            if (!(hypword.compare("NULL") == 0)) {
                wholehypothesis << hypword << " ";
                //if (debug) cout << hypword << " ";
            } else {
                //wholehypothesis << "sil ";
            }
            if (debug) cout << hypword << " ";
        }
        string swholehypothesis = wholehypothesis.str();

        fprintf(hypshiftfile, "%s\n", swholehypothesis.c_str());
        if (debug)    cout << ")" << endl;
        vHypothesis.clear();

        if (debug)
            cout << endl;

        hypshiftfile.close();
        }
        
    } /* Continue to next shifting step ...  */
}
void forceSegmentsWrdSilInc3() {
    unsigned numalign = 1;
    float NullPost = 1;    
    vector<double> vMaxScoresCN; //vector to keep PP of maxScore per CSet, just for display purposes
    cout << "****** Increments to apply: 3 ********" << endl;

    for (short iSkips = 0; iSkips < (vBoundaries.size()-3); iSkips++) {
        if (debug) cout << "Increment: " << iSkips << " " << vBoundaries[iSkips];
        ostringstream fileNameStream("");        
        fileNameStream << prefixout << "inc3.txt";
        string outfilename = fileNameStream.str();        
        File hypshiftfile(outfilename.c_str(), "a");
        
        /* Duplicate vector of boundaries skipping iSkip */
        vector<float> vBoundShifted;
        vector<short> vIndexOriginal;
        if (debug) cout << "[ " ;
        for (short iBoundcopy = 0; iBoundcopy < vBoundaries.size(); iBoundcopy++) {
            if ((iBoundcopy == iSkips)){
                vBoundShifted.push_back(vBoundaries[iBoundcopy]);
                vIndexOriginal.push_back(iBoundcopy);
                if (debug) cout << vBoundaries[iBoundcopy] << " ";
                double nextBoundary=vBoundaries[iBoundcopy+1];
                double addBoundary= vBoundaries[iBoundcopy] + (( nextBoundary-vBoundaries[iBoundcopy] )/2);
                vBoundShifted.push_back(addBoundary); if (debug) cout << addBoundary << " ";
                
                nextBoundary=vBoundaries[iBoundcopy+2];
                addBoundary= vBoundaries[iBoundcopy+1] + (( nextBoundary-vBoundaries[iBoundcopy+1] )/2);
                vBoundShifted.push_back(addBoundary);  if (debug) cout << addBoundary << " "; 
                
                nextBoundary=vBoundaries[iBoundcopy+3];
                addBoundary= vBoundaries[iBoundcopy+2] + (( nextBoundary-vBoundaries[iBoundcopy+2] )/2);
                vBoundShifted.push_back(addBoundary);  if (debug) cout << addBoundary << " "; 
            } else {
                vBoundShifted.push_back(vBoundaries[iBoundcopy]);
                vIndexOriginal.push_back(iBoundcopy);
                if (debug) cout << vBoundaries[iBoundcopy] << " ";
            }
        }
        if (debug) cout << "]" << endl;

        Boolean runthis=true;
        if (runthis) {
            /* Go through skipped set of boundaries, getting hypothesis */
        for (short iCoupleBound = 0; iCoupleBound < (vBoundShifted.size() - 1); iCoupleBound++) {
            
            //string sCurrentRefword = vRefwords[vIndexOriginal[iCoupleBound]];
            NullPost = 1;
            vector<unsigned> vMicid;
            vector<double> vPosts;
            vector<string> vWords, vSegmentWords;
            vector<double> vFinalScores;

            float tS = vBoundShifted[iCoupleBound], tE = vBoundShifted[iCoupleBound + 1], DeltaT = 0.01;
            if (debug)
                cout << "(t:" << tS << " " << tE << "):";

            /* Start link selection */
            ifstream inputFile(tmpDataFile);
            string line;

            /* Select links accomplishing the boundary constraints */
            while (getline(inputFile, line)) {
                if (!line.length() || line[0] == '#')
                    continue;
                unsigned numLatNode = 0, idMic = 0;
                char cWordlink[20];
                string sWordlink;
                float startTime = 0., endTime = 0.;
                double postProb = 0.;

                sscanf(line.c_str(), "%u %19s %f %f %lf %u", &numLatNode, cWordlink, &startTime, &endTime, &postProb, &idMic);
                sWordlink = (string) cWordlink;

                /* Ommit <s> </s> in octave, here is equivalent to NULLs*/
                /* || (sWordlink.compare(str2) == 0 */
                if (!((sWordlink.find("<s>") != std::string::npos) | (sWordlink.find("NULL") != std::string::npos) | (sWordlink.find("</s>") != std::string::npos))) {
                    Boolean startIsValid = false, endIsValid = false;

                    /* Validate Start/End +-Delta boundaries */
                    float midpoint = startTime + ((endTime - startTime) / 2.0);
                    switch (boundsvalidation) {
                        case 1:
                            // Validate Start/End with separate deltas */
                            startIsValid = ((startTime >= (tS - manualdeltaS)) && (startTime <= (tS + manualdeltaS)));
                            endIsValid = ((endTime <= (tE + manualdeltaE)) && (endTime >= (tE - manualdeltaE)));
                            break;
                        case 2:
                            // Use MIDPOINT at a deltatolerance from Start/End */                        
                            if ((midpoint >= tS & midpoint <= tE) | (fabs(midpoint - tS) <= manualdelta) | (fabs(midpoint - tE) <= manualdelta)) {
                                startIsValid = true;
                                endIsValid = true;
                            }
                            break;
                        case 0:
                            // Validate if Start/End are at a DeltaT from boundaries */
                            if ((tE - tS) < (deltaref)) {
                                DeltaT = ((tE - tS) / 2);
                            } else {
                                DeltaT = ((tE - tS) * lratio);
                            }
                            startIsValid = ((startTime >= (tS - DeltaT)) && (startTime <= (tS + DeltaT)));
                            endIsValid = ((endTime <= (tE + DeltaT)) && (endTime >= (tE - DeltaT)));
                            break;
                    }

                    if (startIsValid && endIsValid) {
                        vPosts.push_back(postProb);
                        vWords.push_back(sWordlink);
                        vSegmentWords.push_back(sWordlink);
                        vMicid.push_back(idMic);
                        if (debug > 1) cout << "l[" << sWordlink << " " << startTime << "," << endTime << " " << postProb << " " << idMic << "] ";
                    }
                }
            }
            /* If links were found ... compute avgInterMic(sumIntraMic(PP)) */
            if (vWords.size() > 0) {
                if (debug)
                    cout << "  Found " << vPosts.size() << " links.." << endl;
                /* Get list of unique occurrence of words */
                std::sort(vSegmentWords.begin(), vSegmentWords.end());
                vSegmentWords.erase(std::unique(vSegmentWords.begin(), vSegmentWords.end()), vSegmentWords.end());

                /* ****************** DISPLAY ***************** */
                if (debug) {
                    cout << "   Unique words: ";
                    for (unsigned i = 0; i < vSegmentWords.size(); i++) {
                        cout << vSegmentWords[i] << " ";
                    }
                    cout << endl << "   ";
                }

                /* ======================================== */
                unsigned numWords = vSegmentWords.size();

                vector<vector<double> > mWordsMics;
                short sIndexRefword = -1;

                /* Per each word */
                for (unsigned w = 0; w < numWords; w++) {
                    vector<unsigned> vIndex;
                    string currentWord = vSegmentWords[w];
                    for (unsigned i = 0; i < vWords.size(); i++) {
                        if (vWords[i] == currentWord) {
                            vIndex.push_back(i);
                        }
                    }

                    /* Per each microphone */
                    vector<double> vMics;
                    for (unsigned mic = 0; mic < numMics; mic++) {
                        /* Get indexes of links of current Mic */ //cout << "<Mic" << mic << " ";
                        vector<unsigned> vIndexMic;
                        for (unsigned selectedlink = 0; selectedlink < vIndex.size(); selectedlink++) {
                            if (vMicid[vIndex[selectedlink]] == mic) {
                                vIndexMic.push_back(vIndex[selectedlink]);
                            }
                        }

                        /* IntraMic Score */
                        double Postscore = .0;
                        if (vIndexMic.size() > 0) {
                            for (unsigned i = 0; i < vIndexMic.size(); i++) {
                                Postscore = Postscore + vPosts[vIndexMic[i]];
                            }
                        }
                        vMics.push_back(Postscore);
                    }
                    mWordsMics.push_back(vMics);
                }

                /* InterMic Score */
                vector<double> vNewPost;
                double sumvNewPost = .0, sumvNewPost2 = .0;
                for (unsigned wrow = 0; wrow < mWordsMics.size(); wrow++) {
                    double newPost = .0;

                    double maxscoreInterM = 0;
                    for (unsigned mic = 0; mic < numMics; mic++) {
                        newPost = newPost + mWordsMics[wrow][mic];
                        if (mWordsMics[wrow][mic] > maxscoreInterM) {
                            maxscoreInterM = mWordsMics[wrow][mic];
                        }
                    }
                    double interMicWordScore = 0;
                    switch (scoremethod) {
                        case 1:
                            /* AVG InterMic */
                            interMicWordScore = (newPost / numMics);
                            break;
                        case 2:
                            /* MAX among all mics */
                            interMicWordScore = maxscoreInterM; //Use maxscore instead of AvgInterMic
                            break;
                    }
                    vNewPost.push_back(interMicWordScore);
                    sumvNewPost = sumvNewPost + (interMicWordScore);
                }

                /* Normalization if sumPP of all candidates is >1 */
                if (debug) {
                    cout << " Final Scores ";
                    cout << "[vNewPost size " << vNewPost.size() << "] ";
                    cout << " [alpha*sumvNewPost " << alpha * sumvNewPost << "] ";
                }
                if ((alpha * sumvNewPost) > 1) {
                    for (unsigned i = 0; i < vNewPost.size(); i++) {
                        vFinalScores.push_back(vNewPost[i] / (sumvNewPost));
                        sumvNewPost2 += (vNewPost[i] / (sumvNewPost));
                        if (debug)
                            cout << (vNewPost[i] / (sumvNewPost)) << " ";
                    }
                } else {
                    for (unsigned i = 0; i < vNewPost.size(); i++) {
                        vFinalScores.push_back(vNewPost[i]);
                        sumvNewPost2 += vNewPost[i];
                        //cout << vFinalScores[i] << " " ;
                        if (debug)
                            cout << "PPw " << (vNewPost[i]) << " ";
                    }
                }

                /* Criterion to continue/stop search of segment */
                NullPost = 1 - sumvNewPost2;


            } else {
                NullPost = 1;
                if (debug)
                    cout << "  Found no valid links in this segment." << endl;
                //                    /* Keep track of reference hypothesis scores */
                //                    vRefwordsScore.push_back(0.0);
            }

            if (debug)
                cout << " (NullPost " << NullPost << ") " << endl;


            /* Add NULL word and score to stack of candidates */
            vFinalScores.push_back(NullPost);
            vSegmentWords.push_back("NULL");
            
            /* Find maximum candidate score */
            int indexMax = vSegmentWords.size() - 1;            
            double maxscore = .0;            
            /* Compose CN file */
            for (unsigned i = 0; i < vSegmentWords.size(); i++) {
                if (vFinalScores[i] > maxscore) {
                    indexMax = i;
                    maxscore = vFinalScores[i];
                }
            }

            if (debug) {
                cout << "<< MaxScore " << maxscore << " Index " << indexMax << " :: " << vSegmentWords[indexMax] << ">>";
            }

            vHypothesis.push_back(vSegmentWords[indexMax]);
            vMaxScoresCN.push_back(vFinalScores[indexMax]);
            NullPost = 0;
            numalign++;

        }
        if (debug)     cout << "(H" << iSkips << ": ";
        ostringstream wholehypothesis("");
        for (unsigned i = 0; i < vHypothesis.size(); i++) {
            string hypword = vHypothesis[i];
            if (!(hypword.compare("NULL") == 0)) {
                wholehypothesis << hypword << " ";
            } else {
                //wholehypothesis << "sil ";
            }
            if (debug) cout << hypword << " ";
        }
        string swholehypothesis = wholehypothesis.str();

        fprintf(hypshiftfile, "%s\n", swholehypothesis.c_str());
        if (debug)    cout << ")" << endl;
        vHypothesis.clear();

        if (debug)
            cout << endl;

        hypshiftfile.close();
        }
        
    } /* Continue to next shifting step ...  */
}
void forceSegmentsWrdSilInc4(short numinc) {
    unsigned numalign = 1;
    float NullPost = 1;    
    vector<double> vMaxScoresCN; //vector to keep PP of maxScore per CSet, just for display purposes
    cout << "****** Increments to apply: " << numinc << " ********" << endl;

    for (short iSkips = 0; iSkips < (vBoundaries.size()-numinc); iSkips++) {
        if (debug) cout << "Increment: " << iSkips << " " << vBoundaries[iSkips];
        ostringstream fileNameStream("");        
        fileNameStream << prefixout << "inc" << numinc << ".txt";
        string outfilename = fileNameStream.str();   
        File hypshiftfile(outfilename.c_str(), "a");
        
        /* Duplicate vector of boundaries skipping iSkip */
        vector<float> vBoundShifted;
        vector<short> vIndexOriginal;
        if (debug) cout << "[ " ;
        for (short iBoundcopy = 0; iBoundcopy < vBoundaries.size(); iBoundcopy++) {
             vBoundShifted.push_back(vBoundaries[iBoundcopy]);
             vIndexOriginal.push_back(iBoundcopy);
             if (debug) cout << vBoundaries[iBoundcopy] << " ";
             if ((iBoundcopy == iSkips)){
                for (short itoadd=0; itoadd<numinc; itoadd++){
                    double curBoundary=vBoundaries[iBoundcopy+itoadd];
                    double nextBoundary=vBoundaries[iBoundcopy+1+itoadd];
                    double addBoundary= curBoundary + (( nextBoundary-curBoundary )/2);
                    vBoundShifted.push_back(addBoundary); if (debug) cout << addBoundary << " ";
                }
             }
        }
        if (debug) cout << "]" << endl;
        /* Sort added boundaries */
        std::sort(vBoundShifted.begin(), vBoundShifted.end());

        Boolean runthis=true;
        if (runthis) {
            /* Go through skipped set of boundaries, getting hypothesis */
        for (short iCoupleBound = 0; iCoupleBound < (vBoundShifted.size() - 1); iCoupleBound++) {
            
            //string sCurrentRefword = vRefwords[vIndexOriginal[iCoupleBound]];
            NullPost = 1;
            vector<unsigned> vMicid;
            vector<double> vPosts;
            vector<string> vWords, vSegmentWords;
            vector<double> vFinalScores;

            float tS = vBoundShifted[iCoupleBound], tE = vBoundShifted[iCoupleBound + 1], DeltaT = 0.01;
            if (debug)
                cout << "(t:" << tS << " " << tE << "):";

            /* Start link selection */
            ifstream inputFile(tmpDataFile);
            string line;

            /* Select links accomplishing the boundary constraints */
            while (getline(inputFile, line)) {
                if (!line.length() || line[0] == '#')
                    continue;
                unsigned numLatNode = 0, idMic = 0;
                char cWordlink[20];
                string sWordlink;
                float startTime = 0., endTime = 0.;
                double postProb = 0.;

                sscanf(line.c_str(), "%u %19s %f %f %lf %u", &numLatNode, cWordlink, &startTime, &endTime, &postProb, &idMic);
                sWordlink = (string) cWordlink;

                /* Ommit <s> </s> in octave, here is equivalent to NULLs*/
                /* || (sWordlink.compare(str2) == 0 */
                if (!((sWordlink.find("<s>") != std::string::npos) | (sWordlink.find("NULL") != std::string::npos) | (sWordlink.find("</s>") != std::string::npos))) {
                    Boolean startIsValid = false, endIsValid = false;

                    /* Validate Start/End +-Delta boundaries */
                    float midpoint = startTime + ((endTime - startTime) / 2.0);
                    switch (boundsvalidation) {
                        case 1:
                            // Validate Start/End with separate deltas */
                            startIsValid = ((startTime >= (tS - manualdeltaS)) && (startTime <= (tS + manualdeltaS)));
                            endIsValid = ((endTime <= (tE + manualdeltaE)) && (endTime >= (tE - manualdeltaE)));
                            break;
                        case 2:
                            // Use MIDPOINT at a deltatolerance from Start/End */                        
                            if ((midpoint >= tS & midpoint <= tE) | (fabs(midpoint - tS) <= manualdelta) | (fabs(midpoint - tE) <= manualdelta)) {
                                startIsValid = true;
                                endIsValid = true;
                            }
                            break;
                        case 0:
                            // Validate if Start/End are at a DeltaT from boundaries */
                            if ((tE - tS) < (deltaref)) {
                                DeltaT = ((tE - tS) / 2);
                            } else {
                                DeltaT = ((tE - tS) * lratio);
                            }
                            startIsValid = ((startTime >= (tS - DeltaT)) && (startTime <= (tS + DeltaT)));
                            endIsValid = ((endTime <= (tE + DeltaT)) && (endTime >= (tE - DeltaT)));
                            break;
                    }

                    if (startIsValid && endIsValid) {
                        vPosts.push_back(postProb);
                        vWords.push_back(sWordlink);
                        vSegmentWords.push_back(sWordlink);
                        vMicid.push_back(idMic);
                        if (debug > 1) cout << "l[" << sWordlink << " " << startTime << "," << endTime << " " << postProb << " " << idMic << "] ";
                    }
                }
            }
            /* If links were found ... compute avgInterMic(sumIntraMic(PP)) */
            if (vWords.size() > 0) {
                if (debug)
                    cout << "  Found " << vPosts.size() << " links.." << endl;
                /* Get list of unique occurrence of words */
                std::sort(vSegmentWords.begin(), vSegmentWords.end());
                vSegmentWords.erase(std::unique(vSegmentWords.begin(), vSegmentWords.end()), vSegmentWords.end());

                /* ****************** DISPLAY ***************** */
                if (debug) {
                    cout << "   Unique words: ";
                    for (unsigned i = 0; i < vSegmentWords.size(); i++) {
                        cout << vSegmentWords[i] << " ";
                    }
                    cout << endl << "   ";
                }

                /* ======================================== */
                unsigned numWords = vSegmentWords.size();

                vector<vector<double> > mWordsMics;
                short sIndexRefword = -1;

                /* Per each word */
                for (unsigned w = 0; w < numWords; w++) {
                    vector<unsigned> vIndex;
                    string currentWord = vSegmentWords[w];
                    //if (currentWord.compare(sCurrentRefword) == 0) {
                        /* Keep index of refWord in list of words   */
                        /* to later extract PP from InterMic matrix */
                        //sIndexRefword = w;
                    //}
                    for (unsigned i = 0; i < vWords.size(); i++) {
                        if (vWords[i] == currentWord) {
                            vIndex.push_back(i);
                        }
                    }

                    /* Per each microphone */
                    vector<double> vMics;
                    for (unsigned mic = 0; mic < numMics; mic++) {
                        /* Get indexes of links of current Mic */ //cout << "<Mic" << mic << " ";
                        vector<unsigned> vIndexMic;
                        for (unsigned selectedlink = 0; selectedlink < vIndex.size(); selectedlink++) {
                            if (vMicid[vIndex[selectedlink]] == mic) {
                                vIndexMic.push_back(vIndex[selectedlink]);
                            }
                        }

                        /* IntraMic Score */
                        double Postscore = .0;
                        if (vIndexMic.size() > 0) {
                            for (unsigned i = 0; i < vIndexMic.size(); i++) {
                                Postscore = Postscore + vPosts[vIndexMic[i]];
                            }
                        }
                        vMics.push_back(Postscore);
                    }
                    mWordsMics.push_back(vMics);
                }

                /* InterMic Score */
                vector<double> vNewPost;
                double sumvNewPost = .0, sumvNewPost2 = .0;
                for (unsigned wrow = 0; wrow < mWordsMics.size(); wrow++) {
                    double newPost = .0;

                    double maxscoreInterM = 0;
                    for (unsigned mic = 0; mic < numMics; mic++) {
                        newPost = newPost + mWordsMics[wrow][mic];
                        if (mWordsMics[wrow][mic] > maxscoreInterM) {
                            maxscoreInterM = mWordsMics[wrow][mic];
                        }
                    }
                    double interMicWordScore = 0;
                    switch (scoremethod) {
                        case 1:
                            /* AVG InterMic */
                            interMicWordScore = (newPost / numMics);
                            break;
                        case 2:
                            /* MAX among all mics */
                            interMicWordScore = maxscoreInterM; //Use maxscore instead of AvgInterMic
                            break;
                    }
                    vNewPost.push_back(interMicWordScore);
                    sumvNewPost = sumvNewPost + (interMicWordScore);
                }

                /* Normalization if sumPP of all candidates is >1 */
                if (debug) {
                    cout << " Final Scores ";
                    cout << "[vNewPost size " << vNewPost.size() << "] ";
                    cout << " [alpha*sumvNewPost " << alpha * sumvNewPost << "] ";
                }
                if ((alpha * sumvNewPost) > 1) {
                    for (unsigned i = 0; i < vNewPost.size(); i++) {
                        vFinalScores.push_back(vNewPost[i] / (sumvNewPost));
                        sumvNewPost2 += (vNewPost[i] / (sumvNewPost));
                        if (debug)
                            cout << (vNewPost[i] / (sumvNewPost)) << " ";
                    }
                } else {
                    for (unsigned i = 0; i < vNewPost.size(); i++) {
                        vFinalScores.push_back(vNewPost[i]);
                        sumvNewPost2 += vNewPost[i];
                        //cout << vFinalScores[i] << " " ;
                        if (debug)
                            cout << "PPw " << (vNewPost[i]) << " ";
                    }
                }

                /* Criterion to continue/stop search of segment */
                NullPost = 1 - sumvNewPost2;

            } else {
                NullPost = 1;
                if (debug)
                    cout << "  Found no valid links in this segment." << endl;
                //                    /* Keep track of reference hypothesis scores */
                //                    vRefwordsScore.push_back(0.0);
            }

            if (debug)
                cout << " (NullPost " << NullPost << ") " << endl;

            /* Finally if Set is valid, add NULL to set */
            //if ((NullPost <= THRSNULL) & (vPosts.size() > 0)) {
            //        if (vPosts.size() > 0) {

            /* Add NULL word and score to stack of candidates */
            vFinalScores.push_back(NullPost);
            vSegmentWords.push_back("NULL");
            
            /* Find maximum candidate score */
            int indexMax = vSegmentWords.size() - 1;            
            double maxscore = .0;            
            /* Compose CN file */
            // fprintf(cnTmpBody, "align %u", (numalign));
            for (unsigned i = 0; i < vSegmentWords.size(); i++) {
                if (vFinalScores[i] > maxscore) {
                    indexMax = i;
                    maxscore = vFinalScores[i];
                }
            }

            if (debug) {
                cout << "<< MaxScore " << maxscore << " Index " << indexMax << " :: " << vSegmentWords[indexMax] << ">>";
            }

            vHypothesis.push_back(vSegmentWords[indexMax]);
            vMaxScoresCN.push_back(vFinalScores[indexMax]);
            //            currentalign++;
            NullPost = 0;
            numalign++;
            //        }

        }
        if (debug)     cout << "(H" << iSkips << ": ";
        ostringstream wholehypothesis("");
        for (unsigned i = 0; i < vHypothesis.size(); i++) {
            string hypword = vHypothesis[i];
            if (!(hypword.compare("NULL") == 0)) {
                wholehypothesis << hypword << " ";
                //if (debug) cout << hypword << " ";
            } else {
                //wholehypothesis << "sil ";
            }
            if (debug) cout << hypword << " ";
        }
        string swholehypothesis = wholehypothesis.str();

        fprintf(hypshiftfile, "%s\n", swholehypothesis.c_str());
        if (debug)    cout << ")" << endl;
        vHypothesis.clear();

        if (debug)
            cout << endl;

        hypshiftfile.close();
        }
        
    } /* Continue to next shifting step ...  */
}

/* Explicitly use wrd time boundary couples to extract ConfSets*/
/* Skipping one/+ boundary at a time */
void forceSegmentsWrdSilSkip0() {
    unsigned numalign = 1;
    float NullPost = 1;    

    vector<double> vMaxScoresCN; //vector to keep PP of maxScore per CSet, just for display purposes

    cout << "****** Skip: NONE ******" << endl;
    ostringstream fileNameStream("");
    fileNameStream << prefixout << "skip0.txt";
    string outfilename = fileNameStream.str();
    File hypshiftfile(outfilename.c_str(), "a");

    /* Duplicate vector of boundaries no skipping */
    vector<float> vBoundShifted;
    vector<short> vIndexOriginal;
    if (debug) cout << "[ ";
    for (short iBoundcopy = 0; iBoundcopy < vBoundaries.size(); iBoundcopy++) {
        vBoundShifted.push_back(vBoundaries[iBoundcopy]);
        vIndexOriginal.push_back(iBoundcopy);
        if (debug) cout << vBoundaries[iBoundcopy] << " ";
    }
    if (debug) cout << "]" << endl;
        
        /* Go through set of boundaries, getting hypothesis */
    for (short iCoupleBound = 0; iCoupleBound < (vBoundShifted.size() - 1); iCoupleBound++) {
        string sCurrentRefword = vRefwords[vIndexOriginal[iCoupleBound]];
        NullPost = 1;
        vector<unsigned> vMicid;
        vector<double> vPosts;
        vector<string> vWords, vSegmentWords;
        vector<double> vFinalScores;

        float tS = vBoundShifted[iCoupleBound], tE = vBoundShifted[iCoupleBound + 1], DeltaT = 0.01;
        if (debug)
            cout << "(t:" << tS << " " << tE << "):";

        /* Start link selection */
        ifstream inputFile(tmpDataFile);
        string line;

        /* Select links accomplishing the boundary constraints */
        while (getline(inputFile, line)) {
            if (!line.length() || line[0] == '#')
                continue;
            unsigned numLatNode = 0, idMic = 0;
            char cWordlink[20];
            string sWordlink;
            float startTime = 0., endTime = 0.;
            double postProb = 0.;

            sscanf(line.c_str(), "%u %19s %f %f %lf %u", &numLatNode, cWordlink, &startTime, &endTime, &postProb, &idMic);
            sWordlink = (string) cWordlink;

            /* Ommit <s> </s> in octave, here is equivalent to NULLs*/
            /* || (sWordlink.compare(str2) == 0 */
            if (!((sWordlink.find("<s>") != std::string::npos) | (sWordlink.find("NULL") != std::string::npos) | (sWordlink.find("</s>") != std::string::npos))) {
                Boolean startIsValid = false, endIsValid = false;

                /* Validate Start/End +-Delta boundaries */
                float midpoint = startTime + ((endTime - startTime) / 2.0);
                switch (boundsvalidation) {
                    case 1:
                        // Validate Start/End with separate deltas */
                        startIsValid = ((startTime >= (tS - manualdeltaS)) && (startTime <= (tS + manualdeltaS)));
                        endIsValid = ((endTime <= (tE + manualdeltaE)) && (endTime >= (tE - manualdeltaE)));
                        break;
                    case 2:
                        // Use MIDPOINT at a deltatolerance from Start/End */                        
                        if ((midpoint >= tS & midpoint <= tE) | (fabs(midpoint - tS) <= manualdelta) | (fabs(midpoint - tE) <= manualdelta)) {
                            startIsValid = true;
                            endIsValid = true;
                        }
                        break;
                    case 0:
                        // Validate if Start/End are at a DeltaT from boundaries */
                        if ((tE - tS) < (deltaref)) {
                            DeltaT = ((tE - tS) / 2);
                        } else {
                            DeltaT = ((tE - tS) * lratio);
                        }
                        startIsValid = ((startTime >= (tS - DeltaT)) && (startTime <= (tS + DeltaT)));
                        endIsValid = ((endTime <= (tE + DeltaT)) && (endTime >= (tE - DeltaT)));
                        break;
                }

                if (startIsValid && endIsValid) {
                    vPosts.push_back(postProb);
                    vWords.push_back(sWordlink);
                    vSegmentWords.push_back(sWordlink);
                    vMicid.push_back(idMic);
                    if (debug > 1) cout << "l[" << sWordlink << " " << startTime << "," << endTime << " " << postProb << " " << idMic << "] ";
                }
            }
        }
        /* If links were found ... compute avgInterMic(sumIntraMic(PP)) */
        if (vWords.size() > 0) {
            if (debug)
                cout << "  Found " << vPosts.size() << " links.." << endl;
            /* Get list of unique occurrence of words */
            std::sort(vSegmentWords.begin(), vSegmentWords.end());
            vSegmentWords.erase(std::unique(vSegmentWords.begin(), vSegmentWords.end()), vSegmentWords.end());

            /* ****************** DISPLAY ***************** */
            if (debug) {
                cout << "   Unique words: ";
                for (unsigned i = 0; i < vSegmentWords.size(); i++) {
                    cout << vSegmentWords[i] << " ";
                }
                cout << endl << "   ";
            }

            /* ======================================== */
            unsigned numWords = vSegmentWords.size();

            vector<vector<double> > mWordsMics;
            short sIndexRefword = -1;

            /* Per each word */
            for (unsigned w = 0; w < numWords; w++) {
                vector<unsigned> vIndex;
                string currentWord = vSegmentWords[w];
                if (currentWord.compare(sCurrentRefword) == 0) {
                    /* Keep index of refWord in list of words   */
                    /* to later extract PP from InterMic matrix */
                    sIndexRefword = w;
                }
                for (unsigned i = 0; i < vWords.size(); i++) {
                    if (vWords[i] == currentWord) {
                        vIndex.push_back(i);
                    }
                }

                /* Per each microphone */
                vector<double> vMics;
                for (unsigned mic = 0; mic < numMics; mic++) {
                    /* Get indexes of links of current Mic */ //cout << "<Mic" << mic << " ";
                    vector<unsigned> vIndexMic;
                    for (unsigned selectedlink = 0; selectedlink < vIndex.size(); selectedlink++) {
                        if (vMicid[vIndex[selectedlink]] == mic) {
                            vIndexMic.push_back(vIndex[selectedlink]);
                        }
                    }

                    /* IntraMic Score */
                    double Postscore = .0;
                    if (vIndexMic.size() > 0) {
                        for (unsigned i = 0; i < vIndexMic.size(); i++) {
                            Postscore = Postscore + vPosts[vIndexMic[i]];
                        }
                    }
                    vMics.push_back(Postscore);
                }
                mWordsMics.push_back(vMics);
            }

            /* InterMic Score */
            vector<double> vNewPost;
            double sumvNewPost = .0, sumvNewPost2 = .0;
            for (unsigned wrow = 0; wrow < mWordsMics.size(); wrow++) {
                double newPost = .0;

                double maxscoreInterM = 0;
                for (unsigned mic = 0; mic < numMics; mic++) {
                    newPost = newPost + mWordsMics[wrow][mic];
                    if (mWordsMics[wrow][mic] > maxscoreInterM) {
                        maxscoreInterM = mWordsMics[wrow][mic];
                    }
                }
                double interMicWordScore = 0;
                switch (scoremethod) {
                    case 1:
                        /* AVG InterMic */
                        interMicWordScore = (newPost / numMics);
                        break;
                    case 2:
                        /* MAX among all mics */
                        interMicWordScore = maxscoreInterM; //Use maxscore instead of AvgInterMic
                        break;
                }
                vNewPost.push_back(interMicWordScore);
                sumvNewPost = sumvNewPost + (interMicWordScore);
            }

            /* Normalization if sumPP of all candidates is >1 */
            if (debug) {
                cout << " Final Scores ";
                cout << "[vNewPost size " << vNewPost.size() << "] ";
                cout << " [alpha*sumvNewPost " << alpha * sumvNewPost << "] ";
            }
            if ((alpha * sumvNewPost) > 1) {
                for (unsigned i = 0; i < vNewPost.size(); i++) {
                    vFinalScores.push_back(vNewPost[i] / (sumvNewPost));
                    sumvNewPost2 += (vNewPost[i] / (sumvNewPost));
                    if (debug)
                        cout << (vNewPost[i] / (sumvNewPost)) << " ";
                }
            } else {
                for (unsigned i = 0; i < vNewPost.size(); i++) {
                    vFinalScores.push_back(vNewPost[i]);
                    sumvNewPost2 += vNewPost[i];
                    //cout << vFinalScores[i] << " " ;
                    if (debug)
                        cout << "PPw " << (vNewPost[i]) << " ";
                }
            }

            /* Criterion to continue/stop search of segment */
            NullPost = 1 - sumvNewPost2;


        } else {
            NullPost = 1;
            if (debug)
                cout << "  Found no valid links in this segment." << endl;
            //                    /* Keep track of reference hypothesis scores */
            //                    vRefwordsScore.push_back(0.0);
        }

        if (debug)
            cout << " (NullPost " << NullPost << ") " << endl;

        /* Finally if Set is valid, add NULL to set */
        //if ((NullPost <= THRSNULL) & (vPosts.size() > 0)) {
        //        if (vPosts.size() > 0) {

        /* Add NULL word and score to stack of candidates */
        vFinalScores.push_back(NullPost);
        vSegmentWords.push_back("NULL");

        /* Find maximum candidate score */
        int indexMax = vSegmentWords.size() - 1;
        double maxscore = .0;
        /* Compose CN file */
        // fprintf(cnTmpBody, "align %u", (numalign));
        for (unsigned i = 0; i < vSegmentWords.size(); i++) {
            if (vFinalScores[i] > maxscore) {
                indexMax = i;
                maxscore = vFinalScores[i];
            }
            //                    if (vFinalScores[i] > 0.0000009) {
            //                        //fprintf(cnTmpBody, " %s %f", vSegmentWords[i].c_str(), vFinalScores[i]);
            //                    }
        }
        //            fprintf(cnTmpBody, "\n");

        if (debug) {
            cout << "<< MaxScore " << maxscore << " Index " << indexMax << " :: " << vSegmentWords[indexMax] << ">>";
        }

        vHypothesis.push_back(vSegmentWords[indexMax]);
        vMaxScoresCN.push_back(vFinalScores[indexMax]);
        //            currentalign++;
        NullPost = 0;
        numalign++;
        //        }

    }
    if (debug) cout << "(H: ";
    ostringstream wholehypothesis("");
    for (unsigned i = 0; i < vHypothesis.size(); i++) {
        string hypword = vHypothesis[i];
        if (!(hypword.compare("NULL") == 0)) {
            wholehypothesis << hypword << " ";
            //if (debug) cout << hypword << " ";
        } else {
            //wholehypothesis << "sil ";
        }
        if (debug) cout << hypword << " ";
    }
    string swholehypothesis = wholehypothesis.str();

    fprintf(hypshiftfile, "%s\n", swholehypothesis.c_str());
    if (debug) cout << ")" << endl;
    vHypothesis.clear();


    if (debug)
        cout << endl;
    
    hypshiftfile.close();    
}
void forceSegmentsWrdSilSkip1() {
    unsigned numalign = 1;
    float NullPost = 1;    
    vector<double> vMaxScoresCN; //vector to keep PP of maxScore per CSet, just for display purposes
    cout << "****** Skips to apply: 1 ********" << endl;

    for (short iSkips = 0; iSkips < vBoundaries.size(); iSkips++) {
        if (debug) cout << "Skip: " << iSkips << " " << vBoundaries[iSkips];
        ostringstream fileNameStream("");        
        fileNameStream << prefixout << "skip1.txt";
        string outfilename = fileNameStream.str();        
        File hypshiftfile(outfilename.c_str(), "a");
        
        /* Duplicate vector of boundaries skipping iSkip */
        vector<float> vBoundShifted;
        vector<short> vIndexOriginal;
        if (debug) cout << "[ " ;
        for (short iBoundcopy = 0; iBoundcopy < vBoundaries.size(); iBoundcopy++) {
            if (!(iBoundcopy == iSkips)){
                vBoundShifted.push_back(vBoundaries[iBoundcopy]);
                vIndexOriginal.push_back(iBoundcopy);
                if (debug) cout << vBoundaries[iBoundcopy] << " ";
            }
        }
        if (debug) cout << "]" << endl;
        
        /* Go through skipped set of boundaries, getting hypothesis */
        for (short iCoupleBound = 0; iCoupleBound < (vBoundShifted.size() - 1); iCoupleBound++) {
            string sCurrentRefword = vRefwords[vIndexOriginal[iCoupleBound]];
            NullPost = 1;
            vector<unsigned> vMicid;
            vector<double> vPosts;
            vector<string> vWords, vSegmentWords;
            vector<double> vFinalScores;

            float tS = vBoundShifted[iCoupleBound], tE = vBoundShifted[iCoupleBound + 1], DeltaT = 0.01;
            if (debug)
                cout << "(t:" << tS << " " << tE << "):";

            /* Start link selection */
            ifstream inputFile(tmpDataFile);
            string line;

            /* Select links accomplishing the boundary constraints */
            while (getline(inputFile, line)) {
                if (!line.length() || line[0] == '#')
                    continue;
                unsigned numLatNode = 0, idMic = 0;
                char cWordlink[20];
                string sWordlink;
                float startTime = 0., endTime = 0.;
                double postProb = 0.;

                sscanf(line.c_str(), "%u %19s %f %f %lf %u", &numLatNode, cWordlink, &startTime, &endTime, &postProb, &idMic);
                sWordlink = (string) cWordlink;

                /* Ommit <s> </s> in octave, here is equivalent to NULLs*/
                /* || (sWordlink.compare(str2) == 0 */
                if (!((sWordlink.find("<s>") != std::string::npos) | (sWordlink.find("NULL") != std::string::npos) | (sWordlink.find("</s>") != std::string::npos))) {
                    Boolean startIsValid = false, endIsValid = false;

                    /* Validate Start/End +-Delta boundaries */
                    float midpoint = startTime + ((endTime - startTime) / 2.0);
                    switch (boundsvalidation) {
                        case 1:
                            // Validate Start/End with separate deltas */
                            startIsValid = ((startTime >= (tS - manualdeltaS)) && (startTime <= (tS + manualdeltaS)));
                            endIsValid = ((endTime <= (tE + manualdeltaE)) && (endTime >= (tE - manualdeltaE)));
                            break;
                        case 2:
                            // Use MIDPOINT at a deltatolerance from Start/End */                        
                            if ((midpoint >= tS & midpoint <= tE) | (fabs(midpoint - tS) <= manualdelta) | (fabs(midpoint - tE) <= manualdelta)) {
                                startIsValid = true;
                                endIsValid = true;
                            }
                            break;
                        case 0:
                            // Validate if Start/End are at a DeltaT from boundaries */
                            if ((tE - tS) < (deltaref)) {
                                DeltaT = ((tE - tS) / 2);
                            } else {
                                DeltaT = ((tE - tS) * lratio);
                            }
                            startIsValid = ((startTime >= (tS - DeltaT)) && (startTime <= (tS + DeltaT)));
                            endIsValid = ((endTime <= (tE + DeltaT)) && (endTime >= (tE - DeltaT)));
                            break;
                    }

                    if (startIsValid && endIsValid) {
                        vPosts.push_back(postProb);
                        vWords.push_back(sWordlink);
                        vSegmentWords.push_back(sWordlink);
                        vMicid.push_back(idMic);
                        if (debug > 1) cout << "l[" << sWordlink << " " << startTime << "," << endTime << " " << postProb << " " << idMic << "] ";
                    }
                }
            }
            /* If links were found ... compute avgInterMic(sumIntraMic(PP)) */
            if (vWords.size() > 0) {
                if (debug)
                    cout << "  Found " << vPosts.size() << " links.." << endl;
                /* Get list of unique occurrence of words */
                std::sort(vSegmentWords.begin(), vSegmentWords.end());
                vSegmentWords.erase(std::unique(vSegmentWords.begin(), vSegmentWords.end()), vSegmentWords.end());

                /* ****************** DISPLAY ***************** */
                if (debug) {
                    cout << "   Unique words: ";
                    for (unsigned i = 0; i < vSegmentWords.size(); i++) {
                        cout << vSegmentWords[i] << " ";
                    }
                    cout << endl << "   ";
                }

                /* ======================================== */
                unsigned numWords = vSegmentWords.size();

                vector<vector<double> > mWordsMics;
                short sIndexRefword = -1;

                /* Per each word */
                for (unsigned w = 0; w < numWords; w++) {
                    vector<unsigned> vIndex;
                    string currentWord = vSegmentWords[w];
                    if (currentWord.compare(sCurrentRefword) == 0) {
                        /* Keep index of refWord in list of words   */
                        /* to later extract PP from InterMic matrix */
                        sIndexRefword = w;
                    }
                    for (unsigned i = 0; i < vWords.size(); i++) {
                        if (vWords[i] == currentWord) {
                            vIndex.push_back(i);
                        }
                    }

                    /* Per each microphone */
                    vector<double> vMics;
                    for (unsigned mic = 0; mic < numMics; mic++) {
                        /* Get indexes of links of current Mic */ //cout << "<Mic" << mic << " ";
                        vector<unsigned> vIndexMic;
                        for (unsigned selectedlink = 0; selectedlink < vIndex.size(); selectedlink++) {
                            if (vMicid[vIndex[selectedlink]] == mic) {
                                vIndexMic.push_back(vIndex[selectedlink]);
                            }
                        }

                        /* IntraMic Score */
                        double Postscore = .0;
                        if (vIndexMic.size() > 0) {
                            for (unsigned i = 0; i < vIndexMic.size(); i++) {
                                Postscore = Postscore + vPosts[vIndexMic[i]];
                            }
                        }
                        vMics.push_back(Postscore);
                    }
                    mWordsMics.push_back(vMics);
                }

                /* InterMic Score */
                vector<double> vNewPost;
                double sumvNewPost = .0, sumvNewPost2 = .0;
                for (unsigned wrow = 0; wrow < mWordsMics.size(); wrow++) {
                    double newPost = .0;

                    double maxscoreInterM = 0;
                    for (unsigned mic = 0; mic < numMics; mic++) {
                        newPost = newPost + mWordsMics[wrow][mic];
                        if (mWordsMics[wrow][mic] > maxscoreInterM) {
                            maxscoreInterM = mWordsMics[wrow][mic];
                        }
                    }
                    double interMicWordScore = 0;
                    switch (scoremethod) {
                        case 1:
                            /* AVG InterMic */
                            interMicWordScore = (newPost / numMics);
                            break;
                        case 2:
                            /* MAX among all mics */
                            interMicWordScore = maxscoreInterM; //Use maxscore instead of AvgInterMic
                            break;
                    }
                    vNewPost.push_back(interMicWordScore);
                    sumvNewPost = sumvNewPost + (interMicWordScore);
                }

                /* Normalization if sumPP of all candidates is >1 */
                if (debug) {
                    cout << " Final Scores ";
                    cout << "[vNewPost size " << vNewPost.size() << "] ";
                    cout << " [alpha*sumvNewPost " << alpha * sumvNewPost << "] ";
                }
                if ((alpha * sumvNewPost) > 1) {
                    for (unsigned i = 0; i < vNewPost.size(); i++) {
                        vFinalScores.push_back(vNewPost[i] / (sumvNewPost));
                        sumvNewPost2 += (vNewPost[i] / (sumvNewPost));
                        if (debug)
                            cout << (vNewPost[i] / (sumvNewPost)) << " ";
                    }
                } else {
                    for (unsigned i = 0; i < vNewPost.size(); i++) {
                        vFinalScores.push_back(vNewPost[i]);
                        sumvNewPost2 += vNewPost[i];
                        //cout << vFinalScores[i] << " " ;
                        if (debug)
                            cout << "PPw " << (vNewPost[i]) << " ";
                    }
                }

                /* Criterion to continue/stop search of segment */
                NullPost = 1 - sumvNewPost2;

                //                    /* Keep track of reference hypothesis scores */
                //                    if (sIndexRefword>-1) {
                //                        /* WRef was in set of words in confset */
                //                        vRefwordsScore.push_back(vFinalScores[sIndexRefword]);
                //                    } else {
                //                        /* WRef was not in set of words in confset */
                //                        vRefwordsScore.push_back(0.0);
                //                    }

            } else {
                NullPost = 1;
                if (debug)
                    cout << "  Found no valid links in this segment." << endl;
                //                    /* Keep track of reference hypothesis scores */
                //                    vRefwordsScore.push_back(0.0);
            }

            if (debug)
                cout << " (NullPost " << NullPost << ") " << endl;

            /* Finally if Set is valid, add NULL to set */
            //if ((NullPost <= THRSNULL) & (vPosts.size() > 0)) {
            //        if (vPosts.size() > 0) {

            /* Add NULL word and score to stack of candidates */
            vFinalScores.push_back(NullPost);
            vSegmentWords.push_back("NULL");

            /* Find maximum candidate score */
            int indexMax = vSegmentWords.size() - 1;
            double maxscore = .0;
            /* Compose CN file */
            // fprintf(cnTmpBody, "align %u", (numalign));
            for (unsigned i = 0; i < vSegmentWords.size(); i++) {
                if (vFinalScores[i] > maxscore) {
                    indexMax = i;
                    maxscore = vFinalScores[i];
                }
                //                    if (vFinalScores[i] > 0.0000009) {
                //                        //fprintf(cnTmpBody, " %s %f", vSegmentWords[i].c_str(), vFinalScores[i]);
                //                    }
            }
            //            fprintf(cnTmpBody, "\n");

            if (debug) {
                cout << "<< MaxScore " << maxscore << " Index " << indexMax << " :: " << vSegmentWords[indexMax] << ">>";
            }

            vHypothesis.push_back(vSegmentWords[indexMax]);
            vMaxScoresCN.push_back(vFinalScores[indexMax]);
            //            currentalign++;
            NullPost = 0;
            numalign++;
            //        }

        }
        if (debug)     cout << "(H" << iSkips << ": ";
        ostringstream wholehypothesis("");
        for (unsigned i = 0; i < vHypothesis.size(); i++) {
            string hypword = vHypothesis[i];
            if (!(hypword.compare("NULL") == 0)) {
                wholehypothesis << hypword << " ";
                //if (debug) cout << hypword << " ";
            } else {
                //wholehypothesis << "sil ";
            }
            if (debug) cout << hypword << " ";
        }
        string swholehypothesis = wholehypothesis.str();

        fprintf(hypshiftfile, "%s\n", swholehypothesis.c_str());
        if (debug)    cout << ")" << endl;
        vHypothesis.clear();
            
        if (debug)
            cout << endl;

        hypshiftfile.close();
    } /* Continue to next shifting step ...  */
}
void forceSegmentsWrdSilSkip2() {
    unsigned numalign = 1;
    float NullPost = 1;    

    vector<double> vMaxScoresCN; //vector to keep PP of maxScore per CSet, just for display purposes

    cout << "******* Skips to apply: 2  ****** " << endl;
  
    for (short iSkips = 0; iSkips < (vBoundaries.size()-1); iSkips++) {
        if (debug) cout << "Skip: " << vBoundaries[iSkips] << "," << vBoundaries[iSkips+1] << " ";
        ostringstream fileNameStream("");        
        fileNameStream << prefixout << "skip2.txt";
        string outfilename = fileNameStream.str();        
        File hypshiftfile(outfilename.c_str(), "a");
        
        /* Duplicate vector of boundaries skipping iSkip */
        vector<float> vBoundShifted;
        vector<short> vIndexOriginal;
        if (debug) cout << "[ " ;
        for (short iBoundcopy = 0; iBoundcopy < vBoundaries.size(); iBoundcopy++) {
            if (! ((iBoundcopy == iSkips) | (iBoundcopy == iSkips+1)) ){
                vBoundShifted.push_back(vBoundaries[iBoundcopy]);
                vIndexOriginal.push_back(iBoundcopy);
                if (debug) cout << vBoundaries[iBoundcopy] << " ";
            }
        }
        if (debug) cout << "]" << endl;
        
        /* Go through skipped set of boundaries, getting hypothesis */
        for (short iCoupleBound = 0; iCoupleBound < (vBoundShifted.size() - 1); iCoupleBound++) {
            string sCurrentRefword = vRefwords[vIndexOriginal[iCoupleBound]];
            NullPost = 1;
            vector<unsigned> vMicid;
            vector<double> vPosts;
            vector<string> vWords, vSegmentWords;
            vector<double> vFinalScores;

            float tS = vBoundShifted[iCoupleBound], tE = vBoundShifted[iCoupleBound + 1], DeltaT = 0.01;
            if (debug)
                cout << "(t:" << tS << " " << tE << "):";

            /* Start link selection */
            ifstream inputFile(tmpDataFile);
            string line;

            /* Select links accomplishing the boundary constraints */
            while (getline(inputFile, line)) {
                if (!line.length() || line[0] == '#')
                    continue;
                unsigned numLatNode = 0, idMic = 0;
                char cWordlink[20];
                string sWordlink;
                float startTime = 0., endTime = 0.;
                double postProb = 0.;

                sscanf(line.c_str(), "%u %19s %f %f %lf %u", &numLatNode, cWordlink, &startTime, &endTime, &postProb, &idMic);
                sWordlink = (string) cWordlink;

                /* Ommit <s> </s> in octave, here is equivalent to NULLs*/
                /* || (sWordlink.compare(str2) == 0 */
                if (!((sWordlink.find("<s>") != std::string::npos) | (sWordlink.find("NULL") != std::string::npos) | (sWordlink.find("</s>") != std::string::npos))) {
                    Boolean startIsValid = false, endIsValid = false;

                    /* Validate Start/End +-Delta boundaries */
                    float midpoint = startTime + ((endTime - startTime) / 2.0);
                    switch (boundsvalidation) {
                        case 1:
                            // Validate Start/End with separate deltas */
                            startIsValid = ((startTime >= (tS - manualdeltaS)) && (startTime <= (tS + manualdeltaS)));
                            endIsValid = ((endTime <= (tE + manualdeltaE)) && (endTime >= (tE - manualdeltaE)));
                            break;
                        case 2:
                            // Use MIDPOINT at a deltatolerance from Start/End */                        
                            if ((midpoint >= tS & midpoint <= tE) | (fabs(midpoint - tS) <= manualdelta) | (fabs(midpoint - tE) <= manualdelta)) {
                                startIsValid = true;
                                endIsValid = true;
                            }
                            break;
                        case 0:
                            // Validate if Start/End are at a DeltaT from boundaries */
                            if ((tE - tS) < (deltaref)) {
                                DeltaT = ((tE - tS) / 2);
                            } else {
                                DeltaT = ((tE - tS) * lratio);
                            }
                            startIsValid = ((startTime >= (tS - DeltaT)) && (startTime <= (tS + DeltaT)));
                            endIsValid = ((endTime <= (tE + DeltaT)) && (endTime >= (tE - DeltaT)));
                            break;
                    }

                    if (startIsValid && endIsValid) {
                        vPosts.push_back(postProb);
                        vWords.push_back(sWordlink);
                        vSegmentWords.push_back(sWordlink);
                        vMicid.push_back(idMic);
                        if (debug > 1) cout << "l[" << sWordlink << " " << startTime << "," << endTime << " " << postProb << " " << idMic << "] ";
                    }
                }
            }
            /* If links were found ... compute avgInterMic(sumIntraMic(PP)) */
            if (vWords.size() > 0) {
                if (debug)
                    cout << "  Found " << vPosts.size() << " links.." << endl;
                /* Get list of unique occurrence of words */
                std::sort(vSegmentWords.begin(), vSegmentWords.end());
                vSegmentWords.erase(std::unique(vSegmentWords.begin(), vSegmentWords.end()), vSegmentWords.end());

                /* ****************** DISPLAY ***************** */
                if (debug) {
                    cout << "   Unique words: ";
                    for (unsigned i = 0; i < vSegmentWords.size(); i++) {
                        cout << vSegmentWords[i] << " ";
                    }
                    cout << endl << "   ";
                }

                /* ======================================== */
                unsigned numWords = vSegmentWords.size();

                vector<vector<double> > mWordsMics;
                short sIndexRefword = -1;

                /* Per each word */
                for (unsigned w = 0; w < numWords; w++) {
                    vector<unsigned> vIndex;
                    string currentWord = vSegmentWords[w];
                    if (currentWord.compare(sCurrentRefword) == 0) {
                        /* Keep index of refWord in list of words   */
                        /* to later extract PP from InterMic matrix */
                        sIndexRefword = w;
                    }
                    for (unsigned i = 0; i < vWords.size(); i++) {
                        if (vWords[i] == currentWord) {
                            vIndex.push_back(i);
                        }
                    }

                    /* Per each microphone */
                    vector<double> vMics;
                    for (unsigned mic = 0; mic < numMics; mic++) {
                        /* Get indexes of links of current Mic */ //cout << "<Mic" << mic << " ";
                        vector<unsigned> vIndexMic;
                        for (unsigned selectedlink = 0; selectedlink < vIndex.size(); selectedlink++) {
                            if (vMicid[vIndex[selectedlink]] == mic) {
                                vIndexMic.push_back(vIndex[selectedlink]);
                            }
                        }

                        /* IntraMic Score */
                        double Postscore = .0;
                        if (vIndexMic.size() > 0) {
                            for (unsigned i = 0; i < vIndexMic.size(); i++) {
                                Postscore = Postscore + vPosts[vIndexMic[i]];
                            }
                        }
                        vMics.push_back(Postscore);
                    }
                    mWordsMics.push_back(vMics);
                }

                /* InterMic Score */
                vector<double> vNewPost;
                double sumvNewPost = .0, sumvNewPost2 = .0;
                for (unsigned wrow = 0; wrow < mWordsMics.size(); wrow++) {
                    double newPost = .0;

                    double maxscoreInterM = 0;
                    for (unsigned mic = 0; mic < numMics; mic++) {
                        newPost = newPost + mWordsMics[wrow][mic];
                        if (mWordsMics[wrow][mic] > maxscoreInterM) {
                            maxscoreInterM = mWordsMics[wrow][mic];
                        }
                    }
                    double interMicWordScore = 0;
                    switch (scoremethod) {
                        case 1:
                            /* AVG InterMic */
                            interMicWordScore = (newPost / numMics);
                            break;
                        case 2:
                            /* MAX among all mics */
                            interMicWordScore = maxscoreInterM; //Use maxscore instead of AvgInterMic
                            break;
                    }
                    vNewPost.push_back(interMicWordScore);
                    sumvNewPost = sumvNewPost + (interMicWordScore);
                }

                /* Normalization if sumPP of all candidates is >1 */
                if (debug) {
                    cout << " Final Scores ";
                    cout << "[vNewPost size " << vNewPost.size() << "] ";
                    cout << " [alpha*sumvNewPost " << alpha * sumvNewPost << "] ";
                }
                if ((alpha * sumvNewPost) > 1) {
                    for (unsigned i = 0; i < vNewPost.size(); i++) {
                        vFinalScores.push_back(vNewPost[i] / (sumvNewPost));
                        sumvNewPost2 += (vNewPost[i] / (sumvNewPost));
                        if (debug)
                            cout << (vNewPost[i] / (sumvNewPost)) << " ";
                    }
                } else {
                    for (unsigned i = 0; i < vNewPost.size(); i++) {
                        vFinalScores.push_back(vNewPost[i]);
                        sumvNewPost2 += vNewPost[i];
                        //cout << vFinalScores[i] << " " ;
                        if (debug)
                            cout << "PPw " << (vNewPost[i]) << " ";
                    }
                }

                /* Criterion to continue/stop search of segment */
                NullPost = 1 - sumvNewPost2;


            } else {
                NullPost = 1;
                if (debug)
                    cout << "  Found no valid links in this segment." << endl;
                //                    /* Keep track of reference hypothesis scores */
                //                    vRefwordsScore.push_back(0.0);
            }

            if (debug)
                cout << " (NullPost " << NullPost << ") " << endl;

            /* Finally if Set is valid, add NULL to set */

            /* Add NULL word and score to stack of candidates */
            vFinalScores.push_back(NullPost);
            vSegmentWords.push_back("NULL");

            /* Find maximum candidate score */
            int indexMax = vSegmentWords.size() - 1;
            double maxscore = .0;
            /* Compose CN file */
            // fprintf(cnTmpBody, "align %u", (numalign));
            for (unsigned i = 0; i < vSegmentWords.size(); i++) {
                if (vFinalScores[i] > maxscore) {
                    indexMax = i;
                    maxscore = vFinalScores[i];
                }
            }

            if (debug) {
                cout << "<< MaxScore " << maxscore << " Index " << indexMax << " :: " << vSegmentWords[indexMax] << ">>";
            }

            vHypothesis.push_back(vSegmentWords[indexMax]);
            vMaxScoresCN.push_back(vFinalScores[indexMax]);
            NullPost = 0;
            numalign++;

        }
        
        if (debug) cout << "(H" << iSkips << ": ";
        ostringstream wholehypothesis("");
        for (unsigned i = 0; i < vHypothesis.size(); i++) {
            string hypword = vHypothesis[i];
            if (!(hypword.compare("NULL") == 0)) {
                wholehypothesis << hypword << " ";
                //if (debug) cout << hypword << " ";
            } else {
               // wholehypothesis << "sil ";
            }
            if (debug) cout << hypword << " ";
        }
        string swholehypothesis = wholehypothesis.str();

        fprintf(hypshiftfile, "%s\n", swholehypothesis.c_str());
        if (debug) cout << ")" << endl;
        vHypothesis.clear();
            


        if (debug)
            cout << endl;


        hypshiftfile.close();
    } /* Continue to next shifting step ...  */
}
void forceSegmentsWrdSilSkip3() {
    unsigned numalign = 1;
    float NullPost = 1;    

    vector<double> vMaxScoresCN; //vector to keep PP of maxScore per CSet, just for display purposes

    cout << "******* Skips to apply: 3  ****** " << endl;
  
    for (short iSkips = 0; iSkips < (vBoundaries.size()-2); iSkips++) {
        if (debug) cout << "Skip: " << vBoundaries[iSkips] << "," << vBoundaries[iSkips+1] << "," << vBoundaries[iSkips+2] << " ";
        ostringstream fileNameStream("");        
        fileNameStream << prefixout << "skip3.txt";
        string outfilename = fileNameStream.str();        
        File hypshiftfile(outfilename.c_str(), "a");
        
        /* Duplicate vector of boundaries skipping iSkip */
        vector<float> vBoundShifted;
        vector<short> vIndexOriginal;
        if (debug) cout << "[ " ;
        for (short iBoundcopy = 0; iBoundcopy < vBoundaries.size(); iBoundcopy++) {
            if (! ((iBoundcopy == iSkips) | (iBoundcopy == iSkips+1) | (iBoundcopy == iSkips+2)) ){
                vBoundShifted.push_back(vBoundaries[iBoundcopy]);
                vIndexOriginal.push_back(iBoundcopy);
                if (debug) cout << vBoundaries[iBoundcopy] << " ";
            }
        }
        if (debug) cout << "]" << endl;
        
        /* Go through skipped set of boundaries, getting hypothesis */
        for (short iCoupleBound = 0; iCoupleBound < (vBoundShifted.size() - 1); iCoupleBound++) {
            string sCurrentRefword = vRefwords[vIndexOriginal[iCoupleBound]];
            NullPost = 1;
            vector<unsigned> vMicid;
            vector<double> vPosts;
            vector<string> vWords, vSegmentWords;
            vector<double> vFinalScores;

            float tS = vBoundShifted[iCoupleBound], tE = vBoundShifted[iCoupleBound + 1], DeltaT = 0.01;
            if (debug)
                cout << "(t:" << tS << " " << tE << "):";

            /* Start link selection */
            ifstream inputFile(tmpDataFile);
            string line;

            /* Select links accomplishing the boundary constraints */
            while (getline(inputFile, line)) {
                if (!line.length() || line[0] == '#')
                    continue;
                unsigned numLatNode = 0, idMic = 0;
                char cWordlink[20];
                string sWordlink;
                float startTime = 0., endTime = 0.;
                double postProb = 0.;

                sscanf(line.c_str(), "%u %19s %f %f %lf %u", &numLatNode, cWordlink, &startTime, &endTime, &postProb, &idMic);
                sWordlink = (string) cWordlink;

                /* Ommit <s> </s> in octave, here is equivalent to NULLs*/
                /* || (sWordlink.compare(str2) == 0 */
                if (!((sWordlink.find("<s>") != std::string::npos) | (sWordlink.find("NULL") != std::string::npos) | (sWordlink.find("</s>") != std::string::npos))) {
                    Boolean startIsValid = false, endIsValid = false;

                    /* Validate Start/End +-Delta boundaries */
                    float midpoint = startTime + ((endTime - startTime) / 2.0);
                    switch (boundsvalidation) {
                        case 1:
                            // Validate Start/End with separate deltas */
                            startIsValid = ((startTime >= (tS - manualdeltaS)) && (startTime <= (tS + manualdeltaS)));
                            endIsValid = ((endTime <= (tE + manualdeltaE)) && (endTime >= (tE - manualdeltaE)));
                            break;
                        case 2:
                            // Use MIDPOINT at a deltatolerance from Start/End */                        
                            if ((midpoint >= tS & midpoint <= tE) | (fabs(midpoint - tS) <= manualdelta) | (fabs(midpoint - tE) <= manualdelta)) {
                                startIsValid = true;
                                endIsValid = true;
                            }
                            break;
                        case 0:
                            // Validate if Start/End are at a DeltaT from boundaries */
                            if ((tE - tS) < (deltaref)) {
                                DeltaT = ((tE - tS) / 2);
                            } else {
                                DeltaT = ((tE - tS) * lratio);
                            }
                            startIsValid = ((startTime >= (tS - DeltaT)) && (startTime <= (tS + DeltaT)));
                            endIsValid = ((endTime <= (tE + DeltaT)) && (endTime >= (tE - DeltaT)));
                            break;
                    }

                    if (startIsValid && endIsValid) {
                        vPosts.push_back(postProb);
                        vWords.push_back(sWordlink);
                        vSegmentWords.push_back(sWordlink);
                        vMicid.push_back(idMic);
                        if (debug > 1) cout << "l[" << sWordlink << " " << startTime << "," << endTime << " " << postProb << " " << idMic << "] ";
                    }
                }
            }
            /* If links were found ... compute avgInterMic(sumIntraMic(PP)) */
            if (vWords.size() > 0) {
                if (debug)
                    cout << "  Found " << vPosts.size() << " links.." << endl;
                /* Get list of unique occurrence of words */
                std::sort(vSegmentWords.begin(), vSegmentWords.end());
                vSegmentWords.erase(std::unique(vSegmentWords.begin(), vSegmentWords.end()), vSegmentWords.end());

                /* ****************** DISPLAY ***************** */
                if (debug) {
                    cout << "   Unique words: ";
                    for (unsigned i = 0; i < vSegmentWords.size(); i++) {
                        cout << vSegmentWords[i] << " ";
                    }
                    cout << endl << "   ";
                }

                /* ======================================== */
                unsigned numWords = vSegmentWords.size();

                vector<vector<double> > mWordsMics;
                short sIndexRefword = -1;

                /* Per each word */
                for (unsigned w = 0; w < numWords; w++) {
                    vector<unsigned> vIndex;
                    string currentWord = vSegmentWords[w];
                    if (currentWord.compare(sCurrentRefword) == 0) {
                        /* Keep index of refWord in list of words   */
                        /* to later extract PP from InterMic matrix */
                        sIndexRefword = w;
                    }
                    for (unsigned i = 0; i < vWords.size(); i++) {
                        if (vWords[i] == currentWord) {
                            vIndex.push_back(i);
                        }
                    }

                    /* Per each microphone */
                    vector<double> vMics;
                    for (unsigned mic = 0; mic < numMics; mic++) {
                        /* Get indexes of links of current Mic */ //cout << "<Mic" << mic << " ";
                        vector<unsigned> vIndexMic;
                        for (unsigned selectedlink = 0; selectedlink < vIndex.size(); selectedlink++) {
                            if (vMicid[vIndex[selectedlink]] == mic) {
                                vIndexMic.push_back(vIndex[selectedlink]);
                            }
                        }

                        /* IntraMic Score */
                        double Postscore = .0;
                        if (vIndexMic.size() > 0) {
                            for (unsigned i = 0; i < vIndexMic.size(); i++) {
                                Postscore = Postscore + vPosts[vIndexMic[i]];
                            }
                        }
                        vMics.push_back(Postscore);
                    }
                    mWordsMics.push_back(vMics);
                }

                /* InterMic Score */
                vector<double> vNewPost;
                double sumvNewPost = .0, sumvNewPost2 = .0;
                for (unsigned wrow = 0; wrow < mWordsMics.size(); wrow++) {
                    double newPost = .0;

                    double maxscoreInterM = 0;
                    for (unsigned mic = 0; mic < numMics; mic++) {
                        newPost = newPost + mWordsMics[wrow][mic];
                        if (mWordsMics[wrow][mic] > maxscoreInterM) {
                            maxscoreInterM = mWordsMics[wrow][mic];
                        }
                    }
                    double interMicWordScore = 0;
                    switch (scoremethod) {
                        case 1:
                            /* AVG InterMic */
                            interMicWordScore = (newPost / numMics);
                            break;
                        case 2:
                            /* MAX among all mics */
                            interMicWordScore = maxscoreInterM; //Use maxscore instead of AvgInterMic
                            break;
                    }
                    vNewPost.push_back(interMicWordScore);
                    sumvNewPost = sumvNewPost + (interMicWordScore);
                }

                /* Normalization if sumPP of all candidates is >1 */
                if (debug) {
                    cout << " Final Scores ";
                    cout << "[vNewPost size " << vNewPost.size() << "] ";
                    cout << " [alpha*sumvNewPost " << alpha * sumvNewPost << "] ";
                }
                if ((alpha * sumvNewPost) > 1) {
                    for (unsigned i = 0; i < vNewPost.size(); i++) {
                        vFinalScores.push_back(vNewPost[i] / (sumvNewPost));
                        sumvNewPost2 += (vNewPost[i] / (sumvNewPost));
                        if (debug)
                            cout << (vNewPost[i] / (sumvNewPost)) << " ";
                    }
                } else {
                    for (unsigned i = 0; i < vNewPost.size(); i++) {
                        vFinalScores.push_back(vNewPost[i]);
                        sumvNewPost2 += vNewPost[i];
                        //cout << vFinalScores[i] << " " ;
                        if (debug)
                            cout << "PPw " << (vNewPost[i]) << " ";
                    }
                }

                /* Criterion to continue/stop search of segment */
                NullPost = 1 - sumvNewPost2;

                //                    /* Keep track of reference hypothesis scores */
                //                    if (sIndexRefword>-1) {
                //                        /* WRef was in set of words in confset */
                //                        vRefwordsScore.push_back(vFinalScores[sIndexRefword]);
                //                    } else {
                //                        /* WRef was not in set of words in confset */
                //                        vRefwordsScore.push_back(0.0);
                //                    }

            } else {
                NullPost = 1;
                if (debug)
                    cout << "  Found no valid links in this segment." << endl;
                //                    /* Keep track of reference hypothesis scores */
                //                    vRefwordsScore.push_back(0.0);
            }

            if (debug)
                cout << " (NullPost " << NullPost << ") " << endl;

            /* Finally if Set is valid, add NULL to set */
            //if ((NullPost <= THRSNULL) & (vPosts.size() > 0)) {
            //        if (vPosts.size() > 0) {

            /* Add NULL word and score to stack of candidates */
            vFinalScores.push_back(NullPost);
            vSegmentWords.push_back("NULL");

            /* Find maximum candidate score */
            int indexMax = vSegmentWords.size() - 1;
            double maxscore = .0;
            /* Compose CN file */
            // fprintf(cnTmpBody, "align %u", (numalign));
            for (unsigned i = 0; i < vSegmentWords.size(); i++) {
                if (vFinalScores[i] > maxscore) {
                    indexMax = i;
                    maxscore = vFinalScores[i];
                }
                //                    if (vFinalScores[i] > 0.0000009) {
                //                        //fprintf(cnTmpBody, " %s %f", vSegmentWords[i].c_str(), vFinalScores[i]);
                //                    }
            }
            //            fprintf(cnTmpBody, "\n");

            if (debug) {
                cout << "<< MaxScore " << maxscore << " Index " << indexMax << " :: " << vSegmentWords[indexMax] << ">>";
            }

            vHypothesis.push_back(vSegmentWords[indexMax]);
            vMaxScoresCN.push_back(vFinalScores[indexMax]);
            //            currentalign++;
            NullPost = 0;
            numalign++;
            //        }

        }
        if (debug) cout << "(H" << iSkips << ": ";
        ostringstream wholehypothesis("");
        for (unsigned i = 0; i < vHypothesis.size(); i++) {
            string hypword = vHypothesis[i];
            if (!(hypword.compare("NULL") == 0)) {
                wholehypothesis << hypword << " ";
                //if (debug) cout << hypword << " ";
            } else {
                //wholehypothesis << "sil "; /* Print silence */ 
            }
            if (debug) cout << hypword << " ";
        }
        string swholehypothesis = wholehypothesis.str();

        fprintf(hypshiftfile, "%s\n", swholehypothesis.c_str());
        
        if (debug)     cout << ")" << endl;
        vHypothesis.clear();
            
            


        if (debug)
            cout << endl;


        hypshiftfile.close();
    } /* Continue to next shifting step ...  */
}

void test(){
 /* Update vBounds */
    vBoundariesTests.clear();
    for (short iVUpdate = 0; iVUpdate < vBoundaries.size(); iVUpdate++) {
        vBoundariesTests.push_back(vBoundaries[iVUpdate]);
    }

    /* Run SegmentValidation with vBoundariesTests */
    validateSegmentsInc(0);}
/* After reading reference boundaries, shift boundaries and *
 * run segmentvalidation on the new set of boundaries */
void forceSegmentsWrdSilSegmV() {
    if (debug)
        cout << "Validating temporal segments [#: " << vCoupleBoundaries.size() << "]" << endl;

    /* =============================================== */
    double allshifts[] = {-0.20, -0.15, -0.10, -0.05, 0.00, 0.05, 0.10, 0.15, 0.20};
    
    short numshifts = (sizeof ((allshifts)) / sizeof ((allshifts[0])));

    for (short iShifts = 0; iShifts < numshifts; iShifts++) {
        cout << "******* Shift to apply ****** " << allshifts[iShifts] << endl;

        vector<float> vBoundShifted;

        for (short iCoupleBoundx = 0; iCoupleBoundx < (vBoundaries.size()); iCoupleBoundx++) {
            Boolean boundshifted=false;
            /* Duplicate vector of boundaries */
            vBoundShifted.clear();
            for (short iBoundcopy = 0; iBoundcopy < vBoundaries.size(); iBoundcopy++) {
                vBoundShifted.push_back(vBoundaries[iBoundcopy]);
            }
            
            if (fabs(allshifts[iShifts]) > 0) {
                /* Single shifting on extremes 0/END */
                if ((iCoupleBoundx == 0) | (iCoupleBoundx == (vBoundaries.size() - 1))) {
                    if (iCoupleBoundx == 0) {
                        /* only positive shifting */
                        if (allshifts[iShifts] > 0) {
                            float previousvalue = vBoundShifted[iCoupleBoundx];
                            vBoundShifted[iCoupleBoundx] = previousvalue + allshifts[iShifts];
                            boundshifted=true;
                        }
                    } else {
                        /* only negative shifting */
                        if (allshifts[iShifts] < 0) {
                            float previousvalue = vBoundShifted[iCoupleBoundx];
                            vBoundShifted[iCoupleBoundx] = previousvalue + allshifts[iShifts];
                            boundshifted=true;
                        }
                    }
                } else {
                    /* Positive and Negative shifting */
                    float previousvalue = vBoundShifted[iCoupleBoundx];
                    float previewvalueprev = vBoundShifted[iCoupleBoundx - 1];
                    float previewvalue = previousvalue + allshifts[iShifts];
                    float previewvaluenext = vBoundShifted[iCoupleBoundx + 1];
                    /* Apply only if values are between extremes 0/END, if prev/next boundaries are not overlapped */
                    if (previewvalue > 0 & previewvalue < vBoundaries[vBoundaries.size()-1] & (previewvalue > previewvalueprev) & (previewvalue < previewvaluenext)) {
                        vBoundShifted[iCoupleBoundx] = previousvalue + allshifts[iShifts];
                        boundshifted=true;
                    }
                }
            } else {
                if (iCoupleBoundx == 0){
                    boundshifted=true; //
                }
            }
            
            if (boundshifted){
                /* Update vBounds */
                if (debugx) cout << "[NewB:";
                vBoundariesTests.clear();
                for (short iVUpdate = 0; iVUpdate < vBoundShifted.size(); iVUpdate++) {
                    vBoundariesTests.push_back(vBoundShifted[iVUpdate]);
                    if (debugx) cout << vBoundShifted[iVUpdate] << " ";
                }
                if (debugx) cout << "]" << endl;

                /* Run SegmentValidation with vBoundariesTests */
                validateSegmentsInc(iShifts);
                
            }
            
        } /* read original boundaries to check which one should be shifted */        
        
    } /* Continue to next shifting step ...  */
}
/* Explicitly use wrd time boundary couples to extract ConfSets*/
/* Shifthing one boundary at a time */
void forceSegmentsWrdSil() {
    unsigned numalign = 1;
    //unsigned boundStart = 0, boundEnd = 0, currentalign = 1;
    float NullPost = 1;
    if (debug)
        cout << "Validating temporal segments [#: " << vCoupleBoundaries.size() << "]" << endl;


    /* =============================================== */
    vector<double> vMaxScoresCN; //vector to keep PP of maxScore per CSet, just for display purposes
    vector<double> vRefwordsScore;
    //double allshifts[]={-0.09, -0.06, -0.03, 0.0, 0.03, 0.06, 0.09};
    double allshifts[]={-0.20, -0.15, -0.10, -0.05, 0.00, 0.05 , 0.10, 0.15, 0.20 };
    short numshifts=(sizeof((allshifts))/sizeof((allshifts[0])));
    
    
    for (short iShifts = 0; iShifts < numshifts; iShifts++) {
        ostringstream fileNameStream("");        
        fileNameStream << prefixout << allshifts[iShifts] << ".txt";
        string outfilename = fileNameStream.str();        
        File hypshiftfile(outfilename.c_str(), "a");        

        cout << "******* Shift to apply ****** " << allshifts[iShifts] << endl;

        for (short iCoupleBoundx = 0; iCoupleBoundx < (vBoundaries.size()); iCoupleBoundx++) {            
            Boolean boundshifted=false;
            /* Duplicate vector of boundaries */
            vector<float> vBoundShifted;
            for (short iBoundcopy = 0; iBoundcopy < vBoundaries.size(); iBoundcopy++) {
                vBoundShifted.push_back(vBoundaries[iBoundcopy]);
            }
            if (fabs(allshifts[iShifts]) > 0) {
                /* Single shifting on extremes 0/END */
                if ((iCoupleBoundx == 0) | (iCoupleBoundx == (vBoundaries.size() - 1))) {
                    if (iCoupleBoundx == 0) {
                        /* only positive shifting */
                        if (allshifts[iShifts] > 0) {
                            float previousvalue = vBoundShifted[iCoupleBoundx];
                            vBoundShifted[iCoupleBoundx] = previousvalue + allshifts[iShifts];
                            boundshifted=true;
                        }
                    } else {
                        /* only negative shifting */
                        if (allshifts[iShifts] < 0) {
                            float previousvalue = vBoundShifted[iCoupleBoundx];
                            vBoundShifted[iCoupleBoundx] = previousvalue + allshifts[iShifts];
                            boundshifted=true;
                        }
                    }
                } else {
                    /* Positive and Negative shifting */
                    float previousvalue = vBoundShifted[iCoupleBoundx];
                    float previewvalueprev = vBoundShifted[iCoupleBoundx - 1];
                    float previewvalue = previousvalue + allshifts[iShifts];
                    float previewvaluenext = vBoundShifted[iCoupleBoundx + 1];
                    /* Apply only if values are between extremes 0/END, if prev/next boundaries are not overlapped */
                    if (previewvalue > 0 & previewvalue < vBoundaries[vBoundaries.size()-1] & (previewvalue > previewvalueprev) & (previewvalue < previewvaluenext)) {
                        vBoundShifted[iCoupleBoundx] = previousvalue + allshifts[iShifts];
                        boundshifted=true;
                    }
                }
            } else {
                boundshifted=true;
            }

            if (boundshifted) {
                for (short iCoupleBound = 0; iCoupleBound < (vBoundaries.size() - 1); iCoupleBound++) {
                    string sCurrentRefword = vRefwords[iCoupleBound];
                    NullPost = 1;
                    vector<unsigned> vMicid;
                    vector<double> vPosts;
                    vector<string> vWords, vSegmentWords;
                    vector<double> vFinalScores;

                    float tS = vBoundShifted[iCoupleBound], tE = vBoundShifted[iCoupleBound + 1], DeltaT = 0.01;
                    if (debug)
                        cout << "(t:" << tS << " " << tE << "):";

                    /* Start link selection */
                    ifstream inputFile(tmpDataFile);
                    string line;

                    /* Select links accomplishing the boundary constraints */
                    while (getline(inputFile, line)) {
                        if (!line.length() || line[0] == '#')
                            continue;
                        unsigned numLatNode = 0, idMic = 0;
                        char cWordlink[20];
                        string sWordlink;
                        float startTime = 0., endTime = 0.;
                        double postProb = 0.;

                        sscanf(line.c_str(), "%u %19s %f %f %lf %u", &numLatNode, cWordlink, &startTime, &endTime, &postProb, &idMic);
                        sWordlink = (string) cWordlink;

                        /* Ommit <s> </s> in octave, here is equivalent to NULLs*/
                        /* || (sWordlink.compare(str2) == 0 */
                        if (!((sWordlink.find("<s>") != std::string::npos) | (sWordlink.find("NULL") != std::string::npos) | (sWordlink.find("</s>") != std::string::npos))) {
                            Boolean startIsValid = false, endIsValid = false;

                            /* Validate Start/End +-Delta boundaries */
                            float midpoint = startTime + ((endTime - startTime) / 2.0);
                            switch (boundsvalidation) {
                                case 1:
                                    // Validate Start/End with separate deltas */
                                    startIsValid = ((startTime >= (tS - manualdeltaS)) && (startTime <= (tS + manualdeltaS)));
                                    endIsValid = ((endTime <= (tE + manualdeltaE)) && (endTime >= (tE - manualdeltaE)));
                                    break;
                                case 2:
                                    // Use MIDPOINT at a deltatolerance from Start/End */                        
                                    if ((midpoint >= tS & midpoint <= tE) | (fabs(midpoint - tS) <= manualdelta) | (fabs(midpoint - tE) <= manualdelta)) {
                                        startIsValid = true;
                                        endIsValid = true;
                                    }
                                    break;
                                case 0:
                                    // Validate if Start/End are at a DeltaT from boundaries */
                                    if ((tE - tS) < (deltaref)) {
                                        DeltaT = ((tE - tS) / 2);
                                    } else {
                                        DeltaT = ((tE - tS) * lratio);
                                    }
                                    startIsValid = ((startTime >= (tS - DeltaT)) && (startTime <= (tS + DeltaT)));
                                    endIsValid = ((endTime <= (tE + DeltaT)) && (endTime >= (tE - DeltaT)));
                                    break;
                            }

                            if (startIsValid && endIsValid) {
                                vPosts.push_back(postProb);
                                vWords.push_back(sWordlink);
                                vSegmentWords.push_back(sWordlink);
                                vMicid.push_back(idMic);
                                if (debug > 1) cout << "l[" << sWordlink << " " << startTime << "," << endTime << " " << postProb << " " << idMic << "] ";
                            }
                        }
                    }
                    /* If links were found ... compute avgInterMic(sumIntraMic(PP)) */
                    if (vWords.size() > 0) {
                        if (debug)
                            cout << "  Found " << vPosts.size() << " links.." << endl;
                        /* Get list of unique occurrence of words */
                        std::sort(vSegmentWords.begin(), vSegmentWords.end());
                        vSegmentWords.erase(std::unique(vSegmentWords.begin(), vSegmentWords.end()), vSegmentWords.end());

                        /* ****************** DISPLAY ***************** */
                        if (debug) {
                            cout << "   Unique words: ";
                            for (unsigned i = 0; i < vSegmentWords.size(); i++) {
                                cout << vSegmentWords[i] << " ";
                            }
                            cout << endl << "   ";
                        }

                        /* ======================================== */
                        unsigned numWords = vSegmentWords.size();

                        vector<vector<double> > mWordsMics;
                        short sIndexRefword = -1;

                        /* Per each word */
                        for (unsigned w = 0; w < numWords; w++) {
                            vector<unsigned> vIndex;
                            string currentWord = vSegmentWords[w];
                            if (currentWord.compare(sCurrentRefword) == 0) {
                                /* Keep index of refWord in list of words   */
                                /* to later extract PP from InterMic matrix */
                                sIndexRefword = w;
                            }
                            for (unsigned i = 0; i < vWords.size(); i++) {
                                if (vWords[i] == currentWord) {
                                    vIndex.push_back(i);
                                }
                            }

                            /* Per each microphone */
                            vector<double> vMics;
                            for (unsigned mic = 0; mic < numMics; mic++) {
                                /* Get indexes of links of current Mic */ //cout << "<Mic" << mic << " ";
                                vector<unsigned> vIndexMic;
                                for (unsigned selectedlink = 0; selectedlink < vIndex.size(); selectedlink++) {
                                    if (vMicid[vIndex[selectedlink]] == mic) {
                                        vIndexMic.push_back(vIndex[selectedlink]);
                                    }
                                }

                                /* IntraMic Score */
                                double Postscore = .0;
                                if (vIndexMic.size() > 0) {
                                    for (unsigned i = 0; i < vIndexMic.size(); i++) {
                                        Postscore = Postscore + vPosts[vIndexMic[i]];
                                    }
                                }
                                vMics.push_back(Postscore);
                            }
                            mWordsMics.push_back(vMics);
                        }

                        /* InterMic Score */
                        vector<double> vNewPost;
                        double sumvNewPost = .0, sumvNewPost2 = .0;
                        for (unsigned wrow = 0; wrow < mWordsMics.size(); wrow++) {
                            double newPost = .0;

                            double maxscoreInterM = 0;
                            for (unsigned mic = 0; mic < numMics; mic++) {
                                newPost = newPost + mWordsMics[wrow][mic];
                                if (mWordsMics[wrow][mic] > maxscoreInterM) {
                                    maxscoreInterM = mWordsMics[wrow][mic];
                                }
                            }
                            double interMicWordScore = 0;
                            switch (scoremethod) {
                                case 1:
                                    /* AVG InterMic */
                                    interMicWordScore = (newPost / numMics);
                                    break;
                                case 2:
                                    /* MAX among all mics */
                                    interMicWordScore = maxscoreInterM; //Use maxscore instead of AvgInterMic
                                    break;
                            }
                            vNewPost.push_back(interMicWordScore);
                            sumvNewPost = sumvNewPost + (interMicWordScore);
                        }

                        /* Normalization if sumPP of all candidates is >1 */
                        if (debug) {
                            cout << " Final Scores ";
                            cout << "[vNewPost size " << vNewPost.size() << "] ";
                            cout << " [alpha*sumvNewPost " << alpha * sumvNewPost << "] ";
                        }
                        if ((alpha * sumvNewPost) > 1) {
                            for (unsigned i = 0; i < vNewPost.size(); i++) {
                                vFinalScores.push_back(vNewPost[i] / (sumvNewPost));
                                sumvNewPost2 += (vNewPost[i] / (sumvNewPost));
                                if (debug)
                                    cout << (vNewPost[i] / (sumvNewPost)) << " ";
                            }
                        } else {
                            for (unsigned i = 0; i < vNewPost.size(); i++) {
                                vFinalScores.push_back(vNewPost[i]);
                                sumvNewPost2 += vNewPost[i];
                                //cout << vFinalScores[i] << " " ;
                                if (debug)
                                    cout << "PPw " << (vNewPost[i]) << " ";
                            }
                        }

                        /* Criterion to continue/stop search of segment */
                        NullPost = 1 - sumvNewPost2;


                    } else {
                        NullPost = 1;
                        if (debug)
                            cout << "  Found no valid links in this segment." << endl;
                        //                    /* Keep track of reference hypothesis scores */
                        //                    vRefwordsScore.push_back(0.0);
                    }

                    if (debug)
                        cout << " (NullPost " << NullPost << ") " << endl;

                    /* Finally if Set is valid, add NULL to set */
                    //if ((NullPost <= THRSNULL) & (vPosts.size() > 0)) {
                    //        if (vPosts.size() > 0) {

                    /* Add NULL word and score to stack of candidates */
                    vFinalScores.push_back(NullPost);
                    vSegmentWords.push_back("NULL");

                    /* Find maximum candidate score */
                    int indexMax = vSegmentWords.size() - 1;
                    double maxscore = .0;
                    /* Compose CN file */
                    // fprintf(cnTmpBody, "align %u", (numalign));
                    for (unsigned i = 0; i < vSegmentWords.size(); i++) {
                        if (vFinalScores[i] > maxscore) {
                            indexMax = i;
                            maxscore = vFinalScores[i];
                        }
                    }

                    if (debug) {
                        cout << "<< MaxScore " << maxscore << " Index " << indexMax << " :: " << vSegmentWords[indexMax] << ">>";
                    }

                    vHypothesis.push_back(vSegmentWords[indexMax]);
                    vMaxScoresCN.push_back(vFinalScores[indexMax]);
                    //            currentalign++;
                    NullPost = 0;
                    numalign++;
                    //        }

                }
                if (debug)
                    cout << "[H" << iCoupleBoundx << ": ";                
                ostringstream wholehypothesis("");                
                for (unsigned i = 0; i < vHypothesis.size(); i++) {
                    string hypword = vHypothesis[i];
                    if (!(hypword.compare("NULL") == 0)) {
                        wholehypothesis << hypword << " ";
                        if (debug)
                            cout << hypword << " ";
                    } else {
                        if (printsilence) wholehypothesis << "sil "; //CGF TODO: Allow printing silences
                    }
                }
                string swholehypothesis = wholehypothesis.str();
                
                fprintf(hypshiftfile, "%s\n", swholehypothesis.c_str());
                if (debug)
                    cout << "]" << endl;
                vHypothesis.clear();
            }
            
        }
        

        if (debug)
            cout << endl;


        hypshiftfile.close();
    } /* Continue to next shifting step ...  */
}

/* Explicitly use wrd time boundary couples to extract ConfSets*/
void forceSegmentsWrd() {
    unsigned numalign = 1;
    unsigned boundStart = 0, boundEnd = 0, currentalign = 1;
    float NullPost = 1;
    if (debug)
        cout << "Validating temporal segments [Boundaries: " << vCoupleBoundaries.size() << "]" << endl;

    /* Compose CN file */
    unsigned length = strlen(outCNfilename);
    char *copyCnFile = new char[length + 1]();
    strcpy(copyCnFile, outCNfilename);
    char *tmpCnFile = strcat(copyCnFile, "tmp");
    File cnTmpBody(tmpCnFile, "w");
    /* Save Boundaries to File */
    if (savebounds == 1) {
        File boundsFinal(outBoundfilename, "w"); //fprintf(boundsFinal, "Valid segments\n");
        boundsFinal.close();
    }

    /* =============================================== */
    vector<double> vMaxScoresCN; //vector to keep PP of maxScore per CSet, just for display purposes
    vector<double> vRefwordsScore;
    for (short iCoupleBound = 0; iCoupleBound < vCoupleBoundaries.size(); iCoupleBound++) {
        string sCurrentRefword = vRefwords[iCoupleBound];
        //cout << endl << "(t:" << vCoupleBoundaries[iCoupleBound][0]*16000 << " " << vCoupleBoundaries[iCoupleBound][1]*16000 << "):";
        //boundStart=vCoupleBoundaries[iCoupleBound][0]; boundEnd=vCoupleBoundaries[iCoupleBound][1];
        NullPost = 1;
        vector<unsigned> vMicid;
        vector<double> vPosts;
        vector<string> vWords, vSegmentWords;
        vector<double> vFinalScores;
        boundEnd = boundEnd + 1;
        float tS = vCoupleBoundaries[iCoupleBound][0], tE = vCoupleBoundaries[iCoupleBound][1], DeltaT = 0.01;
        
        /* Manually shifted boundary*/
        /* 1: so it wont become negative*/
        /* Validate that it's respecting tStart */
        /* default coupleboundtoshift=1 */
        if (iCoupleBound==coupleboundtoshift){
            if (shiftendbound==1){
                if ( tS<(tE+shiftbound) ) {
                    tE=tE+shiftbound;
                }
            } else {
                if ( tE>(tS+shiftbound) ) {
                    tS=tS+shiftbound;
                }
            }
        }
        cout << endl << "(t:" << tS << " " << tE << "):";

        /* Start link selection */
        ifstream inputFile(tmpDataFile);
        string line;

        /* Select links accomplishing the boundary constraints */
        while (getline(inputFile, line)) {
            if (!line.length() || line[0] == '#')
                continue;
            unsigned numLatNode = 0, idMic = 0;
            char cWordlink[20];
            string sWordlink;
            float startTime = 0., endTime = 0.;
            double postProb = 0.;

            sscanf(line.c_str(), "%u %19s %f %f %lf %u", &numLatNode, cWordlink, &startTime, &endTime, &postProb, &idMic);
            sWordlink = (string) cWordlink;

            /* Ommit <s> </s> in octave, here is equivalent to NULLs*/
            /* || (sWordlink.compare(str2) == 0 */
            if (!((sWordlink.find("<s>") != std::string::npos) | (sWordlink.find("NULL") != std::string::npos) | (sWordlink.find("</s>") != std::string::npos))) {
                Boolean startIsValid = false, endIsValid = false;

                /* Validate Start/End +-Delta boundaries */
                float midpoint = startTime + ((endTime - startTime) / 2.0);
                switch (boundsvalidation) {
                    case 1:
                        // Validate Start/End with separate deltas */
                        startIsValid = ((startTime >= (tS - manualdeltaS)) && (startTime <= (tS + manualdeltaS)));
                        endIsValid = ((endTime <= (tE + manualdeltaE)) && (endTime >= (tE - manualdeltaE)));
                        break;
                    case 2:
                        // Use MIDPOINT at a deltatolerance from Start/End */                        
                        if ((midpoint >= tS & midpoint <= tE) | (fabs(midpoint - tS) <= manualdelta) | (fabs(midpoint - tE) <= manualdelta)) {
                            startIsValid = true;
                            endIsValid = true;
                        }
                        break;
                    case 0:
                        // Validate if Start/End are at a DeltaT from boundaries */
                        if ((tE - tS) < (deltaref)) {
                            DeltaT = ((tE - tS) / 2);
                        } else {
                            DeltaT = ((tE - tS) * lratio);
                        }
                        startIsValid = ((startTime >= (tS - DeltaT)) && (startTime <= (tS + DeltaT)));
                        endIsValid = ((endTime <= (tE + DeltaT)) && (endTime >= (tE - DeltaT)));
                        break;
                }

                if (startIsValid && endIsValid) {
                    vPosts.push_back(postProb);
                    vWords.push_back(sWordlink);
                    vSegmentWords.push_back(sWordlink);
                    vMicid.push_back(idMic);
                    if (debug > 1) cout << "l[" << sWordlink << " " << startTime << "," << endTime << " " << postProb << " " << idMic << "] ";
                }
            }
        }
        /* If links were found ... compute avgInterMic(sumIntraMic(PP)) */
        if (vWords.size() > 0) {
            if (debug)
                cout << "  Found " << vPosts.size() << " links.." << endl;
            /* Get list of unique occurrence of words */
            std::sort(vSegmentWords.begin(), vSegmentWords.end());
            vSegmentWords.erase(std::unique(vSegmentWords.begin(), vSegmentWords.end()), vSegmentWords.end());

            /* ****************** DISPLAY ***************** */
            if (debug) {
                cout << "   Unique words: ";
                for (unsigned i = 0; i < vSegmentWords.size(); i++) {
                    cout << vSegmentWords[i] << " ";
                }
                cout << endl << "   ";
            }

            /* ======================================== */
            unsigned numWords = vSegmentWords.size();

            vector<vector<double> > mWordsMics;
            short sIndexRefword = -1;

            /* Per each word */
            for (unsigned w = 0; w < numWords; w++) {
                vector<unsigned> vIndex;
                string currentWord = vSegmentWords[w];
                if (currentWord.compare(sCurrentRefword) == 0) { //currentWord == sCurrentRefword ){
                    /* Keep index of refWord in list of words   */
                    /* to later extract PP from InterMic matrix */
                    sIndexRefword = w;
                }
                for (unsigned i = 0; i < vWords.size(); i++) {
                    if (vWords[i] == currentWord) {
                        vIndex.push_back(i);
                    }
                }

                /* Per each microphone */
                vector<double> vMics;
                for (unsigned mic = 0; mic < numMics; mic++) {
                    /* Get indexes of links of current Mic */

                    //cout << "<Mic" << mic << " ";
                    vector<unsigned> vIndexMic;
                    for (unsigned selectedlink = 0; selectedlink < vIndex.size(); selectedlink++) {
                        if (vMicid[vIndex[selectedlink]] == mic) {
                            vIndexMic.push_back(vIndex[selectedlink]);
                        }
                    }

                    /* IntraMic Score */
                    double Postscore = .0;
                    if (vIndexMic.size() > 0) {
                        for (unsigned i = 0; i < vIndexMic.size(); i++) {
                            Postscore = Postscore + vPosts[vIndexMic[i]];
                            //cout << vPosts[vIndexMic[i]] << " ";
                        }
                    } else {
                        //Postscore = Postscore + 0; //cout << "P0 ";
                    }
                    vMics.push_back(Postscore);
                    //cout << "> ";
                }
                mWordsMics.push_back(vMics);
            }
            //cout << endl ;

            /* InterMic Score */
            vector<double> vNewPost;
            double sumvNewPost = .0, sumvNewPost2 = .0;
            for (unsigned wrow = 0; wrow < mWordsMics.size(); wrow++) {
                double newPost = .0;

                double maxscoreInterM = 0;
                for (unsigned mic = 0; mic < numMics; mic++) {
                    newPost = newPost + mWordsMics[wrow][mic];
                    if (mWordsMics[wrow][mic] > maxscoreInterM) {
                        maxscoreInterM = mWordsMics[wrow][mic];
                    }
                }
                double interMicWordScore = 0;
                switch (scoremethod) {
                    case 1:
                        /* AVG InterMic */
                        interMicWordScore = (newPost / numMics);
                        break;
                    case 2:
                        /* MAX among all mics */
                        interMicWordScore = maxscoreInterM; //Use maxscore instead of AvgInterMic
                        break;
                }

                vNewPost.push_back(interMicWordScore);
                sumvNewPost = sumvNewPost + (interMicWordScore);
                //cout << interMicWordScore << " " ;
            }
            //cout << endl;

            /* Normalization if sumPP of all candidates is >1 */
            if (debug) {
                cout << " Final Scores ";
                cout << "[vNewPost size " << vNewPost.size() << "] ";
                cout << " [alpha*sumvNewPost " << alpha * sumvNewPost << "] ";
            }
            if ((alpha * sumvNewPost) > 1) {
                for (unsigned i = 0; i < vNewPost.size(); i++) {
                    vFinalScores.push_back(vNewPost[i] / (sumvNewPost));
                    sumvNewPost2 += (vNewPost[i] / (sumvNewPost));
                    if (debug)
                        cout << (vNewPost[i] / (sumvNewPost)) << " ";
                }
            } else {
                for (unsigned i = 0; i < vNewPost.size(); i++) {
                    vFinalScores.push_back(vNewPost[i]);
                    sumvNewPost2 += vNewPost[i];
                    //cout << vFinalScores[i] << " " ;
                    if (debug)
                        cout << "PPw " << (vNewPost[i]) << " ";
                }
            }

            /* Criterion to continue/stop search of segment */
            NullPost = 1 - sumvNewPost2;

            /* Keep track of reference hypothesis scores */
            if (sIndexRefword>-1) {
                /* WRef was in set of words in confset */
                vRefwordsScore.push_back(vFinalScores[sIndexRefword]);
            } else {
                /* WRef was not in set of words in confset */
                vRefwordsScore.push_back(0.0);
            }

        } else {
            NullPost = 1;
            if (debug)
                cout << "  Found no valid links in this segment." << endl;
            /* Keep track of reference hypothesis scores */
            vRefwordsScore.push_back(0.0);
        }

        cout << " (NullPost " << NullPost << ") " << endl;

        /* Finally if Set is valid, add NULL to set */
        //if ((NullPost <= THRSNULL) & (vPosts.size() > 0)) {
        //        if (vPosts.size() > 0) {

        /* Add NULL word and score to stack of candidates */
        vFinalScores.push_back(NullPost);
        vSegmentWords.push_back("NULL");

        /* Find maximum candidate score */
        int indexMax = vSegmentWords.size() - 1;
        double maxscore = .0;
        /* Compose CN file */
        fprintf(cnTmpBody, "align %u", (numalign));
        for (unsigned i = 0; i < vSegmentWords.size(); i++) {
            if (vFinalScores[i] > maxscore) {
                indexMax = i;
                maxscore = vFinalScores[i];
            }
            if (vFinalScores[i] > 0.0000009) {
                fprintf(cnTmpBody, " %s %f", vSegmentWords[i].c_str(), vFinalScores[i]);
            }
        }
        fprintf(cnTmpBody, "\n");

        if (debug)
            cout << "<< MaxScore " << maxscore << " Index " << indexMax << " :: " << vSegmentWords[indexMax] << ">>";

        vHypothesis.push_back(vSegmentWords[indexMax]);
        vMaxScoresCN.push_back(vFinalScores[indexMax]);
        currentalign++;
        NullPost = 0;
        numalign++;
        //        }

    }

    /* Add CN Tail to cnTemporal file */
    fprintf(cnTmpBody, "align %u </s> 1", (numalign));
    cnTmpBody.close();
    /* Compose CN Header in final CN file */
    File cnFinal(outCNfilename, "w");
    fprintf(cnFinal, "numaligns %u\n", (numalign + 1));
    fprintf(cnFinal, "posterior 1\n");
    fprintf(cnFinal, "align 0 <s> 1\n");
    /* Add CSets to final CN file */
    ifstream cnFile(tmpCnFile);
    string cnline;
    while (getline(cnFile, cnline)) {
        if (!cnline.length() || cnline[0] == '#')
            continue;
        fprintf(cnFinal, "%s\n", cnline.c_str());
    }
    cnFinal.close();
    remove(tmpCnFile);

    //Hypothesis: NULL 0.38612 great 0.170823 speedy 0.249789 slid 0.240616 smooth 0.384809 planks 0.329313

    if (debug)
        cout << endl;

    /* Write PP of ref hypothesis */
    File refPP(outPPRef, "a"); /* OutputPP file */    
    float refhypPP = 0.0;
    cout << "-------------------------------------------------" << endl << "Ref Hyp: ";
    fprintf(refPP, "%s:", utterance);
    for (unsigned iRefword = 0; iRefword < vRefwords.size(); iRefword++) {
        cout << vRefwords[iRefword] << " " << vRefwordsScore[iRefword] << "  ";
        string CNrefword=vRefwords[iRefword];
        fprintf(refPP, "%s %f ", CNrefword.c_str(),vRefwordsScore[iRefword]);
        refhypPP += vRefwordsScore[iRefword];
    }
    cout << ":" << (refhypPP / vRefwords.size()); //AvgHypothesis PP    
    fprintf(refPP, ":%f\n", (refhypPP / vRefwords.size()) );

    /* Write hypothesis file */
    File MMCNPP(outPPMMCN, "a"); /* OutputPP file */    
    float MMCNhypPP = 0.0;
    File hypothout(outHypfilename, "w");
    cout << endl << "MMCNHyp: ";
    fprintf(MMCNPP, "%s:", utterance);
    fprintf(hypothout, "\"%s\"\n", utterance);
    for (unsigned i = 0; i < vHypothesis.size(); i++) {
        cout << vHypothesis[i] << " " << vMaxScoresCN[i] << " ";
        string CNbestword=vHypothesis[i];
        fprintf(MMCNPP, "%s %f ", CNbestword.c_str(), vMaxScoresCN[i]);
        string hypword = vHypothesis[i];
        if (!(hypword.compare("NULL") == 0)) { 
            fprintf(hypothout, "%s\n", hypword.c_str());
        }        
        MMCNhypPP += vMaxScoresCN[i];
    }
    fprintf(hypothout, ".\n");
    cout << ":" << (MMCNhypPP / vRefwords.size()); //AvgHypothesis PP
    fprintf(MMCNPP, ":%f\n", (MMCNhypPP / vRefwords.size()) );
    MMCNPP.close();

}



/* Segment Validation: Use previously identified boundaries
 * to delimitate temporal segments, and validate them.
 * Parameters: THRSNULL=0.46
 */
void validateSegments() {
    unsigned numalign = 1;
    Boolean isSearchContinuing = true;
    unsigned boundStart = 0, boundEnd = 0, currentalign = 1;
    float NullPost = 1;
    if (debug)
        cout << "Validating temporal segments [Boundaries: " << vBoundaries.size() << "]" << endl;

    /* =============================================== */
    /* Compose CN file */
    unsigned length = strlen(outCNfilename);
    char *copyCnFile = new char[length + 1]();
    strcpy(copyCnFile, outCNfilename);
    char *tmpCnFile = strcat(copyCnFile, "tmp");
    File cnTmpBody(tmpCnFile, "w");
    /* =============================================== */
    if (savebounds == 1) {
        File boundsFinal(outBoundfilename, "w");
        //fprintf(boundsFinal, "Valid segments\n");
        boundsFinal.close();
    }
    /* =============================================== */

    while (isSearchContinuing) {
        boundEnd = boundStart;
        NullPost = 1;
        vector<unsigned> vMicid;
        vector<double> vPosts;
        vector<string> vWords, vSegmentWords;
        vector<double> vFinalScores;

        while ((NullPost > THRSNULL) && (boundEnd < (vBoundaries.size() - 1))) {
            vFinalScores.clear();
            vSegmentWords.clear();
            vMicid.clear();
            vPosts.clear();
            vWords.clear();

            boundEnd = boundEnd + 1;
            float tS = vBoundaries[boundStart], tE = vBoundaries[boundEnd], DeltaT = 0.01;
            //For TypeDelta=3 (previous version)
            if ((tE - tS) < (deltaref)) {
                DeltaT = ((tE - tS) / 2);
            } else {
                DeltaT = ((tE - tS) * lratio);
            }

            float WLen = (tE + DeltaT) - max((float) 0.0, (tS - DeltaT));
            if (debug)
                cout << endl << "Checking boundaries " << tS << "," << tE << "(" << DeltaT << ") ";

            /* Start link selection */
            ifstream inputFile(tmpDataFile);
            string line;
            vector<float> vStartTime, vEndTime;

            /* Select links accomplishing the boundary constraints */
            while (getline(inputFile, line)) {
                if (!line.length() || line[0] == '#')
                    continue;
                unsigned numLatNode = 0, idMic = 0;
                char cWordlink[20];
                string sWordlink;
                float startTime = 0., endTime = 0.;
                double postProb = 0.;

                //sscanf(line.c_str(), "%u %19s %lf %lf %lf %u", &numLatNode, &cWordlink, &startTime, &endTime, &postProb, &idMic);
                sscanf(line.c_str(), "%u %19s %f %f %lf %u", &numLatNode, cWordlink, &startTime, &endTime, &postProb, &idMic);
                sWordlink = (string) cWordlink;

                /* Ommit <s> </s> in octave, here is equivalent to NULLs*/
                /* || (sWordlink.compare(str2) == 0 */
                if ((sWordlink.find("<s>") != std::string::npos) | (sWordlink.find("NULL") != std::string::npos)) {
                    //if ((sWordlink.find("<s>") != std::string::npos) ) {
                    /* do nothing if ENTRY/NULL symbols are found! */
                    //if (sWordlink.find("NULL")!= std::string::npos) {
                    /* do nothing if NULL symbols are found! */
                    //} else {
                } else {
                    Boolean startIsValid = false, endIsValid = false;
                    startIsValid = ((startTime >= (tS - DeltaT)) && (startTime <= (tS + DeltaT)));
                    endIsValid = ((endTime <= (tE + DeltaT)) && (endTime >= (tE - DeltaT)));
                    //                        if (tE>0.61 && tE<0.63 && (sWordlink.compare("lo")==0) && (idMic==2) ) {
                    //                            cout << "link [" << sWordlink << " " << startTime << "," << endTime << "  " << postProb << "] " << idMic << "("
                    //                                    << (startIsValid) << " "
                    //                                    << (endIsValid) << ")"
                    //                                    << endl ;
                    //                        }
                    if (startIsValid && endIsValid) {
                        vPosts.push_back(postProb);
                        vWords.push_back(sWordlink);
                        vSegmentWords.push_back(sWordlink);
                        vMicid.push_back(idMic);
                        //cout << "link [" << sWordlink << " " << startTime << "," << endTime << "  " << postProb << "] " << idMic << endl;
                    }
                    //}
                }
            }
            //cout << "***Valid links:" << vPosts.size() << endl;
            /* If links were found ... */
            if (vPosts.size() > 0) {
                if (debug)
                    cout << "  Found " << vPosts.size() << " links.." << endl;
                /* Get list of unique occurrence of words */
                std::sort(vSegmentWords.begin(), vSegmentWords.end());
                vSegmentWords.erase(std::unique(vSegmentWords.begin(), vSegmentWords.end()), vSegmentWords.end());

                /* ****************** DISPLAY ***************** */
                if (debug) {
                    cout << "   Unique words: ";
                    for (unsigned i = 0; i < vSegmentWords.size(); i++) {
                        cout << vSegmentWords[i] << " ";
                    }
                    cout << endl << "   ";
                }

                /* ======================================== */
                unsigned numWords = vSegmentWords.size();

                vector<vector<double> > mWordsMics;

                /* Per each word */
                for (unsigned w = 0; w < numWords; w++) {
                    vector<unsigned> vIndex;
                    string currentWord = vSegmentWords[w];
                    //cout << endl << currentWord << ": ";
                    //                    if (tE>0.61 && tE<0.63){
                    //                        cout << endl << currentWord << ": ";
                    //                    }
                    for (unsigned i = 0; i < vWords.size(); i++) {
                        if (vWords[i] == currentWord) {
                            vIndex.push_back(i);
                        }
                    }

                    /* Per each microphone */
                    vector<double> vMics;
                    for (unsigned mic = 0; mic < numMics; mic++) {
                        /* Get indexes of links of current Mic */
                        //cout << "<Mic" << mic << " ";

                        vector<unsigned> vIndexMic;
                        for (unsigned selectedlink = 0; selectedlink < vIndex.size(); selectedlink++) {
                            if (vMicid[vIndex[selectedlink]] == mic) {
                                vIndexMic.push_back(vIndex[selectedlink]);
                            }
                        }

                        /* IntraMic Score */
                        double Postscore = .0;
                        if (vIndexMic.size() > 0) {
                            for (unsigned i = 0; i < vIndexMic.size(); i++) {
                                Postscore = Postscore + vPosts[vIndexMic[i]];
                                //cout << vPosts[vIndexMic[i]] << " ";
                            }
                        } else {
                            //Postscore = Postscore + 0;
                            //cout << "P0 ";
                        }
                        //                        if (tE>0.61 && tE<0.63) {
                        //                            cout << Postscore << " ";
                        //                        }
                        vMics.push_back(Postscore);
                        //cout << "> ";
                    }
                    mWordsMics.push_back(vMics);
                }
                //cout << endl ;

                /* InterMic Score */
                vector<double> vNewPost;
                double sumvNewPost = .0, sumvNewPost2 = .0;
                for (unsigned wrow = 0; wrow < mWordsMics.size(); wrow++) {
                    double newPost = .0;
                    double maxscoreInterM = 0.0;
                    for (unsigned mic = 0; mic < numMics; mic++) {
                        newPost = newPost + mWordsMics[wrow][mic];
                        if (mWordsMics[wrow][mic] > maxscoreInterM) {
                            maxscoreInterM = mWordsMics[wrow][mic];
                        }
                    }
                    
                    double interMicWordScore = 0.0;
                    switch (scoremethod) {
                        case 1:
                            /* AVG InterMic */
                            interMicWordScore = (newPost / numMics);
                            break;
                        case 2:
                            /* MAX among all mics */
                            interMicWordScore = maxscoreInterM; //Use maxscore instead of AvgInterMic
                            break;
                    }
                
                    vNewPost.push_back(interMicWordScore);
                    sumvNewPost = sumvNewPost + (interMicWordScore);
                    //cout << interMicWordScore << " " ;
                }
                //cout << endl;
                if (debug) {
                    cout << "   Final Scores ";
                    cout << "[vNewPost size " << vNewPost.size() << "] ";
                    cout << " [alpha*sumvNewPost " << alpha * sumvNewPost << "] ";
                }
                if ((alpha * sumvNewPost) > 1) {
                    for (unsigned i = 0; i < vNewPost.size(); i++) {
                        vFinalScores.push_back(vNewPost[i] / (sumvNewPost));
                        sumvNewPost2 += (vNewPost[i] / (sumvNewPost));
                        if (debug)
                            cout << (vNewPost[i] / (sumvNewPost)) << " ";
                    }
                } else {
                    for (unsigned i = 0; i < vNewPost.size(); i++) {
                        vFinalScores.push_back(vNewPost[i]);
                        sumvNewPost2 += vNewPost[i];
                        //cout << vFinalScores[i] << " " ;
                        if (debug)
                            cout << "PPw " << (vNewPost[i]) << " ";
                    }
                }

                /* Criterion to continue/stop search of segment */
                NullPost = 1 - sumvNewPost2;

            } else {
                NullPost = 1;
                if (debug)
                    cout << "  Found no valid links" << endl;
            }
            
            if (debug) cout << "((NullP " << NullPost << "))";

        }
        
        if ((NullPost <= THRSNULL) & (vPosts.size() > 0)) {
            /* Add NULL word and score to stack of candidates */
            vFinalScores.push_back(NullPost);
            vSegmentWords.push_back("NULL");
            if (savebounds) {
                File boundsFinal(outBoundfilename, "a");
                fprintf(boundsFinal, "%f %f\n", vBoundaries[boundStart], vBoundaries[boundEnd]);
                boundsFinal.close();
            }
            if (debug) {
                cout << endl << "[Segm: " << vBoundaries[boundStart] << " " << vBoundaries[boundEnd] << "]" << endl;
                cout << "[FScore size " << vFinalScores.size() << "] ";
            }
            /* Mix Boundaries FwBw for a second search */
            vBoundariesFwBw.push_back(vBoundaries[boundStart]);
            vBoundariesFwBw.push_back(vBoundaries[boundEnd]);

            /* Find maximum candidate score */
            int indexMax = vSegmentWords.size() - 1;
            double maxscore = .0;
            /* =============================================== */
            /* Compose CN file */
            fprintf(cnTmpBody, "align %u", (numalign));
            /* =============================================== */
            for (unsigned i = 0; i < vSegmentWords.size(); i++) {
                if (vFinalScores[i] > maxscore) {
                    indexMax = i;
                    maxscore = vFinalScores[i];
                }
                /* =============================================== */
                /* Compose CN file */
                if (vFinalScores[i] > 0.0000009) {
                    fprintf(cnTmpBody, " %s %f", vSegmentWords[i].c_str(), vFinalScores[i]);
                }
                /* =============================================== */
            }
            fprintf(cnTmpBody, "\n");
            if (debug)
                cout << "<< MaxScore " << maxscore << " Index " << indexMax << " :: " << vSegmentWords[indexMax] << ">>";
            vHypothesis.push_back(vSegmentWords[indexMax]);
            currentalign++;
            NullPost = 0;
            boundStart = boundEnd; //Update next segment end-boundary
            numalign++;
        }

        if (boundEnd >= (vBoundaries.size() - 1)) {//Update next valid segment start-boundary
            boundStart++;
        }

        if (boundStart > (vBoundaries.size() - 2)) {//End search due to Boundary exceeded
            isSearchContinuing = false;
        }
    }

    /* =============================================== */
    /* Compose CN file */
    fprintf(cnTmpBody, "align %u </s> 1", (numalign));
    cnTmpBody.close();
    /*-------------------------------------*/
    File cnFinal(outCNfilename, "w");
    //fprintf(cnFinal, "name %s\n", utterance);
    fprintf(cnFinal, "numaligns %u\n", (numalign + 1));
    fprintf(cnFinal, "posterior 1\n");
    fprintf(cnFinal, "align 0 <s> 1\n");
    /*-------------------------------------*/
    ifstream cnFile(tmpCnFile);
    string cnline;
    while (getline(cnFile, cnline)) {
        if (!cnline.length() || cnline[0] == '#')
            continue;
        fprintf(cnFinal, "%s\n", cnline.c_str());
    }
    cnFinal.close();
    remove(tmpCnFile);
    /* =============================================== */

    if (debug)
        cout << endl;
    File hypothout(outHypfilename, "w");
    cout << endl << "Hypothesis: ";
    fprintf(hypothout, "\"%s\"\n", utterance);
    for (unsigned i = 0; i < vHypothesis.size(); i++) {
        cout << vHypothesis[i] << " ";
        string hypword = vHypothesis[i];
        fprintf(hypothout, "%s\n", hypword.c_str());
    }
    fprintf(hypothout, ".\n");



    /* if (writeMesh) {
            File file(writeMesh, "w");
            mesh.write(file);
        } */

}

/* Segment Validation Backward: Use previously identified boundaries
 * to delimitate temporal segments, and validate them starting from the.
 * final point to the first one.
 * Parameters: THRSNULL=0.46
 */
void validateSegmentsBw() {
    vHypothesis.clear();
    unsigned numalign = 1;
    Boolean isSearchContinuing = true;
    unsigned currentalign = 1;
    short boundStart = 0, boundEnd = 0;
    float NullPost = 1;
    if (debug)
        cout << "Validating temporal segments [Boundaries: " << vBoundaries.size() << "]" << endl;

    /* =============================================== */
    /* Compose CN file */
    unsigned length = strlen(outCNfilename2);
    char *copyCnFile = new char[length + 1]();
    strcpy(copyCnFile, outCNfilename2);
    char *tmpCnFile = strcat(copyCnFile, "tmp");
    File cnTmpBody(tmpCnFile, "w");
    /* =============================================== */
    if (savebounds == 1) {
        File boundsFinal(outBoundfilename, "w");
        boundsFinal.close();
    }
    /* =============================================== */

    boundStart = vBoundaries.size() - 1;
    while (isSearchContinuing) {
        boundEnd = boundStart;
        NullPost = 1;
        vector<unsigned> vMicid;
        vector<double> vPosts;
        vector<string> vWords, vSegmentWords;
        vector<double> vFinalScores;

        while ((NullPost > THRSNULL) && (boundStart > 0)) {
            vFinalScores.clear();
            vSegmentWords.clear();
            vMicid.clear();
            vPosts.clear();
            vWords.clear();

            boundStart = boundStart - 1; //Update segment
            float tS = vBoundaries[boundStart], tE = vBoundaries[boundEnd], DeltaT = 0.01;
            //For TypeDelta=3 (previous version)
            if ((tE - tS) < (deltaref)) {
                DeltaT = ((tE - tS) / 2);
            } else {
                DeltaT = ((tE - tS) * lratio);
            }

            float WLen = (tE + DeltaT) - max((float) 0.0, (tS - DeltaT));
            if (debug)
                cout << endl << "Checking boundaries " << tS << "," << tE << "(" << DeltaT << ") ";

            /* Start link selection */
            ifstream inputFile(tmpDataFile);
            string line;
            vector<float> vStartTime, vEndTime;

            /* Select links accomplishing the boundary constraints */
            while (getline(inputFile, line)) {
                if (!line.length() || line[0] == '#')
                    continue;
                unsigned numLatNode = 0, idMic = 0;
                char cWordlink[20];
                string sWordlink;
                float startTime = 0., endTime = 0.;
                double postProb = 0.;

                //sscanf(line.c_str(), "%u %19s %lf %lf %lf %u", &numLatNode, &cWordlink, &startTime, &endTime, &postProb, &idMic);
                sscanf(line.c_str(), "%u %19s %f %f %lf %u", &numLatNode, cWordlink, &startTime, &endTime, &postProb, &idMic);
                sWordlink = (string) cWordlink;

                /* Ommit <s> </s> in octave, here is equivalent to NULLs*/
                /* || (sWordlink.compare(str2) == 0 */
                if ((sWordlink.find("<s>") != std::string::npos) | (sWordlink.find("NULL") != std::string::npos)) {
                    //if ((sWordlink.find("<s>") != std::string::npos) ) {
                    /* do nothing if ENTRY/NULL symbols are found! */
                    //if (sWordlink.find("NULL")!= std::string::npos) {
                    /* do nothing if NULL symbols are found! */
                    //} else {
                } else {
                    Boolean startIsValid = false, endIsValid = false;
                    startIsValid = ((startTime >= (tS - DeltaT)) && (startTime <= (tS + DeltaT)));
                    endIsValid = ((endTime <= (tE + DeltaT)) && (endTime >= (tE - DeltaT)));
                    //                        if (tE>0.61 && tE<0.63 && (sWordlink.compare("lo")==0) && (idMic==2) ) {
                    //                            cout << "link [" << sWordlink << " " << startTime << "," << endTime << "  " << postProb << "] " << idMic << "("
                    //                                    << (startIsValid) << " "
                    //                                    << (endIsValid) << ")"
                    //                                    << endl ;
                    //                        }
                    if (startIsValid && endIsValid) {
                        vPosts.push_back(postProb);
                        vWords.push_back(sWordlink);
                        vSegmentWords.push_back(sWordlink);
                        vMicid.push_back(idMic);
                        //cout << "link [" << sWordlink << " " << startTime << "," << endTime << "  " << postProb << "] " << idMic << endl;
                    }
                    //}
                }
            }
            //cout << "***Valid links:" << vPosts.size() << endl;
            /* If links were found ... */
            if (vPosts.size() > 0) {
                if (debug)
                    cout << "  Found " << vPosts.size() << " links.." << endl;
                /* Get list of unique occurrence of words */
                std::sort(vSegmentWords.begin(), vSegmentWords.end());
                vSegmentWords.erase(std::unique(vSegmentWords.begin(), vSegmentWords.end()), vSegmentWords.end());

                /* ****************** DISPLAY ***************** */
                if (debug > 1) {
                    cout << "   Unique words: ";
                    for (unsigned i = 0; i < vSegmentWords.size(); i++) {
                        cout << vSegmentWords[i] << " ";
                    }
                    cout << endl << "   ";
                }

                /* ======================================== */
                unsigned numWords = vSegmentWords.size();

                vector<vector<double> > mWordsMics;

                /* Per each word */
                for (unsigned w = 0; w < numWords; w++) {
                    vector<unsigned> vIndex;
                    string currentWord = vSegmentWords[w];
                    //cout << endl << currentWord << ": ";
                    //                    if (tE>0.61 && tE<0.63){
                    //                        cout << endl << currentWord << ": ";
                    //                    }
                    for (unsigned i = 0; i < vWords.size(); i++) {
                        if (vWords[i] == currentWord) {
                            vIndex.push_back(i);
                        }
                    }

                    /* Per each microphone */
                    vector<double> vMics;
                    for (unsigned mic = 0; mic < numMics; mic++) {
                        /* Get indexes of links of current Mic */
                        //cout << "<Mic" << mic << " ";

                        vector<unsigned> vIndexMic;
                        for (unsigned selectedlink = 0; selectedlink < vIndex.size(); selectedlink++) {
                            if (vMicid[vIndex[selectedlink]] == mic) {
                                vIndexMic.push_back(vIndex[selectedlink]);
                            }
                        }

                        /* IntraMic Score */
                        double Postscore = .0;
                        if (vIndexMic.size() > 0) {
                            for (unsigned i = 0; i < vIndexMic.size(); i++) {
                                Postscore = Postscore + vPosts[vIndexMic[i]];
                                //cout << vPosts[vIndexMic[i]] << " ";
                            }
                        } else {
                            //Postscore = Postscore + 0;
                            //cout << "P0 ";
                        }
                        //                        if (tE>0.61 && tE<0.63) {
                        //                            cout << Postscore << " ";
                        //                        }
                        vMics.push_back(Postscore);
                        //cout << "> ";
                    }
                    mWordsMics.push_back(vMics);
                }
                //cout << endl ;

                /* InterMic Score */
                vector<double> vNewPost;
                double sumvNewPost = .0, sumvNewPost2 = .0;
                for (unsigned wrow = 0; wrow < mWordsMics.size(); wrow++) {
                    double newPost = .0;
                    for (unsigned mic = 0; mic < numMics; mic++) {
                        newPost = newPost + mWordsMics[wrow][mic];
                    }
                    double interMicWordScore = (newPost / numMics);
                    vNewPost.push_back(interMicWordScore);
                    sumvNewPost = sumvNewPost + (interMicWordScore);
                    //cout << interMicWordScore << " " ;
                }
                //cout << endl;
                if (debug > 1) {
                    cout << "   Final Scores ";
                    cout << "[vNewPost size " << vNewPost.size() << "] ";
                    cout << " [alpha*sumvNewPost " << alpha * sumvNewPost << "] ";
                    cout << endl;
                }
                if ((alpha * sumvNewPost) > 1) {
                    for (unsigned i = 0; i < vNewPost.size(); i++) {
                        vFinalScores.push_back(vNewPost[i] / (sumvNewPost));
                        sumvNewPost2 += (vNewPost[i] / (sumvNewPost));
                        if (debug > 1)
                            cout << (vNewPost[i] / (sumvNewPost)) << " ";
                    }
                } else {
                    for (unsigned i = 0; i < vNewPost.size(); i++) {
                        vFinalScores.push_back(vNewPost[i]);
                        sumvNewPost2 += vNewPost[i];
                        //cout << vFinalScores[i] << " " ;
                        if (debug > 1)
                            cout << "PPw " << (vNewPost[i]) << " ";
                    }
                }

                /* Criterion to continue/stop search of segment */
                NullPost = 1 - sumvNewPost2;

            } else {
                NullPost = 1;
                if (debug)
                    cout << "  Found no valid links" << endl;
            }

        }
        if ((NullPost <= THRSNULL) & (vPosts.size() > 0)) {
            /* Add NULL word and score to stack of candidates */
            vFinalScores.push_back(NullPost);
            vSegmentWords.push_back("NULL");
            if (savebounds) {
                File boundsFinal(outBoundfilename, "a");
                fprintf(boundsFinal, "%f %f\n", vBoundaries[boundStart], vBoundaries[boundEnd]);
                boundsFinal.close();
            }
            if (debug) {
                //cout << endl;
                cout << "[New CN Segm: " << vBoundaries[boundStart] << " " << vBoundaries[boundEnd] << "]" << endl;
                cout << "[FScore size " << vFinalScores.size() << "] ";
            }
            /* Mix Boundaries FwBw for a second search */
            vBoundariesFwBw.push_back(vBoundaries[boundStart]);
            vBoundariesFwBw.push_back(vBoundaries[boundEnd]);

            /* Find maximum candidate score */
            int indexMax = vSegmentWords.size() - 1;
            double maxscore = .0;
            /* =============================================== */
            /* Compose CN file */
            //fprintf(cnTmpBody, "align %u", (numalign));
            fprintf(cnTmpBody, "alignX");
            /* =============================================== */
            for (unsigned i = 0; i < vSegmentWords.size(); i++) {
                if (vFinalScores[i] > maxscore) {
                    indexMax = i;
                    maxscore = vFinalScores[i];
                }
                /* =============================================== */
                /* Compose CN file */
                if (vFinalScores[i] > 0.0000009) {
                    fprintf(cnTmpBody, " %s %f", vSegmentWords[i].c_str(), vFinalScores[i]);
                }
                /* =============================================== */
            }
            fprintf(cnTmpBody, "\n");
            if (debug)
                cout << "<< MaxScore " << maxscore << " Index " << indexMax << " :: " << vSegmentWords[indexMax] << ">>";
            vHypothesis.push_back(vSegmentWords[indexMax]);
            currentalign++;
            NullPost = 0;
            boundEnd = boundStart; //Update next segment end-boundary
            numalign++;
            //cout << "( Updated " << boundEnd;
            
        }

        if (boundStart <= 0) {//if (boundEnd >= 0) { //Update next valid segment start-boundary
            boundEnd--;
        }

        if (boundEnd < 1) { //End search due to Boundary exceeded
            isSearchContinuing = false;
        }

    }

    /* =============================================== */
    /* Compose CN file */
    fprintf(cnTmpBody, "align %u </s> 1", (numalign));
    cnTmpBody.close();
    /*-------------------------------------*/
    File cnFinal(outCNfilename2, "w");
    //fprintf(cnFinal, "name %s\n", utterance);
    fprintf(cnFinal, "numaligns %u\n", (numalign + 1));
    fprintf(cnFinal, "posterior 1\n");
    fprintf(cnFinal, "align 0 <s> 1\n");
    /*-------------------------------------*/
    /* Reverse CNtmp file which was written backwards*/
    ifstream inreverse(tmpCnFile);
    short numalignbw = numalign - 1;
    vector<string> lines_in_order;
    if (inreverse.is_open()) {
        string alignline;
        cout << endl;
        while (getline(inreverse, alignline)) {
            string reversealign;
            stringstream ss;
            ss << "align " << numalignbw << " ";
            reversealign = ss.str(); //align #
            if (numalignbw > 0) {
                alignline.replace(alignline.find("alignX"), sizeof (reversealign) - 1, reversealign);
                // Store the lines in reverse order than read file.
                lines_in_order.insert(lines_in_order.begin(), alignline);
            }
            numalignbw--;
        }
    }
    inreverse.close();

    /* Print to CN file */
    for (int j = 0; j < lines_in_order.size(); j++) {
        fprintf(cnFinal, "%s\n", lines_in_order[j].c_str());
    }
    fprintf(cnFinal, "align %u </s> 1", (numalign));
    /* ------------------------------------*/
    cnFinal.close();
    remove(tmpCnFile);
    /* =============================================== */

    if (debug)
        cout << endl;
    File hypothout(outHypfilename, "w");
    cout << endl << "HypothesisBW: ";
    fprintf(hypothout, "\"%s\"\n", utterance);
    for (int i = vHypothesis.size(); i-- > 0;) {
        //for (unsigned i = 0; i < vHypothesis.size(); i++) {
        cout << vHypothesis[i] << " ";
        string hypword = vHypothesis[i];
        fprintf(hypothout, "%s\n", hypword.c_str());
    }
    fprintf(hypothout, ".\n");

    /* if (writeMesh) {
            File file(writeMesh, "w");
            mesh.write(file);
        } */

}

void validateSegmentsFrame() {
    Boolean isSearchContinuing = true;
    unsigned boundStart = 0, boundEnd = 0, currentalign = 1;
    float NullPost = 1;
    if (debug)
        cout << "Validating temporal segments [Boundaries: " << vBoundaries.size() << "]" << endl;

    while (isSearchContinuing) {
        boundEnd = boundStart;
        NullPost = 1;
        vector<unsigned> vMicid;
        vector<double> vPosts;
        vector<string> vWords, vSegmentWords;
        vector<double> vFinalScores;
        vector<float> vTimeStart, vTimeEnd;

        while ((NullPost > THRSNULL) && (boundEnd < (vBoundaries.size() - 1))) {
            vFinalScores.clear();
            vSegmentWords.clear();
            vMicid.clear();
            vPosts.clear();
            vWords.clear();
            vTimeStart.clear();
            vTimeEnd.clear();

            /* Update ending boundary of study segment */
            boundEnd = boundEnd + 1;
            float tS = vBoundaries[boundStart], tE = vBoundaries[boundEnd], DeltaT = 0.01;
            //For TypeDelta=3 (previous version)
            if ((tE - tS) < (deltaref)) {
                DeltaT = ((tE - tS) / 2);
            } else {
                DeltaT = ((tE - tS) * lratio);
            }

            float WLen = (tE + DeltaT) - max((float) 0.0, (tS - DeltaT));
            if (debug)
                cout << endl << "Checking boundaries " << tS << "," << tE << "(" << DeltaT << ") ";

            /* Start link selection */
            ifstream inputFile(tmpDataFile);
            string line;
            vector<float> vStartTime, vEndTime;

            /* Select links accomplishing the boundary constraints */
            while (getline(inputFile, line)) {
                if (!line.length() || line[0] == '#')
                    continue;
                unsigned numLatNode = 0, idMic = 0;
                char cWordlink[20];
                string sWordlink;
                float startTime = 0., endTime = 0.;
                double postProb = 0.;

                sscanf(line.c_str(), "%u %19s %f %f %lf %u", &numLatNode, cWordlink, &startTime, &endTime, &postProb, &idMic);
                sWordlink = (string) cWordlink;

                /* Ommit <s> </s> in octave, here is equivalent to NULLs*/
                /* || (sWordlink.compare(str2) == 0 */
                if ((sWordlink.find("<s>") != std::string::npos) | (sWordlink.find("NULL") != std::string::npos)) {
                    //if ((sWordlink.find("<s>") != std::string::npos) ) {
                    /* do nothing if ENTRY/NULL symbols are found! */
                    //if (sWordlink.find("NULL")!= std::string::npos) {
                    /* do nothing if NULL symbols are found! */
                    //} else {
                } else {
                    Boolean startIsValid = false, endIsValid = false;
                    startIsValid = ((startTime >= (tS - DeltaT)) && (startTime <= (tS + DeltaT)));
                    endIsValid = ((endTime <= (tE + DeltaT)) && (endTime >= (tE - DeltaT)));
                    //                        if (tE>0.61 && tE<0.63 && (sWordlink.compare("lo")==0) && (idMic==2) ) {
                    //                            cout << "link [" << sWordlink << " " << startTime << "," << endTime << "  " << postProb << "] " << idMic << "("
                    //                                    << (startIsValid) << " "
                    //                                    << (endIsValid) << ")"
                    //                                    << endl ;
                    //                        }
                    if (startIsValid && endIsValid) {
                        vPosts.push_back(postProb);
                        vWords.push_back(sWordlink);
                        vSegmentWords.push_back(sWordlink);
                        vMicid.push_back(idMic);
                        vTimeStart.push_back(startTime);
                        vTimeEnd.push_back(endTime);
                        //cout << "link [" << sWordlink << " " << startTime << "," << endTime << "  " << postProb << "] " << idMic << endl;
                    }
                    //}
                }
            }
            //cout << "***Valid links:" << vPosts.size() << endl;
            /* If links were found ... */
            if (vPosts.size() > 0) {
                if (debug)
                    cout << "  Found " << vPosts.size() << " links.." << endl;
                /* Get list of unique occurrence of words */
                std::sort(vSegmentWords.begin(), vSegmentWords.end());
                vSegmentWords.erase(std::unique(vSegmentWords.begin(), vSegmentWords.end()), vSegmentWords.end());

                /* ****************** DISPLAY ***************** */
                if (debug) {
                    cout << "   Unique words: ";
                    for (unsigned i = 0; i < vSegmentWords.size(); i++) {
                        cout << vSegmentWords[i] << " ";
                    }
                    cout << endl << "   ";
                }

                /* ======================================== */
                unsigned numWords = vSegmentWords.size();

                vector<vector<double> > mWordsMics;

                /* Per each word */
                for (unsigned w = 0; w < numWords; w++) {
                    vector<unsigned> vIndex;
                    string currentWord = vSegmentWords[w];
                    //cout << endl << currentWord << ": ";
                    //                    if (tE>0.61 && tE<0.63){
                    //                        cout << endl << currentWord << ": ";
                    //                    }
                    for (unsigned i = 0; i < vWords.size(); i++) {
                        if (vWords[i] == currentWord) {
                            vIndex.push_back(i);
                        }
                    }

                    /* Per each microphone */
                    vector<double> vMics;
                    for (unsigned mic = 0; mic < numMics; mic++) {
                        /* Get indexes of links of current Mic */
                        //cout << "<Mic" << mic << " ";

                        vector<unsigned> vIndexMic;
                        for (unsigned selectedlink = 0; selectedlink < vIndex.size(); selectedlink++) {
                            if (vMicid[vIndex[selectedlink]] == mic) {
                                vIndexMic.push_back(vIndex[selectedlink]);
                            }
                        }

                        /* IntraMic Score */
                        double Postscore = .0;
                        if (vIndexMic.size() > 0) {
                            /* *********************************** */
                            /* Step 1) Get sub-boundaries and list unique */
                            vector<float> vSubboundaries;
                            for (unsigned i = 0; i < vIndexMic.size(); i++) {
                                vSubboundaries.push_back(vTimeStart[vIndexMic[i]]);
                                vSubboundaries.push_back(vTimeEnd[vIndexMic[i]]);
                            }
                            std::sort(vSubboundaries.begin(), vSubboundaries.end());
                            vSubboundaries.erase(std::unique(vSubboundaries.begin(), vSubboundaries.end()), vSubboundaries.end());

                            /* Weight SubSegments */
                            vector<float> vWeightSubsegmnt;
                            for (unsigned subSegmnt = 0; subSegmnt < (vSubboundaries.size() - 1); subSegmnt++) {
                                double lSubSeg = (vSubboundaries[subSegmnt + 1] - vSubboundaries[subSegmnt]);
                                double weightSubsegmnt = lSubSeg / (tE - tS);
                                vWeightSubsegmnt.push_back(weightSubsegmnt);
                            }
                            if (debug == 1) {
                                cout << "[M" << mic << "]" << "Subbounds (" << vSubboundaries.size() << ") ";
                                for (unsigned subSegmnt = 0; subSegmnt < vSubboundaries.size(); subSegmnt++) {
                                    cout << vSubboundaries[subSegmnt] << "(w" << vWeightSubsegmnt[subSegmnt] << ") ";
                                }
                            }


                            /* Step 2) For each pair of sub-bounds, accumulate scores */
                            vector<double> vSubsegmentScore;
                            for (unsigned subSegmnt = 0; subSegmnt < vSubboundaries.size(); subSegmnt++) {
                                vSubsegmentScore.push_back(0.00);
                            }
                            for (unsigned i = 0; i < vIndexMic.size(); i++) {
                                double lStart, lEnd;
                                lStart = vTimeStart[vIndexMic[i]];
                                lEnd = vTimeEnd[vIndexMic[i]];
                                for (unsigned subSegmnt = 0; subSegmnt < (vSubboundaries.size() - 1); subSegmnt++) {
                                    if (lStart <= vSubboundaries[subSegmnt] & lEnd >= vSubboundaries[subSegmnt + 1]) {
                                        double oldscore = vWeightSubsegmnt[subSegmnt]*(vSubsegmentScore[subSegmnt]);
                                        vSubsegmentScore.at(subSegmnt) = (oldscore + vPosts[vIndexMic[i]]);
                                    }
                                }
                            }
                            /* Step 3) Get max frame-level sum */
                            if (debug == 1) {
                                cout << "[";
                                for (unsigned subSegmnt = 0; subSegmnt < vSubsegmentScore.size(); subSegmnt++) {
                                    cout << vSubsegmentScore[subSegmnt] << " ";
                                }
                                cout << "]";
                            }
                            vector<double>::iterator result = max_element(vSubsegmentScore.begin(), vSubsegmentScore.end());
                            int maxIndex = std::distance(vSubsegmentScore.begin(), result);
                            Postscore = vSubsegmentScore[maxIndex];
                            if (debug == 1) {
                                cout << "FramePP " << Postscore << " ";
                            }
                        }
                        //                        if (tE>0.61 && tE<0.63) {
                        //                            cout << Postscore << " ";
                        //                        }
                        vMics.push_back(Postscore);
                        //cout << "> ";
                    }
                    mWordsMics.push_back(vMics);
                }
                //cout << endl ;

                /* ================= InterMic Score ================= */
                vector<double> vNewPost;
                double sumvNewPost = .0, sumvNewPost2 = .0;
                for (unsigned wrow = 0; wrow < mWordsMics.size(); wrow++) {
                    double newPost = .0;
                    for (unsigned mic = 0; mic < numMics; mic++) {
                        newPost = newPost + mWordsMics[wrow][mic];
                    }
                    double interMicWordScore = (newPost / numMics);
                    vNewPost.push_back(interMicWordScore);
                    sumvNewPost = sumvNewPost + (interMicWordScore);
                    //cout << interMicWordScore << " " ;
                }
                //cout << endl;
                if (debug) {
                    cout << "   Final Scores ";
                    cout << "[vNewPost size " << vNewPost.size() << "] ";
                    cout << " [alpha*sumvNewPost " << alpha * sumvNewPost << "] ";
                }
                if ((alpha * sumvNewPost) > 1) {
                    for (unsigned i = 0; i < vNewPost.size(); i++) {
                        vFinalScores.push_back(vNewPost[i] / (sumvNewPost));
                        sumvNewPost2 += (vNewPost[i] / (sumvNewPost));
                        if (debug)
                            cout << (vNewPost[i] / (sumvNewPost)) << " ";
                    }
                } else {
                    for (unsigned i = 0; i < vNewPost.size(); i++) {
                        vFinalScores.push_back(vNewPost[i]);
                        sumvNewPost2 += vNewPost[i];
                        //cout << vFinalScores[i] << " " ;
                        if (debug)
                            cout << (vNewPost[i]) << " ";
                    }
                }

                /* Criterion to continue/stop search of segment */
                NullPost = 1 - sumvNewPost2;

            } else {
                NullPost = 1;
                if (debug)
                    cout << "  Found no valid links" << endl;
            }

        }
        if ((NullPost <= THRSNULL) & (vPosts.size() > 0)) {
            /* Add NULL word and score to stack of candidates */
            vFinalScores.push_back(NullPost);
            vSegmentWords.push_back("NULL");
            if (debug)
                cout << "[FScore size " << vFinalScores.size() << "] ";

            /* Find maximum candidate score */
            int indexMax = vSegmentWords.size() - 1;
            double maxscore = .0;
            for (unsigned i = 0; i < vSegmentWords.size(); i++) {
                if (vFinalScores[i] > maxscore) {
                    indexMax = i;
                    maxscore = vFinalScores[i];
                }
            }
            if (debug)
                cout << "<< MaxScore " << maxscore << " Index " << indexMax << " :: " << vSegmentWords[indexMax] << ">>";
            vHypothesis.push_back(vSegmentWords[indexMax]);
            currentalign++;
            NullPost = 0;
            boundStart = boundEnd;
        }

        if (boundEnd >= (vBoundaries.size() - 1)) {
            boundStart++;
        }

        if (boundStart > (vBoundaries.size() - 2)) {
            isSearchContinuing = false;
        }
    }

    File hypothout(outHypfilename, "w");
    if (debug)
        cout << endl;

    cout << "Hypothesis: ";
    fprintf(hypothout, "\"%s\"\n", utterance);
    for (unsigned i = 0; i < vHypothesis.size(); i++) {
        cout << vHypothesis[i] << " ";
        string hypword = vHypothesis[i];
        fprintf(hypothout, "%s\n", hypword.c_str());
    }
    fprintf(hypothout, ".\n");

    cout << endl;
    cout << "----------------------------------------------------------" << endl;
    cout << "min-posterior " << minPosteriorProb << " alpha " << alpha
            << " lratio " << lratio << " deltaref " << deltaref << endl;
    cout << "----------------------------------------------------------" << endl;

}

/*
 * latticeSelection: Use previously identified boundaries
 * to delimitate temporal segments, and validate them.
 * Return the lattice which achieves more likely max-score
 * per segment.
 *
 * Parameters: THRSNULL=0.46
 * vs2: return one mic by ChannelSelection
 */
void validateSegmentsV2() {
    Boolean isSearchContinuing = true;
    unsigned boundStart = 0, boundEnd = 0, currentalign = 1;
    unsigned idsMicMaxScore[numMics];
    for (int i = 0; i < numMics; i++) {
        idsMicMaxScore[i] = 0;
    }
    float NullPost = 1;
    if (debug)
        cout << "Validating temporal segments [Boundaries: " << vBoundaries.size() << "]" << endl;

    while (isSearchContinuing) {
        boundEnd = boundStart;
        NullPost = 1;
        vector<unsigned> vMicid;
        vector<double> vPosts;
        vector<string> vWords, vSegmentWords;
        vector<double> vFinalScores;
        vector<unsigned> vMicMaxprobPerWord;

        while ((NullPost > THRSNULL) && (boundEnd < (vBoundaries.size() - 1))) {
            vMicMaxprobPerWord.clear();
            vFinalScores.clear();
            vSegmentWords.clear();
            vMicid.clear();
            vPosts.clear();
            vWords.clear();

            boundEnd = boundEnd + 1;
            float tS = vBoundaries[boundStart], tE = vBoundaries[boundEnd], DeltaT = 0.01;
            //For TypeDelta=3 (previous version)
            if ((tE - tS) < (deltaref)) {
                DeltaT = ((tE - tS) / 2);
            } else {
                DeltaT = ((tE - tS) * lratio);
            }

            float WLen = (tE + DeltaT) - max((float) 0.0, (tS - DeltaT));
            if (debug)
                cout << endl << "Checking boundaries " << tS << "," << tE << "(" << DeltaT << ") ";

            /* Select paths that start-end in allowed boundary regions */
            ifstream inputFile(tmpDataFile);
            string line;
            vector<float> vStartTime, vEndTime;

            /* Select links accomplishing the boundary constraints */
            while (getline(inputFile, line)) {
                if (!line.length() || line[0] == '#')
                    continue;
                unsigned numLatNode = 0, idMic = 0;
                char cWordlink[20];
                string sWordlink;
                float startTime = 0., endTime = 0.;
                double postProb = 0.;

                //sscanf(line.c_str(), "%u %19s %lf %lf %lf %u", &numLatNode, &cWordlink, &startTime, &endTime, &postProb, &idMic);
                sscanf(line.c_str(), "%u %19s %f %f %lf %u", &numLatNode, cWordlink, &startTime, &endTime, &postProb, &idMic);
                sWordlink = (string) cWordlink;

                /* Ommit <s> </s> in octave, here is equivalent to NULLs*/
                /* || (sWordlink.compare(str2) == 0 */
                if ((sWordlink.find("<s>") != std::string::npos) | (sWordlink.find("NULL") != std::string::npos)) {
                    //if ((sWordlink.find("<s>") != std::string::npos) ) {
                    /* do nothing if ENTRY/NULL symbols are found! */
                    //if (sWordlink.find("NULL")!= std::string::npos) {
                    /* do nothing if NULL symbols are found! */
                    //} else {
                } else {
                    Boolean startIsValid = false, endIsValid = false;
                    startIsValid = ((startTime >= (tS - DeltaT)) && (startTime <= (tS + DeltaT)));
                    endIsValid = ((endTime <= (tE + DeltaT)) && (endTime >= (tE - DeltaT)));
                    //                        if (tE>0.61 && tE<0.63 && (sWordlink.compare("lo")==0) && (idMic==2) ) {
                    //                            cout << "link [" << sWordlink << " " << startTime << "," << endTime << "  " << postProb << "] " << idMic << "("
                    //                                    << (startIsValid) << " "
                    //                                    << (endIsValid) << ")"
                    //                                    << endl ;
                    //                        }
                    if (startIsValid && endIsValid) {
                        vPosts.push_back(postProb);
                        vWords.push_back(sWordlink);
                        vSegmentWords.push_back(sWordlink);
                        vMicid.push_back(idMic);
                        //cout << "link [" << sWordlink << " " << startTime << "," << endTime << "  " << postProb << "] " << idMic << endl;
                    }
                    //}
                }
            }
            //cout << "***Valid links:" << vPosts.size() << endl;
            /* If links were found ... */
            if (vPosts.size() > 0) {
                if (debug)
                    cout << "  Found " << vPosts.size() << " links.." << endl;
                /* Get list of unique occurrence of words */
                std::sort(vSegmentWords.begin(), vSegmentWords.end());
                vSegmentWords.erase(std::unique(vSegmentWords.begin(), vSegmentWords.end()), vSegmentWords.end());

                /* ****************** DISPLAY ***************** */
                if (debug) {
                    cout << "   Unique words: ";
                    for (unsigned i = 0; i < vSegmentWords.size(); i++) {
                        cout << vSegmentWords[i] << " ";
                    }
                    cout << endl << "   ";
                }

                /* ======================================== */
                unsigned numWords = vSegmentWords.size();

                vector<vector<double> > mWordsMics;

                /* Per each word */
                for (unsigned w = 0; w < numWords; w++) {
                    vector<unsigned> vIndex;
                    string currentWord = vSegmentWords[w];
                    //cout << endl << currentWord << ": ";
                    //                    if (tE>0.61 && tE<0.63){
                    //                        cout << endl << currentWord << ": ";
                    //                    }
                    for (unsigned i = 0; i < vWords.size(); i++) {
                        if (vWords[i] == currentWord) {
                            vIndex.push_back(i);
                        }
                    }

                    /* Per each microphone */
                    vector<double> vMics;
                    for (unsigned mic = 0; mic < numMics; mic++) {
                        /* Get indexes of links of current Mic */
                        //cout << "<Mic" << mic << " ";

                        vector<unsigned> vIndexMic;
                        for (unsigned selectedlink = 0; selectedlink < vIndex.size(); selectedlink++) {
                            if (vMicid[vIndex[selectedlink]] == mic) {
                                vIndexMic.push_back(vIndex[selectedlink]);
                            }
                        }

                        /* IntraMic Score */
                        double Postscore = .0;
                        if (vIndexMic.size() > 0) {
                            for (unsigned i = 0; i < vIndexMic.size(); i++) {
                                Postscore = Postscore + vPosts[vIndexMic[i]];
                                //cout << vPosts[vIndexMic[i]] << " ";
                            }
                        } else {
                            //Postscore = Postscore + 0;
                            //cout << "P0 ";
                        }
                        //                        if (tE>0.61 && tE<0.63) {
                        //                            cout << Postscore << " ";
                        //                        }
                        vMics.push_back(Postscore);
                        //cout << "> ";
                    }
                    mWordsMics.push_back(vMics);
                }
                //cout << endl ;

                /* InterMic Score */
                vector<double> vNewPost;
                double sumvNewPost = .0, sumvNewPost2 = .0;
                unsigned idMicMaxScore;
                for (unsigned wrow = 0; wrow < mWordsMics.size(); wrow++) {
                    idMicMaxScore = 0;
                    double micMaxScore = -1;
                    double newPost = .0;
                    for (unsigned mic = 0; mic < numMics; mic++) {
                        newPost = newPost + mWordsMics[wrow][mic];
                        //Update MicId with maxscore per word
                        //                         if (tE>0.46 & tE<0.48){
                        //                             Boolean boolmax=(mWordsMics[wrow][mic]>micMaxScore);
                        //                             cout << "maxscore " << micMaxScore << " micScore " << mWordsMics[wrow][mic] << "(" << boolmax << ")" << " mic" << mic << endl;
                        //                         }
                        if (mWordsMics[wrow][mic] > micMaxScore) {
                            micMaxScore = newPost;
                            idMicMaxScore = mic;
                        }
                    }
                    //                    if (tE>0.46 & tE<0.48){
                    //                        cout << "addmic " << idMicMaxScore << endl;
                    //                    }
                    vMicMaxprobPerWord.push_back(idMicMaxScore);

                    double interMicWordScore = (newPost / numMics);
                    vNewPost.push_back(interMicWordScore);
                    sumvNewPost = sumvNewPost + (interMicWordScore);
                    //cout << interMicWordScore << " " ;
                }
                //cout << endl;
                if (debug) {
                    cout << "   Final Scores ";
                    cout << "[vNewPost size " << vNewPost.size() << "] ";
                    cout << " [alpha*sumvNewPost " << alpha * sumvNewPost << "] ";
                }
                /* Normalize scores */
                if ((alpha * sumvNewPost) > 1) {
                    for (unsigned i = 0; i < vNewPost.size(); i++) {
                        vFinalScores.push_back(vNewPost[i] / (sumvNewPost));
                        sumvNewPost2 += (vNewPost[i] / (sumvNewPost));
                        if (debug)
                            cout << (vNewPost[i] / (sumvNewPost)) << " ";
                    }
                } else {
                    for (unsigned i = 0; i < vNewPost.size(); i++) {
                        vFinalScores.push_back(vNewPost[i]);
                        sumvNewPost2 += vNewPost[i];
                        //cout << vFinalScores[i] << " " ;
                        if (debug)
                            cout << (vNewPost[i]) << " ";
                    }
                }

                /* Criterion to continue/stop search of segment */
                NullPost = 1 - sumvNewPost2;

            } else {
                NullPost = 1;
                if (debug)
                    cout << "  Found no valid links" << endl;
            }

        }
        /* Update structure for hypothesis, and boundaries */
        if ((NullPost <= THRSNULL) & (vPosts.size() > 0)) {
            /* Add NULL word and score to stack of candidates */
            vFinalScores.push_back(NullPost);
            vSegmentWords.push_back("NULL");
            if (debug)
                cout << "[FScore size " << vFinalScores.size() << "] ";

            /* Find maximum candidate score */
            int indexMax = vSegmentWords.size() - 1;
            double maxscore = .0;
            for (unsigned i = 0; i < vSegmentWords.size(); i++) {
                if (vFinalScores[i] > maxscore) {
                    indexMax = i;
                    maxscore = vFinalScores[i];
                }
            }
            if (debug)
                cout << "<< MaxScore " << maxscore << " Index " << indexMax << " :: " << vSegmentWords[indexMax] << ">>";
            vHypothesis.push_back(vSegmentWords[indexMax]);

            unsigned indexMicUpdate = vMicMaxprobPerWord[indexMax];
            idsMicMaxScore[indexMicUpdate] = idsMicMaxScore[indexMicUpdate] + 1;

            currentalign++;
            NullPost = 0;
            boundStart = boundEnd;
        }

        if (boundEnd >= (vBoundaries.size() - 1)) {
            boundStart++;
        }

        if (boundStart > (vBoundaries.size() - 2)) {
            isSearchContinuing = false;
        }
    }

    File cnout(outCNfilename, "w");
    if (debug)
        cout << endl;

    cout << endl << "MaxPerChannel: ";
    unsigned maxchannel = 0;
    unsigned maxChannelCount = 0;
    for (unsigned indexMic = 0; indexMic < numMics; indexMic++) {
        cout << idsMicMaxScore[indexMic] << " ";
        if (idsMicMaxScore[indexMic] > maxChannelCount) {
            maxChannelCount = idsMicMaxScore[indexMic];
            maxchannel = indexMic;
        }
    }
    cout << endl << "SelectedChannel0..M:";
    cout << maxchannel << endl;

    cout << "Hypothesis: ";
    fprintf(cnout, "\"%s\"\n", utterance);
    for (unsigned i = 0; i < vHypothesis.size(); i++) {
        cout << vHypothesis[i] << " ";
        string hypword = vHypothesis[i];
        fprintf(cnout, "%s\n", hypword.c_str());
    }
    fprintf(cnout, ".\n");

    cout << endl;
    cout << "----------------------------------------------------------" << endl;
    cout << "min-posterior " << minPosteriorProb << " alpha " << alpha
            << " lratio " << lratio << " deltaref " << deltaref << endl;
    cout << "----------------------------------------------------------" << endl;

}

/* Segment Validation: Use previously identified boundaries
 * to delimitate temporal segments, and validate them.
 * Parameters: THRSNULL=0.46
 */
void validateSegmentsV3() {
    Boolean isSearchContinuing = true;
    unsigned boundStart = 0, boundEnd = 0, currentalign = 1;
    float NullPost = 1;
    if (debug)
        cout << "Validating temporal segments [Boundaries: " << vBoundaries.size() << "]" << endl;

    while (isSearchContinuing) {
        boundEnd = boundStart;
        NullPost = THRSNULL; /* +1 for eval different thnull values */
        vector<unsigned> vMicid;
        vector<double> vPosts;
        vector<string> vWords, vSegmentWords;
        vector<double> vFinalScores;
        vector<float> vStartT, vEndT;

        while ((NullPost > THRSNULL) && (boundEnd < (vBoundaries.size() - 1))) {
            vFinalScores.clear();
            vSegmentWords.clear();
            vMicid.clear();
            vPosts.clear();
            vWords.clear();
            vStartT.clear();
            vEndT.clear();

            boundEnd = boundEnd + 1;
            float tS = vBoundaries[boundStart], tE = vBoundaries[boundEnd], DeltaT = 0.01;
            //For TypeDelta=3 (previous version)
            //            if ((tE - tS) < (deltaref)) {
            //                DeltaT = ((tE - tS) / 2);
            //            } else {
            DeltaT = ((tE - tS) * lratio);
            //            }

            float WLen = (tE + DeltaT) - max((float) 0.0, (tS - DeltaT));
            if (debug)
                cout << endl << "Checking boundaries " << tS << "," << tE << "(" << DeltaT << ") ";

            /* Start link selection */
            ifstream inputFile(tmpDataFile);
            string line;
            vector<float> vStartTime, vEndTime;

            /* Select links accomplishing the boundary constraints */
            while (getline(inputFile, line)) {
                if (!line.length() || line[0] == '#')
                    continue;
                unsigned numLatNode = 0, idMic = 0;
                char cWordlink[20];
                string sWordlink;
                float startTime = 0., endTime = 0.;
                double postProb = 0.;

                sscanf(line.c_str(), "%u %19s %f %f %lf %u", &numLatNode, cWordlink, &startTime, &endTime, &postProb, &idMic);
                sWordlink = (string) cWordlink;

                /* Ommit <s> </s> in octave, here is equivalent to NULLs*/
                /* || (sWordlink.compare(str2) == 0 */
                if ((sWordlink.find("<s>") != std::string::npos) | (sWordlink.find("NULL") != std::string::npos)) {
                    //if ((sWordlink.find("<s>") != std::string::npos) ) {
                    /* do nothing if ENTRY/NULL symbols are found! */
                    //if (sWordlink.find("NULL")!= std::string::npos) {
                    /* do nothing if NULL symbols are found! */
                    //} else {
                } else {
                    Boolean startIsValid = false, endIsValid = false;
                    startIsValid = ((startTime >= (tS - DeltaT)) && (startTime <= (tS + DeltaT)));
                    endIsValid = ((endTime <= (tE + DeltaT)) && (endTime >= (tE - DeltaT)));
                    //                        if (tE>0.61 && tE<0.63 && (sWordlink.compare("lo")==0) && (idMic==2) ) {
                    //                            cout << "link [" << sWordlink << " " << startTime << "," << endTime << "  " << postProb << "] " << idMic << "("
                    //                                    << (startIsValid) << " "
                    //                                    << (endIsValid) << ")"
                    //                                    << endl ;
                    //                        }
                    if (startIsValid && endIsValid) {
                        vPosts.push_back(postProb);
                        vWords.push_back(sWordlink);
                        vSegmentWords.push_back(sWordlink);
                        vMicid.push_back(idMic);
                        vStartT.push_back(startTime);
                        vEndT.push_back(endTime);
                        //cout << "link [" << sWordlink << " " << startTime << "," << endTime << "  " << postProb << "] " << idMic << endl;
                    }
                    //}
                }
            }
            //cout << "***Valid links:" << vPosts.size() << endl;
            /* If links were found ... */
            if (vPosts.size() > 0) {
                if (debug)
                    cout << "  Found " << vPosts.size() << " links.." << endl;
                /* Get list of unique occurrence of words */
                std::sort(vSegmentWords.begin(), vSegmentWords.end());
                vSegmentWords.erase(std::unique(vSegmentWords.begin(), vSegmentWords.end()), vSegmentWords.end());

                /* ****************** DISPLAY ***************** */
                if (debug) {
                    cout << "   Unique words: ";
                    for (unsigned i = 0; i < vSegmentWords.size(); i++) {
                        cout << vSegmentWords[i] << " ";
                    }
                    cout << endl << "   ";
                }

                /* ======================================== */
                unsigned numWords = vSegmentWords.size();

                vector<vector<double> > mWordsMics;

                /* Per each word */
                for (unsigned w = 0; w < numWords; w++) {
                    /* Keep indexes of link with this word in vIndex */
                    vector<unsigned> vIndex;
                    string currentWord = vSegmentWords[w];
                    //cout << endl << currentWord << ": ";
                    //                    if (tE>0.61 && tE<0.63){
                    //                        cout << endl << currentWord << ": ";
                    //                    }
                    for (unsigned i = 0; i < vWords.size(); i++) {
                        if (vWords[i] == currentWord) {
                            vIndex.push_back(i);
                        }
                    }

                    /* Per each microphone */
                    vector<double> vMics;
                    for (unsigned mic = 0; mic < numMics; mic++) {
                        /* Keep indexes of link with this word, and this mic in vIndexMic */
                        /* Get indexes of links of current Mic */
                        vector<unsigned> vIndexMic;
                        vector<float> vBoundFrames, vSegmentS, vSegmentE;
                        vector<double> vSegmentSum;
                        for (unsigned selectedlink = 0; selectedlink < vIndex.size(); selectedlink++) {
                            if (vMicid[vIndex[selectedlink]] == mic) {
                                vIndexMic.push_back(vIndex[selectedlink]);
                                vBoundFrames.push_back(vStartT[vIndex[selectedlink]]);
                                vBoundFrames.push_back(vEndT[vIndex[selectedlink]]);
                            }
                        }

                        /* Get list of unique occurrence of BoundFrames */
                        std::sort(vBoundFrames.begin(), vBoundFrames.end());
                        vBoundFrames.erase(std::unique(vBoundFrames.begin(), vBoundFrames.end()), vBoundFrames.end());

                        for (unsigned iS = 1; iS < vBoundFrames.size(); iS++) {
                            unsigned iSpre = iS - 1;
                            vSegmentS.push_back(vBoundFrames[iSpre]);
                            vSegmentE.push_back(vBoundFrames[iS]);
                            vSegmentSum.push_back(0.0);
                        }

                        /* IntraMic Score */
                        double Postscore = .0;
                        if (vIndexMic.size() > 0) {
                            for (unsigned i = 0; i < vIndexMic.size(); i++) {
                                /*=====================================*/
                                /*======= MAX OF FRAME-SUM OF ALL POSTERIORS =======*/
                                float BStart = vStartT[vIndexMic[i]];
                                float BEnd = vEndT[vIndexMic[i]];
                                unsigned IdxStartFrame, IdxEndFrame;
                                for (unsigned jIdx = 0; jIdx < vSegmentS.size(); jIdx++) {
                                    if (vSegmentS[jIdx] == BStart)
                                        IdxStartFrame = jIdx;
                                    if (vSegmentE[jIdx] == BEnd)
                                        IdxEndFrame = jIdx;
                                }

                                for (unsigned jIdx = IdxStartFrame; jIdx <= IdxEndFrame; jIdx++) {
                                    double currentPosterior = vPosts[vIndexMic[i]];
                                    //vSegmentSum.[jIdx] =vSegmentSum[jIdx] + currentPosterior;
                                    double preValue = vSegmentSum.at(jIdx);
                                    vSegmentSum.at(jIdx) = preValue + currentPosterior;
                                    if (vSegmentSum[jIdx] > Postscore) {
                                        Postscore = vSegmentSum[jIdx];
                                    }
                                }
                                /*=====================================*/
                                /*======= SUM OF ALL POSTERIORS =======*/
                                /* Postscore = Postscore + vPosts[vIndexMic[i]]; */
                                /*=====================================*/
                            }
                        }
                        //                        if (tE>0.61 && tE<0.63) {
                        //                            cout << Postscore << " ";
                        //                        }
                        vMics.push_back(Postscore);
                        //cout << "> ";
                    }
                    mWordsMics.push_back(vMics);
                }
                //cout << endl ;

                /* InterMic Score */
                vector<double> vNewPost;
                double sumvNewPost = .0, sumvNewPost2 = .0;
                for (unsigned wrow = 0; wrow < mWordsMics.size(); wrow++) {
                    double newPost = .0;
                    for (unsigned mic = 0; mic < numMics; mic++) {
                        newPost = newPost + mWordsMics[wrow][mic];
                    }
                    double interMicWordScore = (newPost / numMics);
                    vNewPost.push_back(interMicWordScore);
                    sumvNewPost = sumvNewPost + (interMicWordScore);
                    //cout << interMicWordScore << " " ;
                }
                //cout << endl;
                if (debug) {
                    cout << "   Final Scores ";
                    cout << "[vNewPost size " << vNewPost.size() << "] ";
                    cout << " [alpha*sumvNewPost " << alpha * sumvNewPost << "] ";
                }
                if ((alpha * sumvNewPost) > 1) {
                    //cout << "Normalized" << endl;
                    for (unsigned i = 0; i < vNewPost.size(); i++) {
                        vFinalScores.push_back(vNewPost[i] / (sumvNewPost));
                        sumvNewPost2 += (vNewPost[i] / (sumvNewPost));
                        if (debug)
                            cout << (vNewPost[i] / (sumvNewPost)) << " ";
                    }
                } else {
                    for (unsigned i = 0; i < vNewPost.size(); i++) {
                        vFinalScores.push_back(vNewPost[i]);
                        sumvNewPost2 += vNewPost[i];
                        //cout << vFinalScores[i] << " " ;
                        if (debug)
                            cout << (vNewPost[i]) << " ";
                    }
                }

                /* Criterion to continue/stop search of segment */
                NullPost = 1 - sumvNewPost2;

            } else {
                NullPost = 1;
                if (debug)
                    cout << "  Found no valid links" << endl;
            }

        }
        if ((NullPost <= THRSNULL) & (vPosts.size() > 0)) {
            /* Add NULL word and score to stack of candidates */
            vFinalScores.push_back(NullPost);
            vSegmentWords.push_back("NULL");
            if (debug)
                cout << "[FScore size " << vFinalScores.size() << "] ";

            /* Find maximum candidate score */
            int indexMax = vSegmentWords.size() - 1;
            double maxscore = .0;
            //cout << vTStartHypothesis[i] << " " << vTEndHypothesis[i] << "Candidates: " ;
            File candidatesout(writeCandidates, "a");
            fprintf(candidatesout, "%lf %lf", vBoundaries[boundStart], vBoundaries[boundEnd]);
            for (unsigned i = 0; i < vSegmentWords.size(); i++) {
                fprintf(candidatesout, " %s", vSegmentWords[i].c_str());
                if (vFinalScores[i] > maxscore) {
                    indexMax = i;
                    maxscore = vFinalScores[i];
                }
            }
            fprintf(candidatesout, "\n");

            if (debug)
                cout << "<< MaxScore " << maxscore << " Index " << indexMax << " :: " << vSegmentWords[indexMax] << ">>";
            vHypothesis.push_back(vSegmentWords[indexMax]);
            vPostHypothesis.push_back(maxscore);
            vTStartHypothesis.push_back(vBoundaries[boundStart]);
            vTEndHypothesis.push_back(vBoundaries[boundEnd]);
            currentalign++;
            NullPost = 0;
            boundStart = boundEnd;
        }

        if (boundEnd >= (vBoundaries.size() - 1)) {
            boundStart++;
        }

        if (boundStart > (vBoundaries.size() - 2)) {
            isSearchContinuing = false;
        }
    }

    File cnout(outCNfilename, "w");
    if (debug)
        cout << endl;

    cout << "Hypothesis: ";
    fprintf(cnout, "\"%s\"\n", utterance);
    for (unsigned i = 0; i < vHypothesis.size(); i++) {
        cout << vHypothesis[i] << " ";
        string hypword = vHypothesis[i];
        double wscore = vPostHypothesis[i];
        fprintf(cnout, "%s %lf\n", hypword.c_str(), wscore);
    }
    fprintf(cnout, ".\n");

    cout << endl;
    cout << "----------------------------------------------------------" << endl;
    cout << "min-posterior " << minPosteriorProb << " alpha " << alpha
            << " lratio " << lratio << " deltaref " << deltaref << " THRSNULL " << THRSNULL << endl;
    cout << "----------------------------------------------------------" << endl;

}

/*====================================================*/

/* MAIN */
int
main(int argc, char *argv[]) {
    setlocale(LC_CTYPE, "");
    Opt_Parse(argc, argv, options, Opt_Number(options), 0);

    lratio = lratio / 100;

    if (version) {
        printVersion(RcsId);
        exit(0);
    }
    if (!inLatticeList) {
        cerr << "need to specify the list of input files (lattices)!\n";
        return 0;
    }
    if (!tmpDataFile) {
        cerr << "need to specify the name of the utterance (no spaces in it)!\n";
        return 0;
    }

    /*Use multiword vocab in case we need it for processing*/
    Vocab *vocab;
    vocab = new Vocab;
    assert(vocab != 0);

    /* BOUNDARY IDENTIFICATION */
    Lattice *lattice2 = 0;
    /* create Ngram count trie (even if not used) */
    NgramCounts<FloatCount> ngramCounts(*vocab, order);

    /* ngram index file */
    //File *ngramIdxFile = 0;
    /* Build the LM used for lattice expansion */
    Ngram *ngram;
    //LM *useLM = ngram;
    //SubVocab hiddenVocab(*vocab);
    Vocab dictVocab;
    //VocabMultiMap dictionary(*vocab, dictVocab, intlogs);
    /* Read dictionary for multiword splitting */
    //LHash<const char*, Array< Array<char*>* > > multiwordDict;
    SubVocab ignoreVocab(*vocab);
    //SubVocab noiseVocab(*vocab);
    //VocabIndex *refVocabIndex = 0;

    /* Initialize temporal matrix file */
    File fileout(tmpDataFile, "w");
    fprintf(fileout, "");
    /* Initialize temporal CN file */
    File cnout1("/home/guerrero/tmpfilex", "w");
    fprintf(cnout1, "");

    if (inLatticeList) {
        File listOfFiles(inLatticeList, "r");
        makeArray(char, fileName,
                outLatticeDir ? strlen(outLatticeDir) + 1024 : 1);
        int flag;
        char buffer[1024];
        unsigned latticeCount = 0;
        while ((flag = fscanf(listOfFiles, " %1024s", buffer)) == 1) {
            cout << "Processing file " << buffer << "\n";
            //cerr
            char *sentid = strrchr(buffer, '/');
            if (sentid != NULL) {
                sentid += 1;
            } else {
                sentid = buffer;
            }
            //fileName[0] = '\0'; /* No outlatticeDir */
            //            getDataMatrix(latticeCount, buffer, fileName, lattice2,
            //                    ngramCounts, ngramIdxFile,
            //                    *useLM, *vocab, hiddenVocab,
            //                    dictionary, multiwordDict,
            //                    ignoreVocab, noiseVocab, refVocabIndex);
            getDataMatrix(latticeCount, buffer, *vocab, ignoreVocab);

            latticeCount++;
            numMics = latticeCount;
        }
    }

    /* =========================================
     *  Analysis 1: does w occurs in DeltaBoundaries?
     * ========================================= */
    if (typeAnalysis == 1) {
        analysis_deltaB();
    } else if (typeAnalysis == 2) {
        isHypothesisInCN();
    } else if (typeAnalysis == 3) {
        identifyBoundaries();
        validateSegmentsV3();
    } else if (typeAnalysis == 4) {
        /* The validation of segment is different
         Intra-mic score computed on a per frame basis */
        identifyBoundaries();
        validateSegmentsFrame();
    } else if (typeAnalysis == 5) {
        identifyBoundariesPQ();
        validateSegments();
    } else if (typeAnalysis == 6) {
        identifyBoundariesPQ();
        validateSegmentsBw();
    } else if (typeAnalysis == 7) {
        identifyBoundariesPQ();
        validateSegments();
        validateSegmentsBw();
        //resetboundaries();
        //validateSegments(); //Second pass using Fw/Bw boundaries
    } else if (typeAnalysis == 8) {
        //ForLattice Analysis
        useFileBoundaries();
        validateSegments();
    } else if (typeAnalysis == 9) {
        useFileBoundarieswrd();
        forceSegmentsWrd();
    } else if (typeAnalysis == 10) {
        useFileBoundarieswrd();
        forceSegmentsWrdSil(); //Shifting + DirectScoring
    } else if (typeAnalysis == 11) {
        useFileBoundarieswrd();
        forceSegmentsWrdSilSkip0();
        forceSegmentsWrdSilSkip1();
        forceSegmentsWrdSilSkip2();
        forceSegmentsWrdSilSkip3();
    } else if (typeAnalysis == 12) {
        useFileBoundarieswrd();
        forceSegmentsWrdSilInc0();
        forceSegmentsWrdSilInc1();         //forceSegmentsWrdSilInc2();        //forceSegmentsWrdSilInc3();        
        forceSegmentsWrdSilInc4(2);
        forceSegmentsWrdSilInc4(3);
        forceSegmentsWrdSilInc4(4);
        forceSegmentsWrdSilInc4(5);
        forceSegmentsWrdSilInc4(6);
    } else if (typeAnalysis == 13) {
        /* Use refBounds, Increase N, validateSegments */
        useFileBoundarieswrd();
        forceSegmentsWrdSilInc0Bounds();
        forceSegmentsWrdSilInc1Bounds();
        forceSegmentsWrdSilIncNBounds(2);
        forceSegmentsWrdSilIncNBounds(3);
        forceSegmentsWrdSilIncNBounds(4);
        forceSegmentsWrdSilIncNBounds(5);
        forceSegmentsWrdSilIncNBounds(6);
    } else if (typeAnalysis == 14) {
        /* Use refBounds, Increase N, validateSegments */
        useFileBoundarieswrd();
        forceSegmentsWrdSilRem0Bounds();
        forceSegmentsWrdSilRem1Bounds();
        forceSegmentsWrdSilRem2Bounds();
        forceSegmentsWrdSilRem3Bounds();
        forceSegmentsWrdSilRem4Bounds();
    } else if (typeAnalysis == 15) {
        //10
        useFileBoundarieswrd();
        forceSegmentsWrdSilSegmV(); //Shifting + SegmentValidation        
    } else {
        /* ==== Identify boundaries from peak selection === */
        identifyBoundaries();
        /* ====== Validate time segments ====== */
        validateSegments();
    }

    cout << endl;
    cout << "----------------------------------------------------------" << endl;
    cout << "min-posterior " << minPosteriorProb << " alpha " << alpha
            << " lratio " << lratio << " deltaref " << deltaref << " thrsnull" 
            << THRSNULL << " BValid " << boundsvalidation << " md " << manualdelta 
            << " mdS " << manualdeltaS << " mdE " << manualdeltaE 
            << "shiftbound " << shiftbound << endl;
    cout << "----------------------------------------------------------" << endl;

    /* ------ Freeing memory - End of operation ------- */
    delete vocab;
    exit(haveError);

}
